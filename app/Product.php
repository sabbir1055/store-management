<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }
    public function subcategory(){
        return $this->hasOne(SubCategory::class,'id','sub_category_id');
    }

    public function vendor(){
        return $this->hasOne(Vendor::class,'id','vendor_id');
    }

    public function stock($id){
        return Product::where('product_code',$id)->sum('stock');
    }

    public function price($id){
        return Product::where('product_code',$id)->orderby('id','desc')->first();
    }
}
