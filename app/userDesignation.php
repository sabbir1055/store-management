<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userDesignation extends Model
{
    public function getDesignationData(){
        return $this->hasOne(Designation::class,'id','designation_id');
    }
}
