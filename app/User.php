<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
class User extends Authenticatable
{
    use EntrustUserTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function data(){
        return $this->hasOne(UserData::class);
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function theme(){
        return $this->hasOne(Theme::class,'user_id','id');
    }

    public function designation(){
        return $this->hasOne(userDesignation::class,'user_id','id');
    }

    public function designationdata(){
        return $this->hasManyThrough(Designation::class,userDesignation::class,'user_id','id','id','designation_id');
    }
}
