<?php

namespace App\Http\Controllers;
use Auth;
use App\Theme;
use Session;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    public function edit(){
        $theme = Auth::user()->theme;
        return view('backend.theme',compact('theme'));
    }

    public function update(Request $request){
        // dd($request->all());
        $theme                  = Theme::where('user_id',Auth::user()->id)->first();
        $theme->top_nav                             = $request->top_nav;
        $theme->font_family                         = $request->font_family;
        $theme->font_size                           = $request->font_size;
        $theme->brand                               = $request->brand;
        $theme->left_side                           = $request->left_side;
        $theme->left_side_dropdown                  = $request->left_side_dropdown;
        $theme->left_border_left_color              = $request->left_border_left_color;
        $theme->left_dropdown_item_back             = $request->left_dropdown_item_back;
        $theme->top_nav_drop_background             = $request->top_nav_drop_background;
        $theme->top_nav_drop_background_hover       = $request->top_nav_drop_background_hover;
        $theme->top_nav_drop_background_items       = $request->top_nav_drop_background_items;
        $theme->main_background                     = $request->main_background;
        $theme->title_background                    = $request->title_background;
        $theme->body_background                     = $request->body_background;
        $theme->panel_background                    = $request->panel_background;
        $theme->body_font_color                     = $request->body_font_color;
        $theme->title_font_color                    = $request->title_font_color;
        $theme->left_side_font_color                = $request->left_side_font_color;
        $theme->left_side_font_family               = $request->left_side_font_family;
        $theme->left_side_font_size                 = $request->left_side_font_size;

        $theme->save();
        Session::flash('successMsg','Colours are applied');
        return redirect()->back();
    }
}
