<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\Designation;
use App\Role;
use App\Import;
use Illuminate\Http\Request;

class DesignationController extends Controller
{
    public function index(){
        $designation = Designation::all();
        return view('backend.designation.index',compact('designation'));
    }

    public function create(){
        return view('backend.designation.create');
    }

    public function store(Request $request){
        $designation = new Designation();
        $designation->title = $request->title;
        $designation->basic_sallery = $request->basic;
        $designation->increment = $request->increment;
        $designation->medical = $request->medical;
        $designation->home = $request->home;
        $designation->other = $request->other;
        $designation->leave = $request->leave;

        if($designation->save()){
            Session::flash('successMsg','Designation added.');
            return redirect()->route('designation.index');
        }else{
            Session::flash('successMsg','Designation faield to add.');
            return redirect()->back();
        }
    }

    public function edit($id){
        $designation = Designation::find($id);
        return view('backend.designation.edit',compact('designation'));
    }

    public function update(Request $request,$id){
        $designation = Designation::find($id);
        $designation->title = $request->title;
        $designation->basic_sallery = $request->basic;
        $designation->increment = $request->increment;
        $designation->medical = $request->medical;
        $designation->home = $request->home;
        $designation->other = $request->other;
        $designation->leave = $request->leave;

        if($designation->save()){
            Session::flash('successMsg','Designation updated.');
            return redirect()->route('designation.index');
        }else{
            Session::flash('successMsg','Designation faield to update.');
            return redirect()->back();
        }
    }

    public function delete(Request $request,$id){
        $designation = Designation::find($id);
        if($designation->delete()){
            Session::flash('successMsg','Designation deleted.');
            return redirect()->route('designation.index');
        }else{
            Session::flash('successMsg','Designation faield to delete.');
            return redirect()->back();
        }
    }

}
