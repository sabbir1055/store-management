<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\Item;
use App\Role;
use App\Import;
use Illuminate\Http\Request;

class IdentifierController extends Controller
{
    public function index(){
        return view('backend.identifier.index');
    }

    public function data(Request $request){
        $columns = array(
          0 => 'id',
          1 => 'title',
          2 => 'code',
          3 => 'identity',
          5 => 'import_id',
          4 => 'resaler_id',
          6 => 'type',
          7 => 'action'
        );
        $total_data = Item::count();

        $limit = $request->length;
        $start = $request->start;
        $order = $columns[$request->input('order.0.column')];
		    $dir = $request->input('order.0.dir');


        if(empty($request->input('search.value'))){
    			$employee_data = Item::offset($start)
      					->limit($limit)
      					->orderBy($order,$dir)
      					->get();
      			$totalFiltered = Item::count();
      		}else{
      			$search = $request->input('search.value');
      			$employee_data = Item::Where('product_code','like',"%{$search}%")
                                ->orWhere('import_id','like',"%{$search}%")
                                ->orWhere('type','like',"%{$search}%")
                                ->orWhere('resaler_id','like',"%{$search}%")
      							->orWhere('Identeti_number','like',"%{$search}%")
      							->offset($start)
      							->limit($limit)
      							->orderBy($order, $dir)
      							->get();
      			$totalFiltered = Item::Where('product_code','like',"%{$search}%")
                                ->orWhere('import_id','like',"%{$search}%")
                                ->orWhere('type','like',"%{$search}%")
                                ->orWhere('resaler_id','like',"%{$search}%")
      							->orWhere('Identeti_number','like',"%{$search}%")
      							->count();
    		}

        $data           = [];
        $i = 1;
        if($employee_data){
          foreach ($employee_data as $key => $value) {
            $nestedData           = [];
            $nestedData['#']      = $i;
            $nestedData['title']   = $value->product ? $value->product[0]->title : 'null';
            $nestedData['code']  = $value->product_code;
            $nestedData['identity'] = $value->Identeti_number;
            if($value->type == 1){
              $nestedData['type'] = 'sold';
            }
            elseif($value->type == 2){
              $nestedData['type'] = 'Resaler hand';
            }
            elseif($value->type == 3){
                $nestedData['type'] = 'Returned';
            }
            elseif($value->type == 4){
                  $nestedData['type'] = 'Damaged';
            }
            elseif($value->type == 4){
                  $nestedData['type'] = 'Recovered';
            }
            else{
                $nestedData['type'] = 'Available';
            }
            $nestedData['import_id'] = $value->import_id;
            $nestedData['resaler_id'] = ($value->resaler_id != 0) ? $value->resaler_id :' ';
              $nestedData['action'] ='<a href="#" class="btn btn-success" onclick="edit('.$value->id.')"> Edit status </a>';
            $data[]                 = $nestedData;
            $i+=1;
          }
        }
        // dd($data);
        $json_data = array(
          "draw"			       => intval($request->input('draw')),
    			"recordsTotal"	   => intval($total_data),
    			"recordsFiltered"  => intval($totalFiltered),
    			"data"			       => $data

        );
        //dd(json_encode($json_data));
        return json_encode($json_data);
    }

    public function edit($id){
        $identifier = Item::find($id);
        return view('backend.identifier.edit',compact('identifier'));
    }

    public function update(Request $request,$id){
        $identifier = Item::find($id);
        $identifier->type = $request->type;
        if($identifier->save()){
            Session::flash('successMsg','Product status updated');
            return redirect()->route('identifier.index');
        }else{
            Session::flash('successMsg','Product status failed to update');
            return redirect()->route('identifier.index');
        }
    }
}
