<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\Commision;
use App\Role;
use App\Import;
use Illuminate\Http\Request;

class ComissionController extends Controller
{
    public function index(){
        $comission = Commision::all();
        return view('backend.comission.index',compact('comission'));
    }
    public function create(){
        return view('backend.comission.create');
    }
    public function store(Request $request){
      // dd($request->all());

      $comission = new Commision();
      $comission->amount = $request->amount;
      $comission->commision = $request->comission;
      if($comission->save()){
        Session::flash('successMsg','Comission added.');
        return redirect()->route('comission.index');
      }else{
        Session::flash('errorMsg','Comission failed to add.');
        return redirect()->route('comission.index');
      }
    }
    public function edit($id){
        $comission = Commision::find($id);
        return view('backend.comission.edit',compact('comission'));
    }
    public function update(Request $request,$id){
      $comission = Commision::find($id);
      $comission->amount = $request->amount;
      $comission->commision = $request->comission;
      if($comission->save()){
        Session::flash('successMsg','Comission added.');
        return redirect()->route('comission.index');
      }else{
        Session::flash('errorMsg','Comission failed to add.');
        return redirect()->route('comission.index');
      }
    }
    public function delete($id){
      $membership = Commision::find($id);
      if($membership->delete()){
          Session::flash('successMsg','Comission deleted.');
          return redirect()->route('comission.index');
      }else{
          Session::flash('errorMsg','Comission failed to delete.');
          return redirect()->route('comission.index');
      }
    }

}
