<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\Vendor;
use App\Import;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    public function index(){
        return view('backend.import.index');
    }
    public function create(){
        $vendor = Vendor::all();
        return view('backend.import.create',compact('vendor'));
    }
    public function store(Request $request){
        // dd($request->all());
        $import = new Import();
        $import->vendor_id = $request->vendor_id;
        $import->paid      = $request->total_paid;
        $import->total     = $request->total_payment;
        $import->due       = $request->total_due;
        $import->is_on_cash= 1;
        $product = [];
        foreach ($request->product_title as $key => $value) {
            $product[$key]['title']                 = $value;
            $product[$key]['code']                  = $request->product_code[$key] ;
            $product[$key]['price']                 = $request->product_price[$key] ;
            $product[$key]['qty']                   = $request->product_qty[$key] ;
            $product[$key]['import_identification'] = $request->product_code[$key].''.time().rand(1111,9999); ;
        }
        $import->products = json_encode($product);
        $import->reciept_id = Auth::user()->id ?? 1;
        if($import->save()){
            Session::flash('successMsg','Import added to list');
            return redirect()->back();
        }else{
            Session::flash('errorMsg','Import added to list');
            return redirect()->back();
        }
    }

    public function data(Request $request){
        $columns = array(
            0 => 'id',
            1 => 'id',
            2 => 'total',
            3 => 'paid',
            4 => 'due',
            5 => 'vendor_id',
            6 => 'action',
      );

      $total_data = Import::count();

      $limit = $request->length;
      $start = $request->start;
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');


      if(empty($request->input('search.value'))){
        $employee_data = Import::offset($start)
              ->limit($limit)
              ->orderBy($order,$dir)
              ->get();
          $totalFiltered = Import::count();
        }else{
          $search = $request->input('search.value');
          $employee_data = Import::where('id', 'like', "%{$search}%")
                  ->orWhere('total','like',"%{$search}%")
                  ->orWhere('paid','like',"%{$search}%")
                  ->orWhere('due','like',"%{$search}%")
                  ->orWhereHas('vendor', function($q) use($search){
                                    $q->where('name', 'like', "%{$search}%");
                                })
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy($order, $dir)
                  ->get();
          $totalFiltered = Import::where('id', 'like', "%{$search}%")
                  ->orWhere('total','like',"%{$search}%")
                  ->orWhere('paid','like',"%{$search}%")
                  ->orWhere('due','like',"%{$search}%")
                  ->orWhereHas('vendor', function($q) use($search){
                                    $q->where('name', 'like', "%{$search}%");
                                })
                  ->count();
      }

      $data           = [];
      $i = 1;
      if($employee_data){
        foreach ($employee_data as $key => $value) {
          $nestedData           = [];
          $nestedData['show']    = `<i class="icon-edit"></i>`;
          $nestedData['id']    = $value->id;
          $nestedData['total']  = $value->total;
          $nestedData['products']  = $value->products;
          $nestedData['paid']         = $value->paid ;
          $nestedData['due']         = $value->due ;
          $nestedData['vendor_id']      = $value->vendor->name ;
          if($value->status){
            $nestedData['action'] ='<a href="#" class="btn btn-danger" onclick="status('.$value->id.',0)"> <i class="fa fa-lock"></i> </a>
            <a href="#" class="btn btn-warning"  onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
            <a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
          }else{
            $nestedData['action'] ='<a href="#" class="btn btn-success"  onclick="status('.$value->id.',1)"> <i class="fa fa-check"></i> </a>
            <a href="#" class="btn btn-warning"  onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
            <a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-lock"></i> </a>';
          }
          // dd($nestedData);
          $data[]                 = $nestedData;
          $i+=1;
        }
      }
      $json_data = array(
        "draw"			       => intval($request->input('draw')),
        "recordsTotal"	     => intval($total_data),
        "recordsFiltered"    => intval($totalFiltered),
        "data"			       => $data

      );
      return json_encode($json_data);
    }

    public function status(Request $request){
        $import  = Import::find($request->import_id);
        $import->status = ($import->status == 1) ? 0 : 1 ;
        $import->save();
    }

    public function update(Request $request,$id){

    }

    public function edit($id){
        $import = Import::find($id);
        return view('backend.import.edit',compact('import'));
    }

    public function view($id){

    }

}
