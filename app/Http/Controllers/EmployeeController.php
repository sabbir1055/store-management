<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\Theme;
use App\UserData;
use App\User;
use App\userDesignation;
use App\Role;
use App\Import;
use App\Designation;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index(Request $request){
        return view('backend.employee.index');
    }
    public function data(Request $request){
        $columns = array(
          0 => 'id',
          1 => 'name',
          2 => 'email',
          3 => 'msisdn',
          4 => 'role',
          5 => 'designation',
          6 => 'status',
          7 => 'action'
        );

        $total_data = User::count();

        $limit = $request->length;
        $start = $request->start;
        $order = $columns[$request->input('order.0.column')];
		    $dir = $request->input('order.0.dir');


        if(empty($request->input('search.value'))){
    			$employee_data = User::offset($start)
      					->limit($limit)
      					->orderBy($order,$dir)
      					->get();
      			$totalFiltered = User::count();
      		}else{
      			$search = $request->input('search.value');
      			$employee_data = User::Where('email','like',"%{$search}%")
                                    ->orWhereHas('data', function($q) use($search){
                                            $q->where('name', 'like', "%{$search}%")
                                                ->orWhere('msisdn','like', "%{$search}%");
                                        }
                                    )
                                    ->orWhereHas('role', function($q) use($search){
                                            $q->where('display_name', 'like', "%{$search}%");
                                        }
                                    )
                                    ->orWhereHas('designationdata', function($q) use($search){
                                                $q->where('title', 'like', "%{$search}%");
                                        }
                                    )
          							->offset($start)
          							->limit($limit)
          							->orderBy($order, $dir)
          							->get();
      			$totalFiltered = User::Where('email','like',"%{$search}%")
                                // ->orWhere('user_datas.msisdn','like',"%{$search}%")
      							->count();
    		}

        $data           = [];
        $i = 1;
        if($employee_data){
          foreach ($employee_data as $key => $value) {
            $nestedData           = [];
            $nestedData['#']      = $i;
            $nestedData['name']   = $value->data ? $value->data->name : 'null';
            $nestedData['designation']   = $value->designation ? $value->designation->getdesignationData->title : '';
            $nestedData['email']  = $value->email;
            $nestedData['msisdn'] = $value->data ? $value->data->msisdn : 'null';
            if($value->status){
              $nestedData['status'] = 'Active';
            }else{
              $nestedData['status'] = 'Deactive';
            }
            $nestedData['role'] = $value->role->display_name;
            if($value->status){
              $nestedData['action'] ='<a href="#" class="btn btn-danger" onclick="status('.$value->id.')"> <i class="fa fa-lock"></i> </a>';
            }else{
              $nestedData['action'] ='<a href="#" class="btn btn-success"  onclick="status('.$value->id.')"> <i class="fa fa-check"></i> </a>';
            }
            $data[]                 = $nestedData;
            $i+=1;
          }
          // dd($data);
        }
        $json_data = array(
          "draw"			       => intval($request->input('draw')),
    			"recordsTotal"	   => intval($total_data),
    			"recordsFiltered"  => intval($totalFiltered),
    			"data"			       => $data

        );
        //dd(json_encode($json_data));
        return json_encode($json_data);
    }
    public function create(){
        $role = Role::all();
      return view('backend.employee.create',compact('role'));
    }
    public function store(Request $request){
        $v = array(
          'email' => 'required|email|unique:users',
        );
        $validator = Validator::make($request->all(), $v);
        if ($validator->fails()) {
            Session::flash('errorMsg','Please check your inuput data.');
            return redirect()->back();
        }else{
          $user         = new  User();
          $user->email  = $request->email;
          $user->role_id= $request->role;
          $user->password = bcrypt('123456');
          if($user->save()){
              $theme = new Theme();
              $theme->user_id = $user->id;
              $theme->save();
              $user_data = new UserData();
              $user_data->user_id = $user->id;
              $user_data->save();
              $user->attachRole($request->role);
              Session::flash('successMsg','Employee Added.');
              return redirect()->route('users');
          }else{
              Session::flash('errorMsg','Employee failed to add.');
              return redirect()->back();
          }
      }
    }
    public function status(Request $request){
      $employee = User::find($request->employee_id);
      if($employee){
        if($employee->status){
          $employee->status = 0;
          if($employee->save()){
              Session::flash('successMsg','Employee status change to deactive.');
            }
            else{
              Session::flash('errorMsg','Employee status failed change.');
            }
        }
        else{
          $employee->status = 1;
          if($employee->save()){
            Session::flash('successMsg','Employee status change to active.');
             }
             else{
               Session::flash('errorMsg','Employee status failed change.');
             }
        }
      }
    }
    public function designation(){
        $employee       = User::all();
        $designation    = Designation::all();

        return view('backend.employee.designation',compact('employee','designation'));
    }
    public function designationStore(Request $request){
        // dd($request->all());
        $v = array(
          'user_id' => 'required|unique:user_designations',
        );
        $validator = Validator::make($request->all(), $v);
        if ($validator->fails()) {
            Session::flash('errorMsg','alreay designated.');
            return redirect()->back();
        }else{
            $user = new userDesignation();
            $user->user_id = $request->user_id;
            $user->designation_id = $request->designation;
            if($user->save()){
                Session::flash('successMsg','Employee designation added.');
                return redirect()->route('users');
            }else{
                Session::flash('successMsg','Employee designation faield to add.');
                return redirect()->route('users');
            }
        }
    }

}
