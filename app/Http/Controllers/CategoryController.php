<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use DB;
use Image;
use App\Category;
use Validator;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $category = Category::all();
        return view('backend.category.index',compact('category'));
    }

    public function create(){
        return view('backend.category.create');
    }

    public function store(Request $request){
        $category = new Category();
        $v = [
            'image' => 'image|mimes:png,ico|max:200',
        ];
        $validator  = Validator::make($request->all(),$v);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            $category->title = $request->title;
            if($request->has('image')){
                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $category->image ='uploads/' . $filename;
                }
            }
            if($category->save()){
                Session::flash('successMsg','Category added.');
                return redirect()->route('category.index');
            }
            else{
                Session::flash('successMsg','Category failed to add.');
                return redirect()->route('category.index');
            }
        }
    }
    public function edit($id){
        $category = Category::find($id);
        return view('backend.category.edit',compact('category'));
    }
    public function update(Request $request,$id){
        $category = Category::find($id);
        if($request->has('image')){
            $v = [
                'image' => 'image|mimes:png,ico|max:200',
            ];
            $validator  = Validator::make($request->all(),$v);
            if($validator->fails()){
                Session::flash('errorMsg',$validator->errors());
                return redirect()->back();
            }else{
                    if ($request->file('image')) {
                        $avatar = $request->file('image');
                        $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                        $category->image ='uploads/' . $filename;
                    }
            }
        }
        $category->title = $request->title;
        if($category->save()){
            Session::flash('successMsg','Category updated.');
            return redirect()->route('category.index');
        }else{
            Session::flash('successMsg','Category failed to update.');
            return redirect()->route('category.index');
        }
    }

    public function delete($id){
        $category = Category::find($id);
        if($category->delete()){
            Session::flash('successMsg','Category deleted.');
            return redirect()->route('category.index');
        }else{
            Session::flash('successMsg','Category failed to delete.');
            return redirect()->route('category.index');
        }
    }
}
