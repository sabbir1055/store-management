<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\User;
use App\Membership;
class MembershipController extends Controller
{
    public function index(){
        $membership = Membership::all();
        return view('backend.membership.index',compact('membership'));
    }

    public function create(){
        return view('backend.membership.create');
    }

    public function store(Request $request){
        $membership = new Membership();
        $membership->title = $request->title;
        $membership->discount = $request->discount;
        if($membership->save()){
            Session::flash('successMsg','Membership added.');
            return redirect()->route('membership.index');
        }else{
            Session::flash('errorMsg','Membership failed to add.');
            return redirect()->route('membership.index');
        }
    }

    public function edit($id){
        $membership = Membership::find($id);
        return view('backend.membership.edit',compact('membership'));
    }

    public function update(Request $request,$id){
        $membership = Membership::find($id);
        $membership->title = $request->title;
        $membership->discount = $request->discount;
        if($membership->save()){
            Session::flash('successMsg','Membership updated.');
            return redirect()->route('membership.index');
        }else{
            Session::flash('errorMsg','Membership failed to update.');
            return redirect()->route('membership.index');
        }
    }

    public function delete($id){
        $membership = Membership::find($id);
        if($membership->delete()){
            Session::flash('successMsg','Membership deleted.');
            return redirect()->route('membership.index');
        }else{
            Session::flash('errorMsg','Membership failed to delete.');
            return redirect()->route('membership.index');
        }
    }
}
