<?php

namespace App\Http\Controllers;
use App\Role;
use App\Permission;
use Session;
use App\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
  public function index(){
    $role = Role::all();
    return view('backend.role.roles',compact('role'));
  }

  public function create(){
    $permission = Permission::all();
    return view('backend.role.rolecreate',compact('permission'));
  }

  public function store(Request $request){
    // dd($request->all());
    $role = new Role();
    $role->name         = $request->name;
    $role->display_name = $request->display_name;
    $role->description  = $request->detail;
    if($role->save()){
      if($request->has('permissions')){
        $role->attachPermissions($request->permissions);
      }
      Session::flash('successMsg','Role added.');
    }else{
      Session::flash('errorMsg','Role failed to add.');
    }

    return redirect()->route('roles');
  }

  public function edit($id){
    $permission = Permission::all();
    $role       = Role::find($id);
    return view('backend.role.roleedit',compact('role','permission'));
  }

  public function update(Request $request,$id){
    $role = Role::find($id);
    $role->name         = $request->name;
    $role->display_name = $request->display_name;
    $role->description  = $request->detail;
    if($role->save()){
      if($request->has('permissions')){
        if($role->permissions){
          $role->detachPermissions();
        }
        $role->attachPermissions($request->permissions);
      }
      Session::flash('successMsg','Role added.');
    }else{
      Session::flash('errorMsg','Role failed to add.');
    }

    return redirect()->route('roles');
  }

  public function delete($id){
    if(Role::find($id)->delete()){
      Session::flash('successMsg','Role deleted.');
    }else{
      Session::flash('errorMsg','Role failed to delete.');
    }
    return redirect()->route('roles');
  }

  public function editEmployee(){
    $roles = Role::all();
    $user  = User::all();
    return view('backend.role.editemployee',compact('roles','user'));
  }

  public function updateEmployee(Request $request){
    // dd($request->all());
    $user   = User::find($request->employee);
    $user->role_id = $request->role;
    $user->save();
    if($user->roles){
      $user->detachRoles();
    }
    $result = $user->attachRole($request->role);
      Session::flash('successMsg','Employee role updated.');
      return redirect()->route('users');
  }

}
