<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\SalesOffer;
use DateTime;
use App\Role;
use App\Product;
use App\Import;
use Illuminate\Http\Request;

class SalesOfferController extends Controller
{
    public function index(){
        $sales = SalesOffer::orderby('created_at','desc')->get();
        return view('backend.salesoffer.index',compact('sales'));
    }
    public function create(){
        $product = Product::where('status',1)->where('stock','>',0)->get();
        return view('backend.salesoffer.create',compact('product'));
    }
    public function store(Request $request){
      $saleoffer = new SalesOffer();
      $saleoffer->product_id = $request->product_id;
      $saleoffer->discount = $request->discount;
      $saleoffer->from = new DateTime($request->from);
      $saleoffer->to = new DateTime($request->to);
      $saleoffer->sales_type = $request->type;
      if($saleoffer->save()){
        Session::flash('successMsg','Sales offer added.');
        return redirect()->route('salesoffer.index');
      }else{
        Session::flash('errorMsg','Sales offer failed to add.');
        return redirect()->route('salesoffer.index');
      }
    }
    public function edit($id){
        $sales = SalesOffer::find($id);
        $product = Product::where('status',1)->get();
        return view('backend.salesoffer.edit',compact('sales','product'));
    }
    public function update(Request $request,$id){
        $saleoffer = SalesOffer::find($id);
        $saleoffer->product_id = $request->product_id;
        $saleoffer->discount = $request->discount;
        $saleoffer->from = new DateTime($request->from);
        $saleoffer->to = new DateTime($request->to);
        $saleoffer->sales_type = $request->type;
        if($saleoffer->save()){
          Session::flash('successMsg','Sales offer updated.');
          return redirect()->route('salesoffer.index');
        }else{
          Session::flash('errorMsg','Sales offer failed to update.');
          return redirect()->route('salesoffer.index');
        }
    }
    public function delete($id){
      $membership = SalesOffer::find($id);
      if($membership->delete()){
          Session::flash('successMsg','Sales offer deleted.');
          return redirect()->route('salesoffer.index');
      }else{
          Session::flash('errorMsg','Sales offer failed to delete.');
          return redirect()->route('salesoffer.index');
      }
    }
}
