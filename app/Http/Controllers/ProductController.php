<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use DB;
use App\Product;
use App\Item;
use App\Category;
use App\Import;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index(){
        return view('backend.product.index');
    }
    public function create(){
        $import = Import::all();
        $category = Category::all();
        // dd($import);
        return view('backend.product.create',compact('import','category'));
    }
    public function import(Request $request){
        $import = Import::find($request->import_id);
        return json_encode($import);
    }
    public function store(Request $request){
        $added_product = Item::where('import_identification',$request->import_identification)->count('id');
        $import        = Import::find($request->I_id);
        foreach (json_decode($import->products) as $key => $value) {
            if($value->import_identification == $request->import_identification){
                if((int)$value->qty == (int)$added_product){
                    Session::flash('errorMsg','This already inserted in your product list');
                    return redirect()->back();
                }
                if((int)$value->qty < ((int)$added_product+(int)$request->stock)){
                    Session::flash('errorMsg','You already inserted '. (int)$added_product .' quantity of this.You can insert '. ($value->qty-$added_product) .' quantity of this kind.');
                    return redirect()->back();
                }
            }
        }
        $v = [
            'image' => 'image|mimes:jpeg,png,jpg',
        ];
        $validator  = Validator::make($request->all(),$v);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            try{
                 DB::beginTransaction();
                $product = new Product();
                if($request->has('image')){
                    if ($request->file('image')) {
                        $avatar = $request->file('image');
                        $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                        $product->image ='uploads/' . $filename;
                    }
                }
                $product->title  = $request->title;
                $product->import_id = (int)$request->I_id;
                $import = Import::find($request->I_id);
                $product->vendor_id = $import->vendor_id;
                $product->price  = (float)$request->price;
                $product->product_code  = $request->code;
                $product->about  = $request->about;
                $product->profit_unit  = (int)$request->price-(int)$request->p_price;
                $product->stock  = (int)$request->stock;
                $product->status  = 1;
                $product->category_id  = (int)$request->category;
                $product->sub_category_id  = (int)$request->sub_category;
                $product->import_identification  = $request->import_identification;
                // dd($product);
                if($product->save()){
                        $last_value = Product::orderBy('created_at', 'desc')->first();
                        $last_id   =  $last_value->id ?? 0 ;
                        // dd($last_id);
                    for($i = 1 ; $i<=(int)$request->stock;$i++) {
                        // dd($product);
                        $item = new Item();
                        $item->product_code             =  $request->code;
                        $item->product_id               =  $product->id;
                        $item->product_price            =  (float)$product->price;
                        $item->import_id                =  (int)$request->I_id;
                        $item->type                     =  0;
                        $item->resaler_id               =  0;
                        $item->Identeti_number          =  (int)$last_id+$i.''.time().rand(1111,9999);
                        $item->import_identification    = $request->import_identification;
                        $item->save();

                    }
                     DB::commit();
                    Session::flash('successMsg','Product added successfully');
                    return redirect()->route('product.index');
                }
                else{
                    Session::flash('errorMsg','Product failed to add');
                    return redirect()->back();
                }
            }catch(\Exception $e){
                DB::rollback();
                Session::flash('errorMsg','Product failed to add');
                return redirect()->back();
            }
        }

    }

    public function data(Request $request){
        // dd($request->input('order.0.column'));
        $columns = array(
            0 => 'id',
            1 => 'title',
            2 => 'code',
            3 => 'stock',
            4 => 'category',
            5 => 'subcategory',
            6 => 'price',
            7 => 'import_id'
        );

        $total_data = Product::count();

        $limit = $request->length;
        $start = $request->start;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        // dd($order);

        if(empty($request->input('search.value'))){
            $employee_data = Product::offset($start)
            ->limit($limit)
            ->orderBy($order,$dir)
            ->get();
            $totalFiltered = Product::count();
        }else{
            $search = $request->input('search.value');
            $employee_data = Product::where('price', 'like', "%{$search}%")
            ->orWhere('title','like',"%{$search}%")
            ->orWhere('product_code','like',"%{$search}%")
            ->orWhere('stock','like',"%{$search}%")
            ->orWhere('import_id','like',"%{$search}%")
            ->offset($start)
            ->limit($limit)
            ->orderBy($order, $dir)
            ->get();
            $totalFiltered = Product::where('price', 'like', "%{$search}%")
            ->orWhere('title','like',"%{$search}%")
            ->orWhere('product_code','like',"%{$search}%")
            ->orWhere('stock','like',"%{$search}%")
            ->orWhere('import_id','like',"%{$search}%")
            ->count();
        }

        $data           = [];
        $i = 1;
        if($employee_data){
            foreach ($employee_data as $key => $value) {
                $nestedData             = [];
                $nestedData['#']        = $i;
                $nestedData['title']    = $value->title;
                $nestedData['category'] = $value->category ? $value->category->title : 'null';
                $nestedData['subcategory'] = $value->subcategory ? $value->subcategory->title : 'None' ;
                if($value->status){
                    $nestedData['status'] = 'Active';
                }else{
                    $nestedData['status'] = 'Deactive';
                }
                $nestedData['stock']         = $value->stock ;
                $nestedData['price']         = $value->price ;
                $nestedData['code']         = $value->product_code ;
                $nestedData['import_id']     = $value->import_id ;
                if($value->status){
                    $nestedData['action'] ='<a href="#" class="btn btn-danger" onclick="status('.$value->id.',0)"> <i class="fa fa-lock"></i> </a>
                    <a href="#" class="btn btn-warning"  onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
                    <a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye" ></i> </a>';
                }else{
                    $nestedData['action'] ='<a href="#" class="btn btn-success" onclick="status('.$value->id.',1)"> <i class="fa fa-click"></i> </a>
                    <a href="#" class="btn btn-warning" onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
                    <a href="#" class="btn btn-info" onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
                }
                // dd($nestedData);
                $data[]                 = $nestedData;
                $i+=1;
            }
        }
        $json_data = array(
            "draw"			       => intval($request->input('draw')),
            "recordsTotal"	   => intval($total_data),
            "recordsFiltered"  => intval($totalFiltered),
            "data"			       => $data

        );
        return json_encode($json_data);
    }

    public function status(Request $request){
        $product  = Product::find($request->product_id);
        $product->status = ($product->status == 1) ? 0 : 1 ;
        $product->save();
    }

    public function update(Request $request,$id){
        $product =  Product::find($id);
        if($request->has('image')){
            $v = [
                'image' => 'image|mimes:jpeg,png,jpg|max:5000',
            ];
            $validator  = Validator::make($request->all(),$v);
            if($validator->fails()){
                Session::flash('errorMsg',$validator->errors());
                return redirect()->back();
            }else{


                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $product->image ='uploads/' . $filename;
                }
            }
        }
        $product->title  = $request->title;
        $product->price  = (double)$request->price;
        $product->product_code  = $request->code;
        $product->profit_unit  = (int)$request->price-(int)$request->p_price;
        $product->about  = $request->about;
        $product->stock  = (int)$request->stock;
        $product->status  = 1;
        $product->category_id  = (int)$request->category;
        $product->sub_category_id  = (int)$request->sub_category;
        // dd($product);
        if($product->save()){
            Session::flash('successMsg','Product updated successfully');
            return redirect()->route('product.index');
        }
        else{
            Session::flash('errorMsg','Product failed to update');
            return redirect()->back();
        }

    }

    public function edit($id){
        $product = Product::find($id);
        $category = Category::all();
        return view('backend.product.edit',compact('product','category'));
    }

    public function view($id){
        $product = Product::find($id);
        return view('backend.product.detail',compact('product'));
    }

    public function category(Request $request){
        $category = Category::find($request->category_id);
        $subcategory = $category->subcategory;
        return json_encode($subcategory);
    }
}
