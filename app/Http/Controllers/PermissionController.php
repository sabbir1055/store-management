<?php

namespace App\Http\Controllers;
use App\Permission;
use Session;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function index(){
      $permission = Permission::all();
      return view('backend.permission.permissions',compact('permission'));
    }

    public function create(){
      return view('backend.permission.permissioncreate');
    }

    public function store(Request $request){
      $permission = new Permission();
      $permission->name         = $request->name;
      $permission->display_name = $request->display_name;
      $permission->description  = $request->detail;
      if($permission->save()){
        Session::flash('successMsg','Permission added.');
      }else{
        Session::flash('errorMsg','Permission failed to add.');
      }

      return redirect()->route('permissions');
    }

    public function delete($id){
      if(Permission::find($id)->delete()){
        Session::flash('successMsg','Permission deleted.');
      }else{
        Session::flash('errorMsg','Permission failed to delete.');
      }
      return redirect()->route('permissions');
    }
}
