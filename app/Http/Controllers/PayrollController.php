<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use DB;
use DateTime;
use App\User;
use App\Sales;
use App\Payroll;
use Illuminate\Http\Request;

class PayrollController extends Controller
{
    public function index(){
        return view('backend.payroll.index');
    }

    public function data(Request $request){
        $columns = array(
          0 => 'id',
          1 => 'name',
          2 => 'total',
          3 => 'deduction',
          4 => 'total_pay',
          5 => 'created_at',
          6 => 'created_by',
          7 => 'status',
        );

        $total_data = Payroll::count();

        $limit = $request->length;
        $start = $request->start;
        $order = $columns[$request->input('order.0.column')];
		    $dir = $request->input('order.0.dir');


        if(empty($request->input('search.value'))){
    			$employee_data = Payroll::offset($start)
      					->limit($limit)
      					->orderBy($order,$dir)
      					->get();
      			$totalFiltered = Payroll::count();
      		}else{

      			$search = $request->input('search.value');
      			$employee_data = Payroll::Where('total','like',"%{$search}%")
                                    ->where('total_pay','like',"%{$search}%")
                                    ->where('deduction','like',"%{$search}%")
                                    ->orWhereHas('employee', function($q) use($search){
                                            $q->where('name', 'like', "%{$search}%");
                                        }
                                    )
                                    ->orWhereHas('createdBy', function($q) use($search){
                                            $q->where('name', 'like', "%{$search}%");
                                        }
                                    )
          							->offset($start)
          							->limit($limit)
          							->orderBy($order, $dir)
          							->get();
      			$totalFiltered = Payroll::Where('total','like',"%{$search}%")
                                    ->where('total_pay','like',"%{$search}%")
                                    ->where('deduction','like',"%{$search}%")
                                    ->orWhereHas('employee', function($q) use($search){
                                            $q->where('name', 'like', "%{$search}%");
                                        }
                                    )
                                    ->orWhereHas('createdBy', function($q) use($search){
                                            $q->where('name', 'like', "%{$search}%");
                                        }
                                    )
      							    ->count();
    		}

        $data           = [];
        $i = 1;
        if($employee_data){
          foreach ($employee_data as $key => $value) {
            $nestedData           = [];
            $nestedData['#']      = $i;
            $nestedData['name']   = $value->employee ? $value->employee->data->name : 'null';
            $nestedData['total']   = $value->total;
            $nestedData['deduction']  = $value->deduction;
            $nestedData['total_pay'] = $value->total_pay;
            $nestedData['created_at'] = $value->created_at->format('Y-m-d H:i:s');
            $nestedData['created_by'] = $value->createdBy ? $value->createdBy->data->name : 'null';
              $nestedData['action'] ='<a href="#" class="btn btn-danger" onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
                                      <a href="#" class="btn btn-info" onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
            $data[]                 = $nestedData;
            $i+=1;
          }
        }
        $json_data = array(
          "draw"			       => intval($request->input('draw')),
    			"recordsTotal"	   => intval($total_data),
    			"recordsFiltered"  => intval($totalFiltered),
    			"data"			       => $data

        );
        //dd(json_encode($json_data));
        return json_encode($json_data);
    }

    public function create(){
        $employee = User::where('status',1)->get();
        return view('backend.payroll.create',compact('employee'));
    }

    public function store(Request $request){
        $month  = date('m');
        $year   = date('Y');
        $payroll = Payroll::whereMonth('created_at',$month)
                            ->whereYear('created_at',$year)
                            ->where('user_id',$request->user_id)
                            ->first();
        if($payroll){
            Session::flash('errorMsg','Sallery sheet already generated for this employee.');
        }else{
            $payroll = new Payroll();

            $payroll->user_id = $request->user_id;
            $payroll->basic_sallery = $request->basic_sallery;
            $payroll->home = $request->home;
            $payroll->medical = $request->medical;
            $payroll->other = $request->other;
            $payroll->commission = $request->comission;
            $payroll->deduction = $request->deduction;
            $payroll->total = $request->total;
            $payroll->created_by = Auth::user()->id;
            $payroll->total_pay = $request->total_pay;

            if($payroll->save()){
                Session::flash('successMsg','Sallery storeSheet added.');
            }else{
                Session::flash('errorMsg','Sallery sheet store failed to add.');
            }
        }
    }

    public function getEmployeeData(Request $request){
        $total_sales    = Sales::where('saller_id',$request->employee_id)->sum('total');
        $total_profit   = Sales::where('saller_id',$request->employee_id)->sum('profit');
        // $employee_data = [];
        $employee       = User::where('id',$request->employee_id)->get()->map(function ($employee)  use($total_sales,$total_profit) {
                            return [
                                'id'            => $employee->id,
                                'email'         => $employee->email,
                                'name'          => $employee->data->name,
                                'designation'   => $employee->designationdata[0]->title,
                                'role'          => $employee->role->display_name,
                                'basic'         => $employee->designationdata[0]->basic_sallery,
                                'medical'       => $employee->designationdata[0]->medical,
                                'home'         => $employee->designationdata[0]->home,
                                'other'         => $employee->designationdata[0]->other,
                                'commission' => ceil(($total_sales > 600) ? (($total_profit*5)/100) : 0 )
                            ];
                        });
        return json_encode($employee);

    }

    public function edit($id){
        $payroll = Payroll::find($id);
        return view('backend.payroll.edit',compact('payroll'));
    }

    public function update(Request $request,$id){

    }

    public function view($id){
        $payroll = Payroll::find($id);
        return view('backend.payroll.view',compact('payroll'));
    }

}
