<?php

namespace App\Http\Controllers\Auth;
use Auth;
use App\User;
use Session;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function index(){
        return view('backend.login');
    }

    public function login(Request $request){
        $v = array(
            'email'       =>'required|email',
            'password'    =>'required|min:6',
        );
        $validator = Validator::make($request->all(), $v);
        if ($validator->fails()) {
            Session::flash('errorMsg','Please check your inuput data.');
            return redirect()->route('login');
        }else{
            $credentials    = $request->only('email', 'password');
            if (Auth::attempt($credentials)) {
              if (Auth::user()->status == '1') {
                return redirect()->route('dashboard');
              }else{
                Auth::logout();
                Session::flash('errorMsg','You are block for some immoral work. Please contact us for detail');
                return redirect()->route('login');
              }
            }
            else{
                Session::flash('errorMsg','User name or password incorrect.');
                return redirect()->route('login');
            }
        }
    }

    public function logout(){
        if(Auth::check()){
            Auth::logout();
            return redirect()->route('login.form');
        }
    }
}
