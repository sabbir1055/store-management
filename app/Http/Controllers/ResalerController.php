<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\Commision;
use App\Resaler;
use Illuminate\Http\Request;

class ResalerController extends Controller
{
    public function index(){
        return view('backend.resaler.index');
    }
    public function data(Request $request){
        $columns = array(
        0 => 'id',
        1 => 'name',
        2 => 'email',
        3 => 'contact',
        4 => 'district',
        5 => 'area',
        6 => 'action',
      );

      $total_data = Resaler::count();

      $limit = $request->length;
      $start = $request->start;
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');


      if(empty($request->input('search.value'))){
        $employee_data = Resaler::offset($start)
              ->limit($limit)
              ->orderBy($order,$dir)
              ->get();
          $totalFiltered = Resaler::count();
        }else{
          $search = $request->input('search.value');
          $employee_data = Resaler::where('id', 'like', "%{$search}%")
                  ->orWhere('name','like',"%{$search}%")
                  ->orWhere('email','like',"%{$search}%")
                  ->orWhere('contact','like',"%{$search}%")
                  ->orWhere('district','like',"%{$search}%")
                  ->orWhere('area','like',"%{$search}%")
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy($order, $dir)
                  ->get();
          $totalFiltered = Resaler::where('id', 'like', "%{$search}%")
                  ->orWhere('name','like',"%{$search}%")
                  ->orWhere('email','like',"%{$search}%")
                  ->orWhere('contact','like',"%{$search}%")
                  ->orWhere('district','like',"%{$search}%")
                  ->orWhere('area','like',"%{$search}%")
                  ->count();
      }

      $data           = [];
      $i = 1;
      if($employee_data){
        foreach ($employee_data as $key => $value) {
          $nestedData               = [];
          $nestedData['id']         = $value->id;
          $nestedData['name']       = $value->name;
          $nestedData['email']      = $value->email;
          $nestedData['contact']    = $value->contact ;
          $nestedData['district']   = $value->district ;
          $nestedData['area']       = $value->area ;
          if($value->status){
            $nestedData['action'] ='<a href="#" class="btn btn-danger" onclick="status('.$value->id.',0)"> <i class="fa fa-lock"></i> </a>
            <a href="#" class="btn btn-warning"  onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
            <a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
          }else{
            $nestedData['action'] ='<a href="#" class="btn btn-success"  onclick="status('.$value->id.',1)"> <i class="fa fa-check"></i> </a>
            <a href="#" class="btn btn-warning"  onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
            <a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
          }
          // dd($nestedData);
          $data[]                 = $nestedData;
          $i+=1;
        }
      }
      $json_data = array(
        "draw"			       => intval($request->input('draw')),
        "recordsTotal"	     => intval($total_data),
        "recordsFiltered"    => intval($totalFiltered),
        "data"			       => $data

      );
      return json_encode($json_data);
    }
    public function create(){
        return view('backend.resaler.create');
    }
    public function store(Request $request){
        $resaler = new Resaler();
        $v = [] ;
        if($request->has('image')){
            $v = [
                'image' => 'image|mimes:jpg,jpeg,png|max:5500',
            ];
        }
        $validator  = Validator::make($request->all(),$v);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            $resaler->name = $request->name;
            $resaler->email = $request->email;
            $resaler->contact = $request->contact;
            $resaler->district = $request->district;
            $resaler->village = $request->village;
            $resaler->area = $request->area;
            if($request->has('image')){
                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $resaler->image ='uploads/' . $filename;
                }
            }
            if($resaler->save()){
                Session::flash('successMsg','Resaler added.');
                return redirect()->route('resaler.index');
            }
            else{
                Session::flash('successMsg','Resaler failed to add.');
                return redirect()->route('resaler.index');
            }
        }
    }
    public function edit($id){
        $resaler = Resaler::find($id);
        return view('backend.resaler.edit',compact('resaler'));
    }
    public function update(Request $request,$id){
        $resaler = Resaler::find($id);
        $v = [] ;
        if($request->has('image')){
            $v = [
                'image' => 'image|mimes:jpg,jpeg,png|max:5500',
            ];
        }
        $validator  = Validator::make($request->all(),$v);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            $resaler->name = $request->name;
            $resaler->email = $request->email;
            $resaler->contact = $request->contact;
            $resaler->district = $request->district;
            $resaler->village = $request->village;
            $resaler->area = $request->area;
            if($request->has('image')){
                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $resaler->image ='uploads/' . $filename;
                }
            }
            if($resaler->save()){
                Session::flash('successMsg','Resaler updated.');
                return redirect()->route('resaler.index');
            }
            else{
                Session::flash('successMsg','Resaler failed to update.');
                return redirect()->route('resaler.index');
            }
        }
    }

    public function status(Request $request){
        $employee = Resaler::find($request->resaler_id);
        if($employee){
          if($employee->status){
            $employee->status = 0;
            if($employee->save()){
                Session::flash('successMsg','Resaler status change to deactive.');
              }
              else{
                Session::flash('errorMsg','Resaler status failed change.');
              }
          }
          else{
            $employee->status = 1;
            if($employee->save()){
              Session::flash('successMsg','Resaler status change to active.');
               }
               else{
                 Session::flash('errorMsg','Resaler status failed change.');
               }
          }
        }
    }

    public function view($id){
        $resaler = Resaler::find($id);
        return view('backend.resaler.view',compact('resaler'));
    }
}
