<?php

namespace App\Http\Controllers;
use App\UserData;
use App\User;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use DB;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(){
        return view('backend.profile.index');
    }

    public function edit(){
        return view('backend.profile.edit');
    }

    public function update(Request $request){
        $v = [
            'image' => 'image|mimes:jpeg,png,jpg'
        ];
        if($request->password != null){
            $v = [
                'image'     => 'image|mimes:jpeg,png,jpg',
                'password'  => 'min:8'
            ];
        }
        $validator  = Validator::make($request->all(),$v);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            if($request->password != null){
                $user = User::find(Auth::user()->id);
                $user->password = bcrypt($request->password);
                $user->save();
                // dd($user);
            }
            $image = Auth::user()->data->image;
            if($request->has('image')){
                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $image ='uploads/' . $filename;
                }
            }
            // dd(Auth::user()->id);
            Auth::user()->data->update(['name'=>$request->name,
                                              'msisdn'=>$request->msisdn,
                                              'b_date'=>$request->b_date,
                                              'address'=>$request->address,
                                              'gender'=>$request->gender,
                                              'b_group'=>$request->b_group,
                                              'image' => $image]);
            Session::flash('successMsg','Profile updated successfully');
            return redirect()->route('profile.index');
        }

    }
}
