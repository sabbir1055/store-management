<?php

namespace App\Http\Controllers;

use DateTime;
use App\Sales;
use App\Item;
use DB;
use PDF;
use App;
use Session;
use Auth;
// use App\NumberToWord;
Use App\Import;
use App\Product;
use carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $lastday = date_create(date('Y-m-d', strtotime('-1 day', strtotime(\carbon\carbon::now()))));
        $last_month = date_create(date('Y-m-d', strtotime('last day of previous month')));
        $last_month = $last_month->format('m');
        $month = date('m');
        $sales = Sales::orderBy('total','desc')->take(5)->get();
        $product = Product::count();
        $profit = Sales::sum('profit');
        $sold_total = Sales::sum('total');
        $sold_today = Sales::whereDate('created_at', Carbon::today())->sum('total');
        $sold_lastday = Sales::whereDate('created_at',Carbon::yesterday())->sum('total');
        $sold_month = Sales::whereMonth('created_at', $month)->sum('total');
        $sold_lastmonth = Sales::whereMonth('created_at', $last_month)->sum('total');
        $sold   = Item::where('type',1)->count();
        $stock  = Product::sum('stock');
        $import = Import::sum('total');
        $resaler   = Item::where('type',2)->count();
        $returned = Item::where('type',3)->count();
        $damaged = Item::where('type',4)->count();
        $recovered = Item::where('type',5)->count();
        $today_growth = (($sold_today - $sold_lastday)/(($sold_lastday != 0) ? $sold_lastday : 1 ))*100;
        $monthly_growth = (($sold_month - $sold_lastmonth)/(($sold_lastmonth != 0) ? $sold_lastmonth : 1 ))*100;
        $sales_chart    = $this->getDailyChart();
        // dd($sales_chart);
        return view('backend.index',compact('sales','product','profit',
        'sold','resaler','returned',
        'damaged','recovered','sold_today',
        'import','stock','sold_month','sold_total',
        'today_growth','monthly_growth',
        'sales_chart'));
    }

    public static function getDailyChart(){
        $data = DB::table('sales')->select(
            DB::raw('date(created_at) as date'),
            DB::raw('sum(total) as sold'),
            DB::raw('sum(profit) as profit'))
            ->groupBy('date')
            ->get();
            $array = [];
            // $array[] = ['Date','Sold','Profit'];
            foreach($data as $key => $value){
                $array[$key] = [$value->date, $value->sold,$value->profit];
            }
            return $array;
        }

        public function salesDataDate(Request $request){
            $array = [];
            if($request->has('date')){
                $date_time  = date_create($request->date);
                $sales      = Sales::whereDate('created_at',$date_time)
                ->get();
                // dd($sales);
                $array[] = ["Name", "Sell", "Profit"];
                foreach ($sales as $key => $value) {
                    $array[++$key] = [$value->employee->data->name, $value->total,$value->profit];
                }
            }
            if($request->has('name')){
                $name   = $request->name;
                $sales      = Sales::WhereHas('employeedata', function($q) use($name){
                    $q->where('name',$name);
                }
                )
                ->get();
                foreach ($sales as $key => $value) {
                    $array[$key] = [substr(date($value->created_at),0,10), $value->total,$value->profit];
                }
            }
            return json_encode($array);
        }
    }
