<?php

namespace App\Http\Controllers;
use Auth;
use Session;
use DB;
use Image;
use App\SubCategory;
use App\Category;
use Validator;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index(){
        $category = SubCategory::all();
        return view('backend.subcategory.index',compact('category'));
    }

    public function create(){
        $category = Category::all();
        return view('backend.subcategory.create',compact('category'));
    }

    public function store(Request $request){
        $category = new SubCategory();
        $v = [
            'image' => 'image|mimes:png,ico|max:200',
        ];
        $validator  = Validator::make($request->all(),$v);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            $category->title        = $request->title;
            $category->category_id  = $request->category_id;
            if($request->has('image')){
                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $category->image ='uploads/' . $filename;
                }
            }
            if($category->save()){
                Session::flash('successMsg','SubCategory added.');
                return redirect()->route('subcategory.index');
            }
            else{
                Session::flash('successMsg','SubCategory failed to add.');
                return redirect()->route('category.index');
            }
        }
    }
    public function edit($id){
        $subcategory = SubCategory::find($id);
        $category  = Category::all();
        return view('backend.subcategory.edit',compact('subcategory','category'));
    }
    public function update(Request $request,$id){
        $category = SubCategory::find($id);
        if($request->has('image')){
            $v = [
                'image' => 'image|mimes:png,ico|max:200',
            ];
            $validator  = Validator::make($request->all(),$v);
            if($validator->fails()){
                Session::flash('errorMsg',$validator->errors());
                return redirect()->back();
            }else{
                    if ($request->file('image')) {
                        $avatar = $request->file('image');
                        $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                        Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                        $category->image ='uploads/' . $filename;
                    }
            }
        }
        $category->title = $request->title;
        $category->category_id = $request->category_id;
        if($category->save()){
            Session::flash('successMsg','SubCategory updated.');
            return redirect()->route('subcategory.index');
        }else{
            Session::flash('successMsg','SubCategory failed to update.');
            return redirect()->route('subcategory.index');
        }
    }
    public function delete($id){
        $subcategory = SubCategory::find($id);
        if($subcategory->delete()){
            Session::flash('successMsg','SubCategory deleted.');
            return redirect()->route('category.index');
        }else{
            Session::flash('errorMsg','SubCategory faield to delete.');
            return redirect()->route('category.index');
        }
    }
}
