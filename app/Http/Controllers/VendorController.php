<?php

namespace App\Http\Controllers;

use Auth;
use URL;
use Session;
use Validator;
use Image;
use App\Vendor;
use App\VendorProduct;
use App\Repositories\VendorRepository;

use Illuminate\Http\Request;

class VendorController extends Controller
{
    private $vendor;
    public function __construct(VendorRepository $vendor){
        $this->vendor = $vendor;
    }
    public function index(){
        return view('backend.vendor.index');
    }
    public function create(){
        return view('backend.vendor.create');
    }
    public function store(Request $request){
        $v = [
            'name' => 'required',
            'msisdn' => 'required|digits:10',
            'email' => 'required|email|unique:vendors',
            'image' => 'image|mimes:jpeg,png,jpg|max:5000',
        ];
        $message = [
            'name.required' => 'Vendor name needed',
            'msisdn.required' => 'Vendor mobile number needed',
            'msisdn.digits:10' => 'Vendor mobile number must be 10 digits',
            'email.required' => 'Vendor email required',
            'email.email' => 'Vendor email must be valid',
            'email.unique:vendors' => 'Vendor email already taken',
            'image.image' => 'File must be vali image',
            'image.mimes:jpeg,png,jpg' => 'Image file must be jpeg,png,jpg',
            'image.max:5000' => 'Image size maximum 5Mb',
        ];
        $validator  = Validator::make($request->all(),$v,$message);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            $user = new Vendor();
            if($request->has('image')){
                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $user->image ='uploads/' . $filename;
                }
            }
            $user->name  = $request->name;
            $user->email  = $request->email;
            $user->msisdn  = '880'.$request->msisdn;
            $user->address  = $request->address;
            $user->website  = $request->website;
            if($user->save()){
                Session::flash('successMsg','Vendor added successfully');
                return redirect()->route('vendor.index');
            }
            else{
                Session::flash('errorMsg','Vendor failed to add');
                return redirect()->back();
            }
        }

    }

    public function data(Request $request){
        // dd($request->input('order.0.column'));
        $columns = array(
        0 => 'id',
        1 => 'name',
        2 => 'email',
        3 => 'msisdn',
        4 => 'address',
        5 => 'status',
        6 => 'action',
      );

      $total_data = Vendor::count();

      $limit = $request->length;
      $start = $request->start;
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');


      if(empty($request->input('search.value'))){
        $employee_data = Vendor::offset($start)
              ->limit($limit)
              ->orderBy($order,$dir)
              ->get();
          $totalFiltered = Vendor::count();
        }else{
          $search = $request->input('search.value');
          $employee_data = Vendor::where('address', 'like', "%{$search}%")
                  ->orWhere('name','like',"%{$search}%")
                  ->orWhere('email','like',"%{$search}%")
                  ->orWhere('msisdn','like',"%{$search}%")
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy($order, $dir)
                  ->get();
          $totalFiltered = Vendor::where('address', 'like', "%{$search}%")
                  ->orWhere('name','like',"%{$search}%")
                  ->orWhere('email','like',"%{$search}%")
                  ->orWhere('msisdn','like',"%{$search}%")
                  ->count();
      }

      $data           = [];
      $i = 1;
      if($employee_data){
        foreach ($employee_data as $key => $value) {
          $nestedData           = [];
          $nestedData['#']                = $i;
          $nestedData['name']             = $value->name;
          $nestedData['email'] = $value->email;
          $nestedData['msisdn'] = $value->msisdn;
          if($value->status){
              $nestedData['status'] = 'Active';
          }else{
              $nestedData['status'] = 'Deactive';
          }
          $nestedData['address']         = substr($value->address,0,15).'....' ;
          if($value->status){
            $nestedData['action'] ='<a href="#" class="btn btn-danger" onclick="status('.$value->id.',0)"> <i class="fa fa-lock"></i> </a>
            <a href="#" class="btn btn-warning"  onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
            <a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
          }else{
            $nestedData['action'] ='<a href="#" class="btn btn-success"  onclick="status('.$value->id.',1)"> <i class="fa fa-check"></i> </a>
            <a href="#" class="btn btn-warning"  onclick="edit('.$value->id.')"> <i class="fa fa-edit"></i> </a>
            <a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
          }
          // dd($nestedData);
          $data[]                 = $nestedData;
          $i+=1;
        }
      }
      $json_data = array(
        "draw"			       => intval($request->input('draw')),
        "recordsTotal"	   => intval($total_data),
        "recordsFiltered"  => intval($totalFiltered),
        "data"			       => $data

      );
      return json_encode($json_data);
    }

    public function status(Request $request){
        $vendor  = Vendor::find($request->vendor_id);
        // dd($vendor);
        $vendor->status = ($vendor->status == 1) ? 0 : 1 ;
        $vendor->save();
    }

    public function update(Request $request,$id){
        $v = [
            'name' => 'required',
            'msisdn' => 'required|digits:10',
            'email' => 'required|email',
            'image' => 'image|mimes:jpeg,png,jpg|max:5000',
        ];
        $validator  = Validator::make($request->all(),$v);
        if($validator->fails()){
            Session::flash('errorMsg',$validator->errors());
            return redirect()->back();
        }else{
            $user = Vendor::find($id);
            if($request->has('image')){
                if ($request->file('image')) {
                    $avatar = $request->file('image');
                    $filename = time() . rand(1111, 9999) . '.'  . $avatar->getClientOriginalExtension();
                    Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );

                    $user->image ='uploads/' . $filename;
                }
            }
            $user->name  = $request->name;
            // $user->email  = $request->email;
            $user->msisdn  = '880'.$request->msisdn;
            $user->address  = $request->address;
            $user->website  = $request->website;
            if($user->save()){
                Session::flash('successMsg','Vendor updated successfully');
                return redirect()->route('vendor.index');
            }
            else{
                Session::flash('errorMsg','Vendor failed to update');
                return redirect()->back();
            }
        }
    }

    public function edit($id){
        $vendor = Vendor::find($id);
        return view('backend.vendor.edit',compact('vendor'));
    }

    public function view($id){
        $vendor = Vendor::find($id);
        return view('backend.vendor.view',compact('vendor'));
    }

    public function detail(Request $request){
        $data['vendor']= Vendor::find($request->vendor_id);
        $data['product']= $data['vendor']->getProduct;
        return json_encode($data);
    }

    public function product(){
        $vendor = Vendor::all();
        return view('backend.vendor.productcreate',compact('vendor'));
    }

    public function getProuctData(Request $request){
        return json_encode(VendorProduct::where('vendor_id',$request->vendor_id)->get());
    }

    public function storeProduct(Request $request){
        $data = [];
        foreach ($request->product_title as $key => $value) {
            $data[$key]['vendor_id']        = $request->vendor_id;
            $data[$key]['product_title']    = $request->product_title[$key];
            $data[$key]['product_price']    = $request->product_price[$key];
            $data[$key]['product_code']     = $request->product_code[$key];
            $data[$key]['created_by']       = Auth::user()->id ?? 1;
        }
        $product = VendorProduct::where('vendor_id',$request->vendor_id)->get();
        if(count($product)){
            if(VendorProduct::where('vendor_id',$request->vendor_id)->delete()){
                VendorProduct::insertIgnore($data);
                Session::flash('successMsg','Vendor products added successfully');
                return redirect()->back();
            }else{
                Session::flash('errorMsg','Vendor product failed to add');
                return redirect()->back();
            }
        }
        else{
            VendorProduct::insertIgnore($data);
            Session::flash('successMsg','Vendor products added successfully');
            return redirect()->back();
        }

    }
}
