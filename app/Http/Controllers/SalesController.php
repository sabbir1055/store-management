<?php

namespace App\Http\Controllers;
use Auth;
use URL;
use Session;
use Validator;
use Image;
use View;
use DB;
use App\Product;
use App\Item;
use App\Resaler;
use App\Category;
use App\Import;
use App\Sales;
use Illuminate\Http\Request;
use App;

class SalesController extends Controller
{
    public function index(){
        // $sales = Sales::with('employeedata','employee')->get();
        return view('backend.sell.index');
        // return View::make('backend.sell.index')->nest('invoice','backend.sell.invoice');
    }

    public function data(Request $request){
        $columns = array(
        0 => 'id',
        1 => 'saller_id',
        2 => 'resaller_id',
        3 => 'total',
        4 => 'paid',
        5 => 'due',
        6 => 'action',
      );

      $total_data = Sales::count();

      $limit = $request->length;
      $start = $request->start;
      $order = $columns[$request->input('order.0.column')];
      $dir = $request->input('order.0.dir');


      if(empty($request->input('search.value'))){
        $employee_data = Sales::with('employeedata','employee','resaler')->offset($start)
              ->limit($limit)
              ->orderBy($order,$dir)
              ->get();
          $totalFiltered = Sales::count();
        }else{
          $search = $request->input('search.value');
          $employee_data = Sales::orWhereHas('employeedata', function($q) use($search){
                            $q->where('name', 'like', "%{$search}%");
                        }
                        )
                        ->orWhereHas('resaler', function($q) use($search){
                                          $q->where('name', 'like', "%{$search}%");
                                }
                        )
                        ->where('id', 'like', "%{$search}%")
                        ->orWhere('saller_id','like',"%{$search}%")
                        ->orWhere('total','like',"%{$search}%")
                        ->orWhere('paid','like',"%{$search}%")
                        ->orWhere('due','like',"%{$search}%")
                  ->offset($start)
                  ->limit($limit)
                  ->orderBy($order, $dir)
                  ->get();
          // $employee_data = Sales::with('employeedata','employee','resaler')->where('id', 'like', "%{$search}%")
          //         ->orWhere('saller_id','like',"%{$search}%")
          //         ->orWhere('total','like',"%{$search}%")
          //         ->orWhere('paid','like',"%{$search}%")
          //         ->orWhere('due','like',"%{$search}%")
          //         ->offset($start)
          //         ->limit($limit)
          //         ->orderBy($order, $dir)
          //         ->get();
          // dd($employee_data);
          $totalFiltered = Sales::with('employeedata','employee','resaler')->where('id', 'like', "%{$search}%")
                  ->orWhere('saller_id','like',"%{$search}%")
                  ->orWhere('total','like',"%{$search}%")
                  ->orWhere('paid','like',"%{$search}%")
                  ->orWhere('due','like',"%{$search}%")
                  ->count();
      }

      $data           = [];
      $i = 1;
      if($employee_data){
        foreach ($employee_data as $key => $value) {
          $nestedData           = [];
          $nestedData['show']    = `<i class="icon-edit"></i>`;
          $nestedData['id']    = $value->id;
          $nestedData['saller_id']    = $value->employeedata ? $value->employee->data->name : $value->employee->email;
          $nestedData['resaller_id']  = $value->resaler ? $value->resaler->name : "Direct sold";
          $nestedData['products']  = $value->products;
          // dd(json_decode($value->products));
          $nestedData['paid']         = $value->paid ;
          $nestedData['due']         = $value->due ;
          $nestedData['total']         = $value->total ;
          if($value->status){
            $nestedData['action'] ='<a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
          }else{
            $nestedData['action'] ='<a href="#" class="btn btn-info"  onclick="view('.$value->id.')"> <i class="fa fa-eye"></i> </a>';
          }
          // dd($nestedData);
          $data[]                 = $nestedData;
          $i+=1;
        }
      }
      $json_data = array(
        "draw"			       => intval($request->input('draw')),
        "recordsTotal"	       => intval($total_data),
        "recordsFiltered"      => intval($totalFiltered),
        "data"			       => $data

      );
      return json_encode($json_data);
    }

    public function create(){
        $resaler = Resaler::where('status',1)->get();
        return view('backend.sell.create',compact('resaler'));
    }

    public function store(Request $request){
        try{
            DB::beginTransaction();
            $quantity   = 0;
            $profit     = 0;
            $sales = new Sales();
            $sales->resaller_id = $request->resaler_id ?? 0;
            $sales->total = $request->total_payment;
            $sales->paid = $request->total_paid;
            $sales->due = $request->total_due;
            $sales->saller_id = Auth::user()->id ?? 1;
            $sales->is_on_cash = ($request->total_due==0) ? 1 : 0;
            $products = [];
            foreach ($request->product_code as $key => $value) {
                $quantity   = (int)$request->product_qty[$key];
                $item = Item::where('product_code',$value)
                        ->where('type',0)
                        ->orderby('created_at','asc')
                        ->take($request->product_qty[$key])->get();
                $product = Product::where('product_code',$value)->where('stock','>',0)->get();
                foreach ($product as $i => $p) {
                    $stock = (int)$p->stock;
                    // dd((int)$p->stock);
                    if($stock > $quantity){
                            $p->decrement('stock',$quantity);
                            $profit   = $profit + ($quantity * $p->profit_unit);
                            $quantity = $quantity - $quantity;
                        }
                    else{
                        $p->decrement('stock',$stock);
                        $quantity = $quantity-$stock;
                        $profit   = $profit + ($stock * $p->profit_unit);
                    }
                }
                if($quantity > 0){
                    DB::rollback();
                    Session::flash('errorMsg','Product quantity get out of stock.');
                    return redirect()->back();
                }
                if($request->resaler_id != null){
                    $item->each(function ($i) use($request){
                        $i->update(['type'=>2,'resaler_id'=>$request->resaler_id]);
                    });
                }else{
                    $item->each(function ($i){
                        $i->update(['type'=>1]);
                    });
                }
                $products [$key] = $item;
            }
            $sales->products = json_encode($products);
            $sales->profit   = $profit;
            $sales->save();
            DB::commit();
            Session::flash('successMsg','Product sold successfully.');
            Session::flash('downloadPdf',json_encode($request->all()));
            // $rec = view('backend.sell.invoice');
            // $pdf = App::make('dompdf.wrapper');
            // $pdf->loadHTML($rec);
            // return $pdf->stream();
            return redirect()->route('sell.index');
        }
        catch(\Exceptoin $e){
            DB::rollback();
            Session::flash('errorMsg','Product sold faield.');
            return redirect()->route('sell.index');
        }

    }

    public function edit($id){

    }

    public function update(Request $request,$id){

    }

    public function view($id){

    }

    public function product(Request $request){
        $product = Product::Distinct('product_code')
            ->select('product_code','title')
            ->get();
            // dd($product);
        $data = [];
        foreach ($product as $key => $value) {
            $data[$key]['product_code'] = $value->product_code;
            $data[$key]['title'] = $value->title;
            $data[$key]['stock'] = $value->stock($value->product_code);
            $data[$key]['price'] = $value->price($value->product_code)->price;
        }
        return json_encode($data);
    }

    public function downloadPdf(Request $request){
        dd($request->all());

        // $data = view('backend.sell.invoice');
        // dd($data);
        // $pdf = App::make('dompdf.wrapper');
        // $pdf->loadHTML($data);
        // return $pdf->download();
    }
}
