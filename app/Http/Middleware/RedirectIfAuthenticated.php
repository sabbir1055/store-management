<?php

namespace App\Http\Middleware;
use URL;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(URL::previous() != URL::to('/') ){
                return redirect()->back();
            }else{
                if(URL::current() == URL::to('/')){
                    return redirect()->route('dashboard');
                }else{
                    return redirect()->back();
                }
            }
        }

        return $next($request);
    }
}
