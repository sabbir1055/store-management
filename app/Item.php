<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
            'type','resaller_id'
    ];
    public function product(){
        return $this->hasMany(Product::class,'product_code','product_code')->latest();
    }
}
