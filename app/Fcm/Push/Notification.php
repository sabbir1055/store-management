<?php

namespace App\Fcm\Push;

use Exception;
use GuzzleHttp\Client;

class Notification{

    private $recipients = [];
    private $topics = [];
    private $data = [];
    private $title;
    private $apiToken;
    private $body;

    public function __construct(string $title = '', string $body = '', string $recipient = '')
    {
        $this->apiToken  = env("server_key", "somedefaultvalue");
        $this->title = $title;
        $this->body = $body;
        if (!empty($recipient)) {
            $this->addRecipient($recipient);
        }
    }

    public function setTitle(string $title){
        $this->title = $title;
        return $this;
    }

    public function setBody(string $body){
        $this->body = $body;
        return $this;
    }

    public function getBody(){
        if (empty($this->recipients) && empty($this->topics)) {
            throw new Exception('Must minimaly specify a single recipient or topic.');
        }
        if (!empty($this->recipients) && !empty($this->topics)) {
            throw new Exception('You cannot use both recipient and topic.You can use only one.');
        }
        $request = [];
        if (!empty($this->recipients)) {
            if (\count($this->recipients) === 1) {
                $request['to'] = current($this->recipients);
            } else {
                $request['registration_Ids'] = $this->recipients;
            }
        }

        if (!empty($this->topics)) {
            $request['condition'] = array_reduce($this->topics, function ($carry, string $topic) {
                $topicSyntax = "'%s' in topics";
                if (end($this->topics) === $topic) {
                    return $carry .= sprintf($topicSyntax, $topic);
                }
                return $carry .= sprintf($topicSyntax, $topic) . '||';
            });
        }
        if (!empty($this->data)) {
            $request['data'] = $this->data;
        }
        $request['notification']['title'] = $this->title;
        $request['notification']['body'] = $this->body;
        return $request;
    }

    public function addRecipient($iidToken){
        if (\is_string($iidToken)) {
            $this->recipients[] = $iidToken;
        }
        if (\is_array($iidToken)) {
            $this->recipients = array_merge($this->recipients, $iidToken);
        }
        return $this;
    }

    public function addTopic($topic){
        if (\is_string($topic)) {
            $this->topics[] = $topic;
        }
        if (\is_array($topic)) {
            $this->topics = array_merge($this->topics, $topic);
        }
        return $this;
    }

    public function addData($name, $value){
        $this->data[$name] = $value;
        return $this;
    }
    public function getGuzzleClient(){
        return new Client([
            'headers' => [
                'Authorization' => 'key='.$this->apiToken
            ]
        ]);
    }

    public function send(){
        $client = $this->getGuzzleClient();
        $url = 'https://fcm.googleapis.com/fcm/send';
        $response = $client->post($url, [
            'json' => $this->getBody()
        ]);
        $body = json_decode($response->getBody()->getContents(), true);
        if ($body === null || json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Failed to json decode response body: '.json_last_error_msg());
        }

        return $body;
    }
}
