<?php

namespace App\Fcm\Device;

use Exception;
use GuzzleHttp\Client;

class Info{

    private $deviceId;
    private $apiToken;
    private $details;

    public function __construct(string $deviceId, bool $details = false){
        $this->apiToken  = env("server_key", "somedefaultvalue");
        $this->deviceId = $deviceId;
        $this->details = $details;
    }

    public function getUrl(){
        $url = "https://iid.googleapis.com/iid/info/{$this->deviceId}";
        if ($this->details) {
            $url .= '?details=true';
        }
        return $url;
    }

    public function getBody(){
        return [];
    }

    public function getGuzzleClient(){
        return new Client([
            'headers' => [
                'Authorization' => 'key='.$this->apiToken
            ]
        ]);
    }
    public function send(){
        $client = $this->getGuzzleClient();
        $url = $this->getUrl();
        $response = $client->post($url, [
            'json' => $this->getBody()
        ]);
        $body = json_decode($response->getBody()->getContents(), true);
        if ($body === null || json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Failed to json decode response body: '.json_last_error_msg());
        }
        return $body;
    }
}
