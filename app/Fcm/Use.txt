Copy this folder inside App folder. And in .env add server_key = firebase server key.

1/Get device information

use App\Fcm\Device\Info;

    $client = new Info($deviceId, true);

    $response = $client->send();

    //dd($response);

2/Send Notification


use App\Fcm\Push\Notification;

	$notification = new Notification();

        $notification->setTitle($data['title'])
                     ->setBody($data['desc'])
                     ->addTopic($title) //If use addRecipient no need this
		                 ->addRecipient($device_token)//If use addTopic no need this
                     ->addData('data',$data);//optional

	$response = $notification->send();

	//dd($response);

3/Subscribe and unsubscribe to topic

use App\Fcm\Topic\Subscription;

	$subscribe  = new Subscription('topic_name');
	$subscribe->addDevice('device_id');
	$response = $subscribe->send('subscribe');//To subscribe
	$response = $subscribe->send('unsubscribe');//To unsubscribe
