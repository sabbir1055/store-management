<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    public function resaler(){
        return $this->hasOne(Resaler::class,'id','resaller_id');
    }

    public function employee(){
        return $this->hasOne(User::class,'id','saller_id');
    }

    public function employeedata(){
        return $this->hasManyThrough(UserData::class,User::class,'id','user_id','saller_id');
    }

}
