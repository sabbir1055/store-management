<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    public function employee(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function createdBy(){
        return $this->hasOne(User::class,'id','created_by');
    }
}
