<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    protected $fillable=[
        'name','msisdn','b_date','b_group','image','address'
    ];
}
