<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class VendorProduct extends Model
{

    public static function insertIgnore($arrayOfArrays) {
        $static = new static();
        $table = with(new static)->getTable();
        $questionMarks = '';
        $values = [];
        foreach ($arrayOfArrays as $k => $array) {
            if ($static->timestamps) {
                $now = \Carbon\Carbon::now();
                $arrayOfArrays[$k]['created_at'] = $now;
                $arrayOfArrays[$k]['updated_at'] = $now;
                if ($k > 0) {
                    $questionMarks .= ',';
                }
                $questionMarks .= '(?' . str_repeat(',?', count($array) - 1) . ')';
                $values = array_merge($values, array_values($array));
            }
        }
        $query = 'INSERT IGNORE INTO ' . $table . ' (' . implode(',', array_keys($array)) . ') VALUES ' . $questionMarks;
        return DB::insert($query, $values);
        // return \Illuminate\Support\Facades\DB::insert($query, $values);
    }

    
}
