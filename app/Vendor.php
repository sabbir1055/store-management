<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    public function getProduct(){
        return $this->hasMany(VendorProduct::class,'vendor_id','id');
    }
}
