<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('top_nav')->default('#fff');
            $table->string('font_family')->default('"Source Sans Pro", sans-serif');
            $table->string('font_size')->default('15px');
            $table->string('brand')->default('#fff');
            $table->string('left_side')->default('#2B333E');
            $table->string('left_side_dropdown')->default('#252c35');
            $table->string('left_border_left_color')->default('#00AAFF');
            $table->string('left_dropdown_item_back')->default('#252c35');
            $table->string('top_nav_drop_background')->default('#fff');
            $table->string('top_nav_drop_background_hover')->default('#fff');
            $table->string('top_nav_drop_background_items')->default('#fff');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
    }
}
