<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->double('discount')->nullable();
            $table->DateTime('from')->nullable();
            $table->DateTime('to')->nullable();
            $table->integer('sales_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_offers');
    }
}
