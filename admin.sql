-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2019 at 06:11 PM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Education', 'uploads/15476563051527.png', '2019-01-16 10:31:46', '2019-01-16 10:31:46'),
(5, 'Drinks', 'uploads/15479735532624.png', '2019-01-20 02:39:14', '2019-01-20 02:39:53');

-- --------------------------------------------------------

--
-- Table structure for table `commisions`
--

CREATE TABLE `commisions` (
  `id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `commision` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `commisions`
--

INSERT INTO `commisions` (`id`, `amount`, `commision`, `created_at`, `updated_at`) VALUES
(2, 50000, 5, '2019-01-19 21:18:27', '2019-01-19 21:18:27');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `basic_sallery` float NOT NULL,
  `increment` float NOT NULL DEFAULT '0',
  `medical` float NOT NULL DEFAULT '0',
  `home` float NOT NULL DEFAULT '0',
  `leave` int(11) NOT NULL DEFAULT '30',
  `other` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `title`, `basic_sallery`, `increment`, `medical`, `home`, `leave`, `other`, `created_at`, `updated_at`) VALUES
(3, 'Manager', 22000, 15, 2000, 8000, 40, 0, '2019-01-17 22:26:58', '2019-01-17 22:26:58'),
(4, 'Assistant', 8000, 10, 1500, 4000, 20, 500, '2019-01-30 23:59:36', '2019-01-30 23:59:36');

-- --------------------------------------------------------

--
-- Table structure for table `imports`
--

CREATE TABLE `imports` (
  `id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `products` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` double NOT NULL,
  `paid` double NOT NULL,
  `due` double NOT NULL,
  `is_on_cash` int(11) NOT NULL,
  `reciept_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imports`
--

INSERT INTO `imports` (`id`, `vendor_id`, `products`, `total`, `paid`, `due`, `is_on_cash`, `reciept_id`, `status`, `created_at`, `updated_at`) VALUES
(7, 2, '[{\"title\":\"SPA Water\",\"code\":\"S-1000\",\"price\":\"25\",\"qty\":\"10\",\"import_identification\":\"S-100015492533023071\"},{\"title\":\"Pen\",\"code\":\"P-10\",\"price\":\"12\",\"qty\":\"25\",\"import_identification\":\"P-1015492533029664\"}]', 550, 550, 0, 1, 1, 1, '2019-02-03 22:08:22', '2019-02-03 22:08:22');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `import_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `resaler_id` int(11) NOT NULL,
  `Identeti_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` float NOT NULL,
  `import_identification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `product_id`, `product_code`, `import_id`, `type`, `resaler_id`, `Identeti_number`, `product_price`, `import_identification`, `created_at`, `updated_at`) VALUES
(22, 22, 'P-10', 4, 1, 0, '115479785392335', 0, '', '2019-01-20 04:02:19', '2019-01-29 03:56:00'),
(23, 22, 'P-10', 4, 1, 0, '215479785391214', 0, '', '2019-01-20 04:02:19', '2019-01-21 02:33:39'),
(24, 22, 'P-10', 4, 1, 0, '315479785395870', 0, '', '2019-01-20 04:02:19', '2019-01-21 02:40:34'),
(25, 22, 'P-10', 4, 1, 0, '415479785392888', 0, '', '2019-01-20 04:02:19', '2019-01-21 02:40:34'),
(26, 22, 'P-10', 4, 1, 0, '515479785397451', 0, '', '2019-01-20 04:02:19', '2019-01-21 02:40:34'),
(27, 22, 'P-10', 4, 1, 0, '615479785399046', 0, '', '2019-01-20 04:02:19', '2019-01-21 02:40:34'),
(28, 23, 'Spa-250', 4, 1, 0, '2315479785944663', 0, '', '2019-01-20 04:03:14', '2019-01-21 02:40:34'),
(29, 23, 'Spa-250', 4, 1, 0, '2415479785945509', 0, '', '2019-01-20 04:03:14', '2019-01-21 02:40:34'),
(30, 23, 'Spa-250', 4, 1, 0, '2515479785945076', 0, '', '2019-01-20 04:03:14', '2019-01-29 00:01:13'),
(31, 24, 'P-10', 5, 1, 0, '2915480450716502', 0, '', '2019-01-20 22:31:11', '2019-01-21 02:40:34'),
(32, 24, 'P-10', 5, 1, 0, '3015480450719915', 0, '', '2019-01-20 22:31:11', '2019-01-21 02:40:34'),
(33, 24, 'P-10', 5, 1, 0, '3115480450711793', 0, '', '2019-01-20 22:31:11', '2019-01-21 02:40:34'),
(34, 24, 'P-10', 5, 1, 0, '3215480450716068', 0, '', '2019-01-20 22:31:11', '2019-01-21 03:06:32'),
(35, 25, 'P-10', 6, 2, 0, '3215480616627513', 0, '', '2019-01-21 03:07:42', '2019-01-21 04:51:31'),
(36, 25, 'P-10', 6, 2, 0, '3315480616625147', 0, '', '2019-01-21 03:07:42', '2019-01-21 04:51:31'),
(37, 25, 'P-10', 6, 2, 0, '3415480616621143', 0, '', '2019-01-21 03:07:42', '2019-01-21 04:51:31'),
(38, 25, 'P-10', 6, 2, 0, '3515480616628475', 0, '', '2019-01-21 03:07:42', '2019-01-21 04:51:31'),
(39, 25, 'P-10', 6, 2, 0, '3615480616625663', 0, '', '2019-01-21 03:07:42', '2019-01-21 04:51:31'),
(40, 25, 'P-10', 6, 2, 0, '3715480616629023', 0, '', '2019-01-21 03:07:42', '2019-01-21 04:51:31'),
(41, 25, 'P-10', 6, 2, 0, '3815480616624817', 0, '', '2019-01-21 03:07:42', '2019-01-26 05:21:02'),
(42, 25, 'P-10', 6, 2, 0, '3915480616629321', 0, '', '2019-01-21 03:07:42', '2019-01-26 05:21:02'),
(43, 25, 'P-10', 6, 2, 0, '4015480616623095', 0, '', '2019-01-21 03:07:42', '2019-01-26 05:21:02'),
(44, 25, 'P-10', 6, 2, 0, '4115480616627998', 0, '', '2019-01-21 03:07:42', '2019-01-26 05:21:02'),
(45, 36, 'P-10', 6, 1, 0, '3715486669967477', 15, '', '2019-01-28 03:16:36', '2019-01-28 03:34:19'),
(46, 36, 'P-10', 6, 1, 0, '3815486669969778', 15, '', '2019-01-28 03:16:36', '2019-01-28 03:34:19'),
(47, 36, 'P-10', 6, 1, 0, '3915486669966865', 15, '', '2019-01-28 03:16:36', '2019-01-28 03:34:19'),
(48, 36, 'P-10', 6, 1, 0, '4015486669966301', 15, '', '2019-01-28 03:16:36', '2019-01-28 03:34:19'),
(49, 36, 'P-10', 6, 1, 0, '4115486669964267', 15, '', '2019-01-28 03:16:36', '2019-01-28 03:34:19'),
(50, 36, 'P-10', 6, 1, 0, '4215486669964948', 15, '', '2019-01-28 03:16:36', '2019-01-28 03:34:19'),
(51, 36, 'P-10', 6, 1, 0, '4315486669968625', 15, '', '2019-01-28 03:16:36', '2019-01-28 03:34:19'),
(52, 36, 'P-10', 6, 1, 0, '4415486669962425', 15, '', '2019-01-28 03:16:36', '2019-01-28 22:31:54'),
(53, 36, 'P-10', 6, 1, 0, '4515486669966319', 15, '', '2019-01-28 03:16:36', '2019-01-28 22:31:54'),
(54, 36, 'P-10', 6, 1, 0, '4615486669963033', 15, '', '2019-01-28 03:16:36', '2019-01-29 00:02:42'),
(55, 36, 'P-10', 6, 1, 0, '4715486669962447', 15, '', '2019-01-28 03:16:36', '2019-01-29 00:02:42'),
(56, 36, 'P-10', 6, 1, 0, '4815486669963610', 15, '', '2019-01-28 03:16:36', '2019-01-29 00:02:42'),
(57, 37, 'Spa-500', 5, 1, 0, '3815487553519713', 30, '', '2019-01-29 03:49:11', '2019-01-29 03:50:05'),
(58, 37, 'Spa-500', 5, 1, 0, '3915487553513748', 30, '', '2019-01-29 03:49:11', '2019-01-29 03:50:05'),
(59, 38, 'P-10', 5, 1, 0, '3915491813355249', 15, '', '2019-02-03 02:08:55', '2019-02-03 02:29:43'),
(60, 38, 'P-10', 5, 1, 0, '4015491813359795', 15, '', '2019-02-03 02:08:55', '2019-02-03 02:29:43'),
(61, 38, 'P-10', 5, 1, 0, '4115491813358022', 15, '', '2019-02-03 02:08:55', '2019-02-03 02:29:43'),
(62, 38, 'P-10', 5, 1, 0, '4215491813355351', 15, '', '2019-02-03 02:08:55', '2019-02-03 02:29:43'),
(63, 38, 'P-10', 5, 1, 0, '4315491813359927', 15, '', '2019-02-03 02:08:55', '2019-02-03 02:29:43'),
(64, 38, 'P-10', 5, 1, 0, '4415491813356359', 15, '', '2019-02-03 02:08:55', '2019-02-03 02:30:38'),
(65, 39, 'P-10', 6, 1, 0, '4015491827378270', 15, '', '2019-02-03 02:32:17', '2019-02-03 02:32:36'),
(66, 39, 'P-10', 6, 1, 0, '4115491827378116', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:36:05'),
(67, 39, 'P-10', 6, 1, 0, '4215491827379166', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:39:04'),
(68, 39, 'P-10', 6, 1, 0, '4315491827379514', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:41:41'),
(69, 39, 'P-10', 6, 1, 0, '4415491827376599', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:47:39'),
(70, 39, 'P-10', 6, 1, 0, '4515491827377961', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:48:01'),
(71, 39, 'P-10', 6, 1, 0, '4615491827374749', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:48:01'),
(72, 39, 'P-10', 6, 1, 0, '4715491827371564', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:48:01'),
(73, 39, 'P-10', 6, 1, 0, '4815491827372030', 15, '', '2019-02-03 02:32:17', '2019-02-03 03:48:01'),
(74, 39, 'P-10', 6, 1, 0, '4915491827375994', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:16:40'),
(75, 39, 'P-10', 6, 1, 0, '5015491827378550', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:16:40'),
(76, 39, 'P-10', 6, 1, 0, '5115491827377177', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:16:40'),
(77, 39, 'P-10', 6, 1, 0, '5215491827371631', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:16:40'),
(78, 39, 'P-10', 6, 1, 0, '5315491827376524', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:17:10'),
(79, 39, 'P-10', 6, 1, 0, '5415491827379267', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:17:45'),
(80, 39, 'P-10', 6, 1, 0, '5515491827376963', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:19:04'),
(81, 39, 'P-10', 6, 1, 0, '5615491827375029', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:20:39'),
(82, 39, 'P-10', 6, 1, 0, '5715491827375486', 15, '', '2019-02-03 02:32:17', '2019-02-03 04:27:20'),
(83, 40, 'P-10', 6, 1, 0, '4115491900047592', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:31'),
(84, 40, 'P-10', 6, 1, 0, '4215491900048720', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:49'),
(85, 40, 'P-10', 6, 1, 0, '4315491900046022', 280, '', '2019-02-03 04:33:24', '2019-02-03 05:46:38'),
(86, 40, 'P-10', 6, 1, 0, '4415491900048332', 280, '', '2019-02-03 04:33:24', '2019-02-03 05:51:27'),
(87, 40, 'P-10', 6, 1, 0, '4515491900043695', 280, '', '2019-02-03 04:33:24', '2019-02-04 10:15:28'),
(88, 40, 'P-10', 6, 1, 0, '4615491900042053', 280, '', '2019-02-03 04:33:24', '2019-02-04 10:16:17'),
(89, 40, 'P-10', 6, 1, 0, '4715491900041828', 280, '', '2019-02-03 04:33:24', '2019-02-04 10:17:14'),
(90, 40, 'P-10', 6, 0, 0, '4815491900046716', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(91, 40, 'P-10', 6, 0, 0, '4915491900049473', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(92, 40, 'P-10', 6, 0, 0, '5015491900043050', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(93, 40, 'P-10', 6, 0, 0, '5115491900045035', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(94, 40, 'P-10', 6, 0, 0, '5215491900045698', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(95, 40, 'P-10', 6, 0, 0, '5315491900048082', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(96, 40, 'P-10', 6, 0, 0, '5415491900049367', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(97, 40, 'P-10', 6, 0, 0, '5515491900045919', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(98, 40, 'P-10', 6, 0, 0, '5615491900041871', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(99, 40, 'P-10', 6, 0, 0, '5715491900043820', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(100, 40, 'P-10', 6, 0, 0, '5815491900049577', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(101, 40, 'P-10', 6, 0, 0, '5915491900048735', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(102, 40, 'P-10', 6, 0, 0, '6015491900043343', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(103, 40, 'P-10', 6, 0, 0, '6115491900045709', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(104, 40, 'P-10', 6, 0, 0, '6215491900048385', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(105, 40, 'P-10', 6, 0, 0, '6315491900043464', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(106, 40, 'P-10', 6, 0, 0, '6415491900049537', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(107, 40, 'P-10', 6, 0, 0, '6515491900047900', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(108, 40, 'P-10', 6, 0, 0, '6615491900046932', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(109, 40, 'P-10', 6, 0, 0, '6715491900049907', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(110, 40, 'P-10', 6, 0, 0, '6815491900048293', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(111, 40, 'P-10', 6, 0, 0, '6915491900046671', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(112, 40, 'P-10', 6, 0, 0, '7015491900043026', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(113, 40, 'P-10', 6, 0, 0, '7115491900041629', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(114, 40, 'P-10', 6, 0, 0, '7215491900049820', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(115, 40, 'P-10', 6, 0, 0, '7315491900046551', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(116, 40, 'P-10', 6, 0, 0, '7415491900046380', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(117, 40, 'P-10', 6, 0, 0, '7515491900046229', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(118, 40, 'P-10', 6, 0, 0, '7615491900042688', 280, '', '2019-02-03 04:33:24', '2019-02-03 04:33:24'),
(119, 41, 'P-10', 7, 0, 0, '4215492558736892', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(120, 41, 'P-10', 7, 0, 0, '4315492558736022', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(121, 41, 'P-10', 7, 0, 0, '4415492558735967', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(122, 41, 'P-10', 7, 0, 0, '4515492558739733', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(123, 41, 'P-10', 7, 0, 0, '4615492558739756', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(124, 41, 'P-10', 7, 0, 0, '4715492558734661', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(125, 41, 'P-10', 7, 0, 0, '4815492558736387', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(126, 41, 'P-10', 7, 0, 0, '4915492558733058', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(127, 41, 'P-10', 7, 0, 0, '5015492558737123', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(128, 41, 'P-10', 7, 0, 0, '5115492558731765', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(129, 41, 'P-10', 7, 0, 0, '5215492558735745', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(130, 41, 'P-10', 7, 0, 0, '5315492558739753', 15, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-03 22:51:13'),
(131, 42, 'P-10', 7, 0, 0, '4315492562051290', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(132, 42, 'P-10', 7, 0, 0, '4415492562051557', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(133, 42, 'P-10', 7, 0, 0, '4515492562051927', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(134, 42, 'P-10', 7, 0, 0, '4615492562059679', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(135, 42, 'P-10', 7, 0, 0, '4715492562052807', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(136, 42, 'P-10', 7, 0, 0, '4815492562055311', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(137, 42, 'P-10', 7, 0, 0, '4915492562055800', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(138, 42, 'P-10', 7, 0, 0, '5015492562053608', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(139, 42, 'P-10', 7, 0, 0, '5115492562058368', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(140, 42, 'P-10', 7, 0, 0, '5215492562051748', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(141, 42, 'P-10', 7, 0, 0, '5315492562051177', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(142, 42, 'P-10', 7, 0, 0, '5415492562054798', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45'),
(143, 42, 'P-10', 7, 0, 0, '5515492562055993', 15, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-03 22:56:45');

-- --------------------------------------------------------

--
-- Table structure for table `memberships`
--

CREATE TABLE `memberships` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `memberships`
--

INSERT INTO `memberships` (`id`, `title`, `discount`, `created_at`, `updated_at`) VALUES
(2, 'Silver member', 5, '2019-01-18 03:57:53', '2019-01-18 03:57:53'),
(3, 'Regular member', 0, '2019-01-18 04:02:48', '2019-01-18 04:02:48'),
(4, 'Golden member', 10, '2019-01-18 09:53:17', '2019-01-18 09:53:17'),
(5, 'Platinum member', 12, '2019-01-18 09:53:34', '2019-01-18 09:53:34');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_01_11_230613_entrust_setup_tables', 1),
(4, '2019_01_11_235651_create_user_datas_table', 1),
(5, '2019_01_12_012001_create_products_table', 1),
(6, '2019_01_12_012041_create_vendors_table', 1),
(7, '2019_01_12_012059_create_imports_table', 1),
(8, '2019_01_12_012131_create_sales_table', 1),
(9, '2019_01_12_012207_create_designations_table', 1),
(10, '2019_01_12_012227_create_payrolls_table', 1),
(11, '2019_01_12_012245_create_memberships_table', 1),
(12, '2019_01_12_012304_create_sales_offers_table', 1),
(13, '2019_01_12_012331_create_commisions_table', 1),
(14, '2019_01_12_012419_create_utility_costs_table', 1),
(15, '2019_01_12_115839_create_categories_table', 1),
(16, '2019_01_12_115845_create_sub_categories_table', 1),
(17, '2019_01_12_141119_create_user_designations_table', 1),
(18, '2019_01_12_141520_create_user_memberships_table', 1),
(19, '2019_01_15_044038_create_vendor_products_table', 2),
(20, '2019_01_20_065410_create_items_table', 3),
(21, '2019_01_20_105831_create_resalers_table', 4),
(22, '2019_01_25_162019_create_themes_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payrolls`
--

CREATE TABLE `payrolls` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `basic_sallery` float NOT NULL,
  `medical` float NOT NULL,
  `home` float NOT NULL,
  `other` float NOT NULL,
  `commission` int(11) NOT NULL,
  `total` float NOT NULL,
  `deduction` float NOT NULL,
  `total_pay` float NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payrolls`
--

INSERT INTO `payrolls` (`id`, `user_id`, `basic_sallery`, `medical`, `home`, `other`, `commission`, `total`, `deduction`, `total_pay`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 22000, 2000, 8000, 1, 2, 32002, 0, 32002, 1, '2019-01-30 23:12:29', '2019-01-30 23:12:29'),
(2, 2, 8000, 1500, 4000, 0, 0, 13500, 0, 13500, 1, '2019-01-31 00:19:55', '2019-01-31 00:19:55'),
(3, 5, 8000, 1500, 4000, 0, 0, 13500, 0, 13500, 1, '2019-01-31 00:20:22', '2019-01-31 00:20:22');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'grant-permission', 'Grant permission ', 'This permission user can get access on this site', NULL, NULL),
(2, 'can-modify-product', 'Can modify product', 'This permission user can modify products.', '2019-01-17 11:25:34', '2019-01-17 11:25:34'),
(4, 'can-modify-role', 'Can modify role', 'This permission user can modify user role', '2019-01-21 05:38:51', '2019-01-21 05:38:51'),
(5, 'can-modify-category', 'Can modify category', 'This permission user can modify category', '2019-01-21 05:39:26', '2019-01-21 05:39:26'),
(6, 'can-modify-vendor', 'Can modify venor', 'This permission user can modify vendors', '2019-01-21 05:40:01', '2019-01-21 05:40:01'),
(7, 'can-modify-sell', 'Can modify sell', 'This permission user can modify sells', '2019-01-21 05:40:48', '2019-01-21 05:40:48'),
(8, 'can-modify-employee', 'Can modify employee', 'This permission user can modify employees', '2019-01-21 05:41:12', '2019-01-21 05:41:12'),
(9, 'can-modify-resaler', 'Can modify resaler', 'This permission user can modify resalers', '2019-01-21 05:41:32', '2019-01-21 05:41:32'),
(10, 'can-modify-payroll', 'Can modify payroll', 'This permission user can modify payroll', '2019-01-21 05:42:06', '2019-01-21 05:42:06'),
(11, 'can-modify-membership', 'Can modify membership', 'This permission user can modify membership', '2019-01-21 05:42:30', '2019-01-21 05:42:30'),
(12, 'can-modify-salesoffer', 'Can modify sales offer', 'This permission user can modify sales offer', '2019-01-21 05:43:08', '2019-01-21 05:43:08'),
(13, 'can-modify-commission', 'Can modify commission', 'This permission user can modify commissions', '2019-01-21 05:43:40', '2019-01-21 05:43:40'),
(14, 'can-modify-accounts', 'Can modify accounts', 'This permission user can modify accounts', '2019-01-21 05:45:16', '2019-01-21 05:45:16'),
(15, 'can-modify-import', 'Can modify import', 'This permission user can import products', '2019-01-21 05:51:35', '2019-01-21 05:51:35');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profit_unit` float NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `import_id` int(11) NOT NULL,
  `stock` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `import_identification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `image`, `price`, `product_code`, `about`, `profit_unit`, `category_id`, `sub_category_id`, `vendor_id`, `import_id`, `stock`, `status`, `import_identification`, `created_at`, `updated_at`) VALUES
(22, 'Ball pen', 'uploads/15479785384306.png', 15, 'P-10', 'qwfertyhgjvdfgnh', 3, 1, 3, 2, 4, 0, 1, '', '2019-01-20 04:02:19', '2019-01-29 03:56:50'),
(23, 'Water Bottle', 'uploads/15479785947795.png', 30, 'Spa-250', 'asfdghjtytje', 5, 5, 2, 2, 4, 0, 1, '', '2019-01-20 04:03:14', '2019-01-29 00:01:13'),
(24, 'Ball pen', 'uploads/15480450716887.png', 15, 'P-10', 'wretyu', 3, 1, 3, 2, 5, 0, 1, '', '2019-01-20 22:31:11', '2019-01-21 03:06:32'),
(25, 'Ball pen', 'uploads/15480616621253.png', 15, 'P-10', 'AWSERTYUI', 3, 1, 3, 2, 6, 0, 1, '', '2019-01-21 03:07:42', '2019-01-26 05:21:02'),
(36, 'Ball pen', 'uploads/15486669967610.jpg', 15, 'P-10', 'fsrgty', 3, 5, 2, 2, 6, 0, 1, '', '2019-01-28 03:16:36', '2019-01-29 00:02:42'),
(37, 'Spa drinking water', 'uploads/15487553517652.png', 30, 'Spa-500', 'eg&nbsp; er&nbsp;', 5, 5, 2, 2, 5, 0, 1, '', '2019-01-29 03:49:11', '2019-01-29 03:50:05'),
(38, 'Ball pen', 'uploads/15491813357655.png', 15, 'P-10', 'sf', 3, 1, 3, 2, 5, 0, 1, '', '2019-02-03 02:08:55', '2019-02-03 02:30:38'),
(39, 'Ball pen', 'uploads/15491827377128.png', 15, 'P-10', 'qw', 3, 1, 3, 2, 6, 0, 1, '', '2019-02-03 02:32:17', '2019-02-03 04:27:20'),
(40, 'Ball pen', 'uploads/15491900045508.png', 280, 'P-10', 'rsedfghjk', 1, 1, 3, 2, 6, 29, 1, '', '2019-02-03 04:33:24', '2019-02-04 10:17:14'),
(41, 'Ball pen', 'uploads/15492558726393.png', 15, 'P-10', 'fsrgthyjukilo', 3, 1, 3, 2, 7, 12, 1, 'P-1015492533029664', '2019-02-03 22:51:13', '2019-02-04 10:17:14'),
(42, 'Ball pen', 'uploads/15492562058979.png', 15, 'P-10', 'svv', 3, 1, 3, 2, 7, 13, 1, 'P-1015492533029664', '2019-02-03 22:56:45', '2019-02-04 10:17:14');

-- --------------------------------------------------------

--
-- Table structure for table `resalers`
--

CREATE TABLE `resalers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `village` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `resalers`
--

INSERT INTO `resalers` (`id`, `name`, `contact`, `district`, `village`, `area`, `email`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mr Shafiq', '01959695455', 'Dhaka', 'Khilkhet', 'Khilkhet', 'shafiq@abc.com', 'uploads/15480410293716.jpg', '1', '2019-01-20 21:23:49', '2019-01-20 21:38:09');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'super-admin', 'Super Admin', 'This role user have all the power.', NULL, NULL),
(2, 'product-admin', 'Product admin', 'This admin can modify products when he wants.', '2019-01-17 11:33:16', '2019-01-17 11:33:16'),
(3, 'reception-admin', 'Reception Admin', 'This role user can sell products', '2019-01-18 02:33:49', '2019-01-18 02:33:49');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(6, 3),
(8, 3);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `resaller_id` int(11) NOT NULL DEFAULT '0',
  `products` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` double NOT NULL,
  `paid` double NOT NULL,
  `due` double NOT NULL,
  `is_on_cash` int(11) NOT NULL,
  `saller_id` int(11) NOT NULL,
  `profit` float NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `resaller_id`, `products`, `total`, `paid`, `due`, `is_on_cash`, `saller_id`, `profit`, `created_at`, `updated_at`) VALUES
(3, 0, '[[{\"id\":22,\"product_id\":22,\"product_code\":\"P-10\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"115479785392335\",\"created_at\":\"2019-01-20 10:02:19\",\"updated_at\":\"2019-01-21 08:33:39\"},{\"id\":23,\"product_id\":22,\"product_code\":\"P-10\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"215479785391214\",\"created_at\":\"2019-01-20 10:02:19\",\"updated_at\":\"2019-01-21 08:33:39\"}]]', 30, 0, 30, 0, 1, 0, '2019-01-21 02:33:39', '2019-01-21 02:33:39'),
(4, 0, '[[{\"id\":24,\"product_id\":22,\"product_code\":\"P-10\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"315479785395870\",\"created_at\":\"2019-01-20 10:02:19\",\"updated_at\":\"2019-01-21 08:40:34\"},{\"id\":25,\"product_id\":22,\"product_code\":\"P-10\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"415479785392888\",\"created_at\":\"2019-01-20 10:02:19\",\"updated_at\":\"2019-01-21 08:40:34\"},{\"id\":26,\"product_id\":22,\"product_code\":\"P-10\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"515479785397451\",\"created_at\":\"2019-01-20 10:02:19\",\"updated_at\":\"2019-01-21 08:40:34\"},{\"id\":27,\"product_id\":22,\"product_code\":\"P-10\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"615479785399046\",\"created_at\":\"2019-01-20 10:02:19\",\"updated_at\":\"2019-01-21 08:40:34\"},{\"id\":31,\"product_id\":24,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"2915480450716502\",\"created_at\":\"2019-01-21 04:31:11\",\"updated_at\":\"2019-01-21 08:40:34\"},{\"id\":32,\"product_id\":24,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3015480450719915\",\"created_at\":\"2019-01-21 04:31:11\",\"updated_at\":\"2019-01-21 08:40:34\"},{\"id\":33,\"product_id\":24,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3115480450711793\",\"created_at\":\"2019-01-21 04:31:11\",\"updated_at\":\"2019-01-21 08:40:34\"}],[{\"id\":28,\"product_id\":23,\"product_code\":\"Spa-250\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"2315479785944663\",\"created_at\":\"2019-01-20 10:03:14\",\"updated_at\":\"2019-01-21 08:40:34\"},{\"id\":29,\"product_id\":23,\"product_code\":\"Spa-250\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"2415479785945509\",\"created_at\":\"2019-01-20 10:03:14\",\"updated_at\":\"2019-01-21 08:40:34\"}]]', 165, 0, 165, 0, 1, 0, '2019-01-21 02:40:34', '2019-01-21 02:40:34'),
(5, 0, '[[{\"id\":34,\"product_id\":24,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3215480450716068\",\"created_at\":\"2019-01-21 04:31:11\",\"updated_at\":\"2019-01-21 09:06:32\"}]]', 15, 0, 15, 0, 1, 0, '2019-01-21 03:06:32', '2019-01-21 03:06:32'),
(6, 1, '[[{\"id\":35,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3215480616627513\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-21 10:51:31\"},{\"id\":36,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3315480616625147\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-21 10:51:31\"},{\"id\":37,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3415480616621143\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-21 10:51:31\"},{\"id\":38,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3515480616628475\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-21 10:51:31\"},{\"id\":39,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3615480616625663\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-21 10:51:31\"},{\"id\":40,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3715480616629023\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-21 10:51:31\"}]]', 90, 0, 90, 0, 1, 0, '2019-01-21 04:51:31', '2019-01-21 04:51:31'),
(7, 1, '[[{\"id\":41,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3815480616624817\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-26 11:21:02\"},{\"id\":42,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"3915480616629321\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-26 11:21:02\"},{\"id\":43,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"4015480616623095\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-26 11:21:02\"},{\"id\":44,\"product_id\":25,\"product_code\":\"P-10\",\"import_id\":6,\"type\":2,\"resaler_id\":0,\"Identeti_number\":\"4115480616627998\",\"created_at\":\"2019-01-21 09:07:42\",\"updated_at\":\"2019-01-26 11:21:02\"}]]', 60, 60, 0, 1, 1, 12, '2019-01-26 05:21:02', '2019-01-26 05:21:02'),
(8, 0, '[[{\"id\":45,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3715486669967477\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-28 09:34:19\"},{\"id\":46,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3815486669969778\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-28 09:34:19\"},{\"id\":47,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3915486669966865\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-28 09:34:19\"},{\"id\":48,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4015486669966301\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-28 09:34:19\"},{\"id\":49,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4115486669964267\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-28 09:34:19\"},{\"id\":50,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4215486669964948\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-28 09:34:19\"},{\"id\":51,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4315486669968625\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-28 09:34:19\"}]]', 105, 0, 105, 0, 1, 0, '2019-01-28 03:34:19', '2019-01-28 03:34:19'),
(9, 0, '[[{\"id\":52,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4415486669962425\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-29 04:31:54\"},{\"id\":53,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4515486669966319\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-29 04:31:54\"}]]', 30, 30, 0, 1, 1, 0, '2019-01-28 22:31:54', '2019-01-28 22:31:54'),
(10, 0, '[[{\"id\":30,\"product_id\":23,\"product_code\":\"Spa-250\",\"import_id\":4,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"2515479785945076\",\"product_price\":0,\"created_at\":\"2019-01-20 10:03:14\",\"updated_at\":\"2019-01-29 06:01:13\"}]]', 30, 0, 30, 0, 1, 5, '2019-01-29 00:01:13', '2019-01-29 00:01:13'),
(11, 0, '[[{\"id\":54,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4615486669963033\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-29 06:02:42\"},{\"id\":55,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4715486669962447\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-29 06:02:42\"},{\"id\":56,\"product_id\":36,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4815486669963610\",\"product_price\":15,\"created_at\":\"2019-01-28 09:16:36\",\"updated_at\":\"2019-01-29 06:02:42\"}]]', 45, 0, 45, 0, 1, 9, '2019-01-29 00:02:42', '2019-01-29 00:02:42'),
(12, 0, '[[{\"id\":57,\"product_id\":37,\"product_code\":\"Spa-500\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3815487553519713\",\"product_price\":30,\"created_at\":\"2019-01-29 09:49:11\",\"updated_at\":\"2019-01-29 09:50:05\"},{\"id\":58,\"product_id\":37,\"product_code\":\"Spa-500\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3915487553513748\",\"product_price\":30,\"created_at\":\"2019-01-29 09:49:11\",\"updated_at\":\"2019-01-29 09:50:05\"}]]', 60, 0, 60, 0, 1, 10, '2019-01-29 03:50:05', '2019-01-29 03:50:05'),
(25, 0, '[[{\"id\":59,\"product_id\":38,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"3915491813355249\",\"product_price\":15,\"created_at\":\"2019-02-03 08:08:55\",\"updated_at\":\"2019-02-03 08:29:43\"},{\"id\":60,\"product_id\":38,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4015491813359795\",\"product_price\":15,\"created_at\":\"2019-02-03 08:08:55\",\"updated_at\":\"2019-02-03 08:29:43\"},{\"id\":61,\"product_id\":38,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4115491813358022\",\"product_price\":15,\"created_at\":\"2019-02-03 08:08:55\",\"updated_at\":\"2019-02-03 08:29:43\"},{\"id\":62,\"product_id\":38,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4215491813355351\",\"product_price\":15,\"created_at\":\"2019-02-03 08:08:55\",\"updated_at\":\"2019-02-03 08:29:43\"},{\"id\":63,\"product_id\":38,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4315491813359927\",\"product_price\":15,\"created_at\":\"2019-02-03 08:08:55\",\"updated_at\":\"2019-02-03 08:29:43\"}]]', 75, 0, 75, 0, 1, 0, '2019-02-03 02:29:43', '2019-02-03 02:29:43'),
(27, 0, '[[{\"id\":64,\"product_id\":38,\"product_code\":\"P-10\",\"import_id\":5,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4415491813356359\",\"product_price\":15,\"created_at\":\"2019-02-03 08:08:55\",\"updated_at\":\"2019-02-03 08:30:38\"}]]', 15, 0, 15, 0, 1, 3, '2019-02-03 02:30:38', '2019-02-03 02:30:38'),
(28, 0, '[[{\"id\":65,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4015491827378270\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 08:32:36\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 02:32:36', '2019-02-03 02:32:36'),
(84, 0, '[[{\"id\":66,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4115491827378116\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:36:05\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 03:36:06', '2019-02-03 03:36:06'),
(85, 0, '[[{\"id\":67,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4215491827379166\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:39:04\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 03:39:05', '2019-02-03 03:39:05'),
(86, 0, '[[{\"id\":68,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4315491827379514\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:41:41\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 03:41:42', '2019-02-03 03:41:42'),
(87, 0, '[[{\"id\":69,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4415491827376599\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:47:39\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 03:47:40', '2019-02-03 03:47:40'),
(88, 0, '[[{\"id\":70,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4515491827377961\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:48:01\"},{\"id\":71,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4615491827374749\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:48:01\"},{\"id\":72,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4715491827371564\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:48:01\"},{\"id\":73,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4815491827372030\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 09:48:01\"}]]', 60, 60, 0, 1, 1, 0, '2019-02-03 03:48:01', '2019-02-03 03:48:01'),
(89, 0, '[[{\"id\":74,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4915491827375994\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:16:40\"},{\"id\":75,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5015491827378550\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:16:40\"},{\"id\":76,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5115491827377177\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:16:40\"},{\"id\":77,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5215491827371631\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:16:40\"}]]', 60, 0, 60, 0, 1, 0, '2019-02-03 04:16:40', '2019-02-03 04:16:40'),
(90, 0, '[[{\"id\":78,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5315491827376524\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:17:10\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 04:17:10', '2019-02-03 04:17:10'),
(91, 0, '[[{\"id\":79,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5415491827379267\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:17:45\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 04:17:45', '2019-02-03 04:17:45'),
(92, 0, '[[{\"id\":80,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5515491827376963\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:19:04\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 04:19:04', '2019-02-03 04:19:04'),
(93, 0, '[[{\"id\":81,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5615491827375029\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:20:39\"}]]', 15, 0, 15, 0, 1, 0, '2019-02-03 04:20:39', '2019-02-03 04:20:39'),
(94, 0, '[[{\"id\":82,\"product_id\":39,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"5715491827375486\",\"product_price\":15,\"created_at\":\"2019-02-03 08:32:17\",\"updated_at\":\"2019-02-03 10:27:20\"}]]', 15, 0, 15, 0, 1, 3, '2019-02-03 04:27:20', '2019-02-03 04:27:20'),
(95, 0, '[[{\"id\":83,\"product_id\":40,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4115491900047592\",\"product_price\":280,\"created_at\":\"2019-02-03 10:33:24\",\"updated_at\":\"2019-02-03 10:33:31\"}]]', 280, 0, 280, 0, 1, 0, '2019-02-03 04:33:31', '2019-02-03 04:33:31'),
(96, 0, '[[{\"id\":84,\"product_id\":40,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4215491900048720\",\"product_price\":280,\"created_at\":\"2019-02-03 10:33:24\",\"updated_at\":\"2019-02-03 10:33:49\"}]]', 280, 0, 280, 0, 1, 0, '2019-02-03 04:33:49', '2019-02-03 04:33:49'),
(97, 0, '[[{\"id\":85,\"product_id\":40,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4315491900046022\",\"product_price\":280,\"created_at\":\"2019-02-03 10:33:24\",\"updated_at\":\"2019-02-03 11:46:38\"}]]', 280, 0, 280, 0, 1, 0, '2019-02-03 05:46:38', '2019-02-03 05:46:38'),
(98, 0, '[[{\"id\":86,\"product_id\":40,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4415491900048332\",\"product_price\":280,\"created_at\":\"2019-02-03 10:33:24\",\"updated_at\":\"2019-02-03 11:51:27\"}]]', 280, 0, 280, 0, 1, 1, '2019-02-03 05:51:27', '2019-02-03 05:51:27'),
(99, 0, '[[{\"id\":87,\"product_id\":40,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4515491900043695\",\"product_price\":280,\"import_identification\":\"\",\"created_at\":\"2019-02-03 10:33:24\",\"updated_at\":\"2019-02-04 16:15:28\"}]]', 15, 0, 15, 0, 1, 1, '2019-02-04 10:15:28', '2019-02-04 10:15:28'),
(100, 0, '[[{\"id\":88,\"product_id\":40,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4615491900042053\",\"product_price\":280,\"import_identification\":\"\",\"created_at\":\"2019-02-03 10:33:24\",\"updated_at\":\"2019-02-04 16:16:17\"}]]', 15, 0, 15, 0, 1, 1, '2019-02-04 10:16:17', '2019-02-04 10:16:17'),
(101, 0, '[[{\"id\":89,\"product_id\":40,\"product_code\":\"P-10\",\"import_id\":6,\"type\":1,\"resaler_id\":0,\"Identeti_number\":\"4715491900041828\",\"product_price\":280,\"import_identification\":\"\",\"created_at\":\"2019-02-03 10:33:24\",\"updated_at\":\"2019-02-04 16:17:14\"}]]', 15, 0, 15, 0, 1, 1, '2019-02-04 10:17:14', '2019-02-04 10:17:14');

-- --------------------------------------------------------

--
-- Table structure for table `sales_offers`
--

CREATE TABLE `sales_offers` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `discount` double DEFAULT NULL,
  `from` datetime DEFAULT NULL,
  `to` datetime DEFAULT NULL,
  `sales_type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `title`, `image`, `created_at`, `updated_at`) VALUES
(2, 5, 'Water', 'uploads/15479736095976.png', '2019-01-20 02:40:09', '2019-01-20 02:40:09'),
(3, 1, 'Ball pen', 'uploads/15479743138706.png', '2019-01-20 02:51:53', '2019-01-20 02:51:53');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `top_nav` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `font_family` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '"Source Sans Pro", sans-serif',
  `font_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '15px',
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `left_side` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#2B333E',
  `left_side_dropdown` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#252c35',
  `left_border_left_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#00AAFF',
  `left_dropdown_item_back` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#252c35',
  `top_nav_drop_background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `top_nav_drop_background_hover` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `top_nav_drop_background_items` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `main_background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `title_background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `body_background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `panel_background` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#ffffff',
  `body_font_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#000000',
  `title_font_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#000000',
  `left_side_font_family` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '"Source Sans Pro", sans-serif',
  `left_side_font_color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#676a6d',
  `left_side_font_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '18px',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `user_id`, `top_nav`, `font_family`, `font_size`, `brand`, `left_side`, `left_side_dropdown`, `left_border_left_color`, `left_dropdown_item_back`, `top_nav_drop_background`, `top_nav_drop_background_hover`, `top_nav_drop_background_items`, `main_background`, `title_background`, `body_background`, `panel_background`, `body_font_color`, `title_font_color`, `left_side_font_family`, `left_side_font_color`, `left_side_font_size`, `created_at`, `updated_at`) VALUES
(1, 1, '#ffffff', '\"Source Sans Pro\", sans-serif', '16px', '#ffffff', '#ffffff', '#c0c0c0', '#000000', '#ffffff', '#ffffff', '#c0c0c0', '#ffffff', '#ffffff', '#000000', '#ffffff', '#ffffff', '#000000', '#ffffff', 'monospace', '#000000', '15px', '2019-01-24 18:00:00', '2019-01-31 02:45:53'),
(2, 6, '#ffffff', '\"Source Sans Pro\", sans-serif', '15px', '#ffffff', '#2B333E', '#252c35', '#00AAFF', '#252c35', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#000000', '#000000', '\"Source Sans Pro\", sans-serif', '#676a6d', '18px', '2019-01-25 10:52:50', '2019-01-25 10:52:50'),
(3, 5, '#ffffff', '\"Source Sans Pro\", sans-serif', '15px', '#ffffff', '#2B333E', '#252c35', '#00AAFF', '#252c35', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#000000', '#000000', '\"Source Sans Pro\", sans-serif', '#676a6d', '18px', NULL, NULL),
(4, 2, '#ffffff', '\"Source Sans Pro\", sans-serif', '15px', '#ffffff', '#2b333e', '#252c35', '#00aaff', '#252c35', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#000000', '#000000', '\"Source Sans Pro\", sans-serif', '#676a6d', '18px', NULL, '2019-01-26 04:57:55'),
(5, 4, '#ffffff', '\"Source Sans Pro\", sans-serif', '15px', '#ffffff', '#2B333E', '#252c35', '#00AAFF', '#252c35', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#000000', '#000000', '\"Source Sans Pro\", sans-serif', '#676a6d', '18px', NULL, NULL),
(6, 3, '#ffffff', '\"Source Sans Pro\", sans-serif', '15px', '#ffffff', '#2B333E', '#252c35', '#00AAFF', '#252c35', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#000000', '#000000', '\"Source Sans Pro\", sans-serif', '#676a6d', '18px', NULL, NULL),
(7, 7, '#ffffff', '\"Source Sans Pro\", sans-serif', '15px', '#ffffff', '#2B333E', '#252c35', '#00AAFF', '#252c35', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#000000', '#000000', '\"Source Sans Pro\", sans-serif', '#676a6d', '18px', '2019-01-28 04:47:07', '2019-01-28 04:47:07'),
(8, 8, '#ffffff', '\"Source Sans Pro\", sans-serif', '15px', '#ffffff', '#2B333E', '#252c35', '#00AAFF', '#252c35', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#ffffff', '#000000', '#000000', '\"Source Sans Pro\", sans-serif', '#676a6d', '18px', '2019-01-28 04:50:58', '2019-01-28 04:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `role_id` int(11) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `email_verified_at`, `password`, `status`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sabbirchowdury000@gmail.com', NULL, '$2y$10$8sG9wckZIls/nswgA7m8B.2cAPH4A.TZk07xB7FyeSkt.jUqZri7u', 1, 1, '8EgHReqxwbHhCGC2I2Hfg9rTSCrl4BjwATmriyrgBxJZp4esEINK8UrOwV1Y', NULL, '2019-01-28 22:31:02'),
(2, 'sabbirchowdury@gmail.com', NULL, '$2y$10$HM5ixix3u8dUO9xaeGE3E.XDECYSX30K0HJHl6xkgKeSvUcmB1p5O', 1, 1, 'cErjn39fcLXQqnv8953lCTjnROSww0A12l7kbtJAizdYrV5pSQfbeS59LbXI', '2019-01-17 16:28:04', '2019-01-26 05:23:50'),
(3, 'shafiq@gmail.com', NULL, '$2y$12$odIsSTSM09/02RZSK1oRA.zc4OR2F2EWg5EySWha/h9ESREQ9McX.', 1, 2, 'ut3rIvQQkjt7BpBnrZ7V4p7rN5dfkDE88zm5bw3T1B5gASTbCa3yNX0LSt7F', '2019-01-17 21:41:52', '2019-01-17 21:41:52'),
(4, 'sufair@gmail.com', NULL, '$2y$10$TkwAfFCGTb8Q3d3fpbpX/upKplQKGBWMbSVYWlTDy91dytKON8pVq', 1, 2, NULL, '2019-01-17 21:42:37', '2019-01-17 21:42:37'),
(5, 'opu@test.com', NULL, '$2y$10$j4c7wTQKlqXKCmY9bEmmoOfiM8FKg3Ti44um8uaGYQjr4Gj87qC52', 1, 3, NULL, '2019-01-25 10:51:47', '2019-01-25 10:51:47'),
(6, 'zakir@test.com', NULL, '$2y$10$r0ISAwg8mOsMhiI/TVye9OXB.HZVbUoNIdNcWbgx2y2mTSnxHTkg6', 1, 3, NULL, '2019-01-25 10:52:50', '2019-01-25 10:52:50'),
(7, 'diptochowdury000@gmail.com', NULL, '$2y$10$073.IFqw7hKrUwKLaQUjkeT2UK92NBD9b53f2oQOpYcGCHoMJ07lK', 1, 2, NULL, '2019-01-28 04:47:07', '2019-01-28 04:47:07'),
(8, 'diptochowdury@gmail.com', NULL, '$2y$10$8R7mYqswhl2AK7o9ARt3MO78io0B56WogXrXg/1pWnrsgMkhdlYam', 1, 3, NULL, '2019-01-28 04:50:58', '2019-01-28 04:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_datas`
--

CREATE TABLE `user_datas` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msisdn` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `b_date` datetime DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'noimage.png',
  `b_group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_datas`
--

INSERT INTO `user_datas` (`id`, `user_id`, `name`, `msisdn`, `address`, `b_date`, `image`, `b_group`, `gender`, `created_at`, `updated_at`) VALUES
(1, 1, 'Sabbir Hosain Chowdury', '8801684815187', 'Nikunja-2,Khilkhet,Dhaka', '1993-11-21 00:00:00', 'uploads/15486728609178.jpg', 'O+', 1, '2019-01-17 18:00:00', '2019-01-28 22:31:02'),
(2, 5, NULL, '', NULL, NULL, 'noimage.png', NULL, NULL, NULL, NULL),
(3, 6, NULL, '', NULL, NULL, 'noimage.png', NULL, NULL, NULL, NULL),
(4, 2, NULL, '', NULL, NULL, 'noimage.png', NULL, NULL, NULL, NULL),
(5, 3, NULL, '', NULL, NULL, 'noimage.png', NULL, NULL, NULL, NULL),
(6, 4, NULL, '', NULL, NULL, 'noimage.png', NULL, NULL, NULL, NULL),
(7, 8, NULL, NULL, NULL, NULL, 'noimage.png', NULL, NULL, '2019-01-28 04:50:58', '2019-01-28 04:50:58');

-- --------------------------------------------------------

--
-- Table structure for table `user_designations`
--

CREATE TABLE `user_designations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `designation_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_designations`
--

INSERT INTO `user_designations` (`id`, `user_id`, `designation_id`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2019-01-17 22:51:23', '2019-01-17 22:51:23'),
(2, 8, 3, '2019-01-28 05:08:37', '2019-01-28 05:08:37'),
(3, 2, 4, '2019-01-30 23:59:47', '2019-01-30 23:59:47'),
(4, 3, 4, '2019-01-31 00:00:01', '2019-01-31 00:00:01'),
(5, 4, 4, '2019-01-31 00:00:09', '2019-01-31 00:00:09'),
(6, 5, 4, '2019-01-31 00:00:17', '2019-01-31 00:00:17'),
(7, 6, 4, '2019-01-31 00:00:25', '2019-01-31 00:00:25'),
(8, 7, 3, '2019-01-31 00:00:32', '2019-01-31 00:00:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_memberships`
--

CREATE TABLE `user_memberships` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `membership_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `utility_costs`
--

CREATE TABLE `utility_costs` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `utility_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `use_date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `msisdn` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `name`, `image`, `address`, `website`, `msisdn`, `email`, `status`, `created_at`, `updated_at`) VALUES
(2, 'mrs shafiq and co', 'uploads/15475326369694.jpg', 'nikunja-2,khilkhet,dhaka', NULL, '8801684815187', 'shafiq@abc.com', 1, '2019-01-15 00:10:36', '2019-01-15 09:04:00');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_products`
--

CREATE TABLE `vendor_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `product_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_products`
--

INSERT INTO `vendor_products` (`id`, `vendor_id`, `product_title`, `product_code`, `product_price`, `created_by`, `created_at`, `updated_at`) VALUES
(30, 2, 'Pen', 'P-10', '12', 1, NULL, NULL),
(31, 2, 'SPA Water', 'S-1000', '25', 1, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `commisions`
--
ALTER TABLE `commisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `imports`
--
ALTER TABLE `imports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `items_identeti_number_unique` (`Identeti_number`);

--
-- Indexes for table `memberships`
--
ALTER TABLE `memberships`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payrolls`
--
ALTER TABLE `payrolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resalers`
--
ALTER TABLE `resalers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_offers`
--
ALTER TABLE `sales_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cascate` (`category_id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_datas`
--
ALTER TABLE `user_datas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_datas_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_designations`
--
ALTER TABLE `user_designations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `user_designations_user_id_foreign` (`user_id`),
  ADD KEY `user_designations_designation_id_foreign` (`designation_id`);

--
-- Indexes for table `user_memberships`
--
ALTER TABLE `user_memberships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_memberships_user_id_foreign` (`user_id`),
  ADD KEY `user_memberships_membership_id_foreign` (`membership_id`);

--
-- Indexes for table `utility_costs`
--
ALTER TABLE `utility_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor_products`
--
ALTER TABLE `vendor_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vendor_id` (`vendor_id`,`product_title`,`product_code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `commisions`
--
ALTER TABLE `commisions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `imports`
--
ALTER TABLE `imports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `memberships`
--
ALTER TABLE `memberships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `payrolls`
--
ALTER TABLE `payrolls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `resalers`
--
ALTER TABLE `resalers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `sales_offers`
--
ALTER TABLE `sales_offers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_datas`
--
ALTER TABLE `user_datas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_designations`
--
ALTER TABLE `user_designations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_memberships`
--
ALTER TABLE `user_memberships`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `utility_costs`
--
ALTER TABLE `utility_costs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `vendor_products`
--
ALTER TABLE `vendor_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `cascate` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `themes`
--
ALTER TABLE `themes`
  ADD CONSTRAINT `themes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_datas`
--
ALTER TABLE `user_datas`
  ADD CONSTRAINT `user_datas_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_designations`
--
ALTER TABLE `user_designations`
  ADD CONSTRAINT `user_designations_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_designations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_memberships`
--
ALTER TABLE `user_memberships`
  ADD CONSTRAINT `user_memberships_membership_id_foreign` FOREIGN KEY (`membership_id`) REFERENCES `memberships` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_memberships_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `vendor_products`
--
ALTER TABLE `vendor_products`
  ADD CONSTRAINT `vendor_products_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
