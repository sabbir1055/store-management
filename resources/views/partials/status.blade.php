<!-- error/success messages -->
<!-- @if (count($errors))
      <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
            @endforeach
            </ul>
      </div>
@endif -->
<!-- @if( Session::has('successMsg'))
      <p class="btn btn-block btn-success disabled successMsg m-b-30">{{ Session::get('successMsg') }}</p>
@endif
@if( Session::has('errorMsg') )
      <p class="btn btn-block btn-danger disabled errorMsg m-b-30">{{ Session::get('errorMsg') }}</p>
@endif  -->
<?php
    $successMsg = Session::get('successMsg');
    $errorMsg   = Session::get('errorMsg');
?>
<span id="successMsg" data="{{$successMsg}}"></span>
<span id="errorMsg" data="{{$errorMsg}}"></span>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-centre"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" style="display:block;margin:auto;">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END -->
