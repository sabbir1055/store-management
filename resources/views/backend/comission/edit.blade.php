@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Comission </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('comission.update',$comission->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="com-md-6">
                <label for="name"> Comission Amount : (In Tk)</label>
                <input type="text" class="form-control" name="amount" id="name" value="{{ $comission->amount }}" required>
            </div>
            <div class="com-md-6">
                <label for="discount"> Comission Rate : (in percent)</label>
                <input type="number" class="form-control" name="comission" id="discount" value="{{ $comission->commision }}" required>
            </div>

            <div class="com-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">UPDATE COMISSION</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
