@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Add Comission </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('comission.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="name"> Comission Amount : (In Tk)</label>
                <input type="text" class="form-control" name="amount" id="name" required>
            </div>
            <div class="col-md-6">
                <label for="discount"> Comission Rate : (in percent)</label>
                <input type="number" class="form-control" name="comission" id="discount" required>
            </div>

            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">ADD COMISSION</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
