@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Comission </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('comission.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Comission</button></a>
        </div>

        <table class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th class="text-center"> Comission Amount </th>
                    <th class="text-center"> Comission Rate </th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
                @if($comission)
                    @foreach($comission as $i => $value)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $value->amount }} Tk</td>
                            <td>{{ $value->commision }} %</td>
                            <td>
                                <a href="{{ route('comission.edit',$value->id) }}"> <button type="button" class="btn btn-info"> <i class="fa fa-edit"></i> </button> </a>
                                <a href="{{ route('comission.delete',$value->id) }}"> <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button> </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                  <th> SL </th>
                  <th class="text-center"> Comission Amount </th>
                  <th class="text-center"> Comission Rate </th>
                  <th class="text-center">Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
