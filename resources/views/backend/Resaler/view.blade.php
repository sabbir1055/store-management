@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Reseller # {{ $resaler->name }}</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-6" style="margin-top:2%">
            <label for=""> &nbsp </label>
            <img src="{{ asset($resaler->image) ?? asset('noimage.png') }}" alt="">
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <label for="name">Reseller Name : </label>
                <input type="text" class="form-control" name="name" id="name" value="{{$resaler->name}}" disabled>
            </div>
            <div class="col-md-12">
                <label for="email">Reseller Email : </label>
                <input type="email" class="form-control" name="email" id="email" value="{{$resaler->email}}" disabled>
            </div>
            <div class="col-md-12">
                <label for="contact">Reseller Contact No : </label>
                <input type="text" class="form-control" name="contact" id="contact" value="{{$resaler->contact}}" disabled>
            </div>
            <div class="col-md-12">
                <label for="district">Reseller District : </label>
                <input type="text" class="form-control" name="district" id="district" value="{{$resaler->district}}" disabled>
            </div>
            <div class="col-md-12">
                <label for="village">Reseller Village : </label>
                <input type="text" class="form-control" name="village" id="village" value="{{$resaler->village}}" disabled>
            </div>
            <div class="col-md-12">
                <label for="area">Reseller Area : </label>
                <input type="text" class="form-control" name="area" id="area" value="{{$resaler->area}}" disabled>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">Reseller Purchase Details</h3>
        </div>
    </div>
</div>
@endsection
