@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Add Reseller </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('resaler.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col-md-6">
                    <label for="name">Reseller Name : </label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="email">Reseller Email : </label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="contact">Reseller Contact No : </label>
                    <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="district">Reseller District : </label>
                    <input type="text" class="form-control" name="district" id="district" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="village">Reseller Village : </label>
                    <input type="text" class="form-control" name="village" id="village" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="area">Reseller Area : </label>
                    <input type="text" class="form-control" name="area" id="area" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="image">Reseller Image : </label>
                    <input type="file" class="form-control" name="image" id="district" placeholder="Enter resaler name here ........" value="" required>
                </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="submit" class="btn btn-success" value="ADD RESELLER">
            </div>
        </form>
    </div>
</div>
@endsection
