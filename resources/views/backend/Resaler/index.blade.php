@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Resellers </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('resaler.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Resaler</button></a>
        </div>

        <table id="VendorDataTable" class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <!-- <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="100"> -->
            <!--Table head-->
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>District</th>
                    <th>Area</th>
                    <th>Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->

            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact</th>
                    <th>District</th>
                    <th>Area</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
        var table = $('#VendorDataTable').DataTable({
            'processing' 	: true,
            'serverSide' 	: true,
            'ajax'			:{
                'url'       : '{{ route('resaler.data') }}',
                'dataType' 	: 'json',
                'type' 		: 'POST',
                'data'		: {
                    '_token'    : '{{ csrf_token() }}',
                }
            },
            'columns' 		: [
                { 'data' : 'id'},
                { 'data' : 'name'},
                { 'data' : 'email'},
                { 'data' : 'contact'},
                { 'data' : 'district'},
                { 'data' : 'area'},
                { 'data' : 'action','searchable':false,'orderable':false}
            ]
        });
    });
function status(e){
    $.ajax({
        url: '{{ route('resaler.status') }}',
        method: 'post',
        'data': {
            '_token'    : '{{ csrf_token() }}',
            'resaler_id' : e
        },
        success: function (data) {
            location.reload();
        }
    });
}
function edit(e){
    window.location.href = window.Laravel.base_url+"/resaler/edit/"+e;
}
function view(e){
    window.location.href = window.Laravel.base_url+"/resaler/view/"+e;
}
</script>
@endsection
