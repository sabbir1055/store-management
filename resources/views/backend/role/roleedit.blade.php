@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
		@include('partials.status')
		<h3 class="panel-title"> Edit Role </h3>
		<p class="text-muted m-b-30"> Fill all required field.</p>
	</div>
	<div class="panel-body">
		<form name="add_permission" action="{{ route('role.update',$role->id) }}" method="post">
			{{ csrf_field() }}
			<div class="col-md-6">
				<label for="name">Role Name : </label>
				<input type="text" class="form-control" name="name" id="name" value="{{ $role->name }}" required>
			</div>
			<div class="col-md-6">
				<label for="display_name">Role Display Name : </label>
				<input type="text" class="form-control" name="display_name" id="display_name"  value="{{ $role->display_name }}" required>
			</div>
			<div class="col-md-6 role">
				<label for="permission">Role Permission : </label>
				<ul>
					@if($permission)
						@foreach($permission as $r)
							<li> <input type="checkbox" name="permissions[]" value="{{ $r->id }}" @foreach($role->permissions as $p)
																									@if($p->id == $r->id)
																										checked
																									@endif
																									@endforeach> {{$r->display_name}} </li>
						@endforeach
					@endif
				</ul>
			</div>
			<div class="col-md-6">
				<label for="detail">Role Details : </label>
				<textarea name="detail" class="form-control textarea_editor" style="width:90%;height:10%">{!! $role->description !!}</textarea>
			</div>
			<div class="col-md-12 text-center" style="margin-top:5%;">
					<label for="">&nbsp</label>
					<input type="submit" class="btn btn-success" value="UPDATE">
			</div>
		</form>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$('.textarea_editor').wysihtml5();
	});
</script>
@endsection
