@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
		@include('partials.status')
		<h3 class="panel-title"> Edit Employee Role </h3>
		<p class="text-muted m-b-30"> Fill all required field.</p>
	</div>
	<div class="panel-body">
		<form name="add_permission" action="{{ route('role.employee.update') }}" method="post">
			{{ csrf_field() }}

			<div class="col-md-6 role">
				<label for="permission">Role : </label>
				<select class="form-control" name="role" name="role">
					<option value="">Select a role</option>
					@if($roles)
						@foreach($roles as $r)
							<option value="{{ $r->id }}">{{ $r->display_name }}</option>
						@endforeach
					@endif
				</select>
			</div>

			<div class="col-md-6 role">
				<label for="permission">Employee : </label>
				<select class="form-control" name="employee" name="role">
					<option value="">Select an employee</option>
					@if($user)
						@foreach($user as $r)
							<option value="{{ $r->id }}">{{ $r->data ? $r->data->name : $r->email }}</option>
						@endforeach
					@endif
				</select>
			</div>
			<div class="col-md-12 text-center" style="margin-top:5%;">
					<label for="">&nbsp</label>
					<input type="submit" class="btn btn-success" value="UPDATE">
			</div>
		</form>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript">
	$(document).ready(function(){
		$('.textarea_editor').wysihtml5();
	});
</script>
@endsection
