@extends('backend.layout.layout')
@section('content')
<div class="module">
    <div class="module-head">
        @include('partials.status')
        <h3> Update Vendor</h3>
    </div>
    <div class="module-body">
        <form class="form-horizontal row-fluid" action="{{ route('vendor.update',$vendor->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @if($vendor->image)
            <div class="control-group" style="padding:10px;margin-left:25%;">
                <img src="{{ asset($vendor->image) }}" class="img img-responsive img-circle"alt="">
            </div>
            @endif
            <div class="control-group">
                <label class="control-label" for="name">Vendor Name : </label>
                <div class="controls">
                    <input type="text" id="name" name="name" placeholder="Type name here..." class="span8" value="{{ $vendor->name }}" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="email">Vendor Email : </label>
                <div class="controls">
                    <input type="email" id="email" name="email" value="{{ $vendor->email }}" placeholder="Type email here..." class="span8" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="image">Vendor Image : </label>
                <div class="controls">
                    <input type="file" id="image" name="image" class="span8">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="msisdn">Vendor Mobile : </label>
                <div class="controls">
                    <input type="text" name="country_code" value="+880" style="width:5%;" disabled>
                    <input type="number" id="msisdn" name="msisdn" value="{{ substr($vendor->msisdn,-10) }}" class="span8" style="width:58%;" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="address">Vendor Address : </label>
                <div class="controls">
                    <input type="text" id="address" value="{{ $vendor->address }}" name="address" class="span8">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="website">Vendor Website : </label>
                <div class="controls">
                    <input type="text" id="website" value="{{ $vendor->website }}" name="website" class="span8">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="">&nbsp </label>
                <div class="controls">
                    <button type="submit" class="btn btn-success" > UPDATE VENDOR </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
