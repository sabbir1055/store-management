@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Sell Product </h3>
        <h3> Seller : {{ @Auth::user()->data->name }} </h3>
    </div>
    <div class="panel-body">
        <div class="col-md-6" >
                <label for="name">Select Sell Type: </label>
                    <select id="sell_type" class="form-control">
                        <option value=""> Select sell type </option>
                        <option value="1"> Resaler sell </option>
                        <option value="2"> Retailer sell </option>
                    </select>
        </div>
        <form action="{{ route('sell.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col-md-6" id="select__resaler" style="display: none;">
                    <label for="name"> Resaler Name : </label>
                        <select  name="resaler_id" class="form-control resaler_name">
                            <option value="">Select a Resaler</option>
                            @if($resaler)
                                @foreach($resaler as $key=>$value)
                                    <option value="{{ $value->id }}"> {{ $value->name }} </option>
                                @endforeach
                            @endif
                        </select>
                </div>
                <div class="col-md-12" id="import_product_list" style="margin-top:30px;display:block;">
                        <table class="table table-dark table-responsive">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>CODE</th>
                                <th>TITLE</th>
                                <th>UNIT PRICE</th>
                                <th>QTY</th>
                                <th>TOTAL</th>
                                <th><button type="button" title="add row" class="button button5 table_row_add_button" style="border-radius:50%;"> <i class="fa fa-plus"></i> </button></th>
                              </tr>
                            </thead>
                            <tbody id="product_list">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td>
                                        Total payment : <input type="text" name="total_payment" id="total_payment" class="form-control" value="0" readonly>
                                    </td>
                                    <td>
                                        Total paid : <input type="text" name="total_paid" id="total_paid" class="form-control" value="0">
                                    </td>
                                    <td>
                                        Total due : <input type="text" name="total_due" id="total_due" class="form-control" value="0" readonly>
                                    </td>
                                    <td></td>
                                </tr>
                            </tfoot>
                        </table>
                </div>
                <div class="col-md-12">
                    <div class="text-center"  style="display:block;">
                        <button type="submit" class="btn btn-success"> MAKE SELL </button>
                    </div>
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        var data;
        $(document).ready(function(){
                $.ajax({
                    url: '{{ route('sell.product') }}',
                    method: 'post',
                    'data': {
                        '_token'    : '{{ csrf_token() }}'
                    },
                    success: function (d) {
                        data = JSON.parse(d);
                        var dom2 = '';
                        dom2 = `<tr>
                                    <td><h5>#</h5></td>
                                    <td>
                                    <select name="product_code[]" class="form-control" id="product_code" >
                                        <option value="">Select product</option>`;
                                    $.each( data, function( key, value ) {
                                        if(value.stock != 0){
                                            dom2+=`<option value="`+value.product_code+`">`+value.product_code+`(`+value.stock+`)`+`</option>`;
                                        }
                                    });
                        dom2+=     `</select>
                                    </td>
                                    <td>
                                        <input type="text" name="product_title[]" id="product_title" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input type="number" name="product_price[]" id="product_price" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input type="number" name="product_qty[]" id="product_qty" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" id="product_total" class="form-control" disabled>
                                    </td>
                                </tr>`;
                        $('#product_list').html(dom2);
                        $('#import_product_list').attr('style','display:block');
                    }
                });
        });
        $(document).on('change','#sell_type',function(){
            if($(this).val() == 1){
                $('#select__resaler').attr('style','display:block');
            }
            else{
                $('#select__resaler').attr('style','display:none');
            }
        });
        $(document).on('change','#product_code',function(){
            var dom = $(this);
                var p_id = $(this).val();
                $.each( data, function( key, value ) {
                    if(value.product_code == p_id){
                        dom.parent().parent().find('#product_price').val(value.price);
                        dom.parent().parent().find('#product_title').val(value.title);
                        dom.parent().parent().find('#product_qty').val(1);
                        dom.parent().parent().find('#product_total').val(value.price*1);
                        var product_prices = $("input[id='product_price']")
                          .map(function(){return $(this).val();}).get();

                          var product_qties = $("input[id='product_qty']")
                            .map(function(){return $(this).val();}).get();
                            var total = 0;
                        $.each(product_prices,function(i,value){
                            total += (value*product_qties[i]);
                        });
                        $('#total_payment').val(total);
                        $('#total_due').val(total);
                    }
                });
        });
        $(document).on('keyup','#product_qty',function(){
            var price = $(this).parent().parent().find('#product_price').val();
            var qty = $(this).parent().parent().find('#product_qty').val();
            $(this).parent().parent().find('#product_total').val(price*qty);
            var product_prices = $("input[id='product_price']")
              .map(function(){return $(this).val();}).get();

              var product_qties = $("input[id='product_qty']")
                .map(function(){return $(this).val();}).get();
                var total = 0;
            $.each(product_prices,function(i,value){
                total += (value*product_qties[i]);
            });
            var paid  = $('#total_paid').val();
            $('#total_payment').val(total);
            $('#total_due').val(total-paid);
        });
        $(document).on('click','.table_row_add_button',function(){
            var dom2 ="";
            dom2 = `<tr>
                        <td><h5>#</h5></td>
                        <td>
                        <select name="product_code[]" class="form-control" id="product_code" >
                            <option value="">Select product</option>`;
                        $.each( data , function( key, value ) {
                            if(value.stock != 0){
                                dom2+=`<option value="`+value.product_code+`">`+value.product_code+`(`+value.stock+`)`+`</option>`;
                            }
                        });
            dom2+=     `</select>
                        </td>
                        <td>
                            <input type="text" name="product_title[]" id="product_title" class="form-control" readonly>
                        </td>
                        <td>
                            <input type="number" name="product_price[]" id="product_price" class="form-control" readonly>
                        </td>
                        <td>
                            <input type="number" name="product_qty[]" id="product_qty" class="form-control">
                        </td>
                        <td>
                            <input type="text" id="product_total" class="form-control" disabled>
                        </td>
                        <td> <button type="button" class="button button5 table_row_remove_button" style="border-radius:50%;"> <i class="fa fa-minus"></i> </button> </td>
                    </tr>`;
                $('#product_list').append(dom2);
        });
        $(document).on('click','.table_row_remove_button',function(){
            $(this).parent().parent().remove();
        });
        $(document).on('keyup','#total_paid',function(){
            var total  = $('#total_payment').val();
            var paid  = $('#total_paid').val();
            $('#total_due').val(total-paid);
        });
    </script>
@endsection
