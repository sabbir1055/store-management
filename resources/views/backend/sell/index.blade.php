@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Sales </h3>
    </div>
    <div class="panel-body">
        <div class="row">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('sell.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Sell Product </button></a>
        </div>

        <table id="VendorDataTable" class="table table-hover table-fixed table-responsive">
            <thead class="table--head">
                <tr>
                    <th> Show Products </th>
                    <th> Sell Id </th>
                    <th> Seller </th>
                    <th> Reseller </th>
                    <th>Total</th>
                    <th>Paid</th>
                    <th>Due</th>
                    <th>Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->

            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> Show Products </th>
                    <th> Sell Id </th>
                    <th> Seller </th>
                    <th> Reseller </th>
                    <th>Total</th>
                    <th>Paid</th>
                    <th>Due</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
    </div>
</div>

<?php
if(session('lastRequestPdfDownloadData')){
    $invoice = new \App\invoice\SellInvoice();
    $rec = $invoice->invoice(session('lastRequestPdfDownloadData'));
    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadHTML($rec);
    return $pdf->download();
}
?>
@endsection
@section('script')
<script type="text/javascript">
function format ( d ) {
    // `d` is the original data object for the row
    var dom = '';
    var i = 0;
    $.each( d, function( keyA, values ){
        $.each(values, function( key, value ){
            dom+= ` <table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">
                        <span> `+(++i)+`/  </span>
                        <tr>
                            <td>Product Title : </td>
                            <td>`+value.product_code+`</td>
                        </tr>
                        <tr>
                            <td>Product Code : </td>
                            <td>`+value.product_id+`</td>
                        </tr>
                        <tr>
                            <td>Product Price : </td>
                            <td>`+value.product_price+`</td>
                        </tr>
                        <tr>
                            <td>Product Resaller : </td>`;
                            if(value.resaler_id == 0){
                                dom+=  `<td>Direct sold to customer</td>`;
                            }else{
                                dom+=  `<td>`+value.resaler_id+`</td>`;
                            }
                    dom+=  `</tr>
                        <tr>
                            <td>Product Identeti : </td>
                            <td> `+value.Identeti_number+` </td>
                        </tr>
                    </table>`;
        });
    });
    return dom;
    }
$(document).ready(function(){
    var table = $('#VendorDataTable').DataTable({
        'processing' 	: true,
        'serverSide' 	: true,
        'ajax'			:{
            'url'       : '{{ route('sell.data') }}',
            'dataType' 	: 'json',
            'type' 		: 'POST',
            'data'		: {
                '_token'    : '{{ csrf_token() }}',
            }
        },
        'columns' 		: [
            {
                "className":'details-control',
                "orderable":false,
                "searchable":false,
                "defaultContent":'<i class="fa fa-eye"></i>',
            },
            { 'data' : 'id'},
            { 'data' : 'saller_id',"className":'text-center'},
            { 'data' : 'resaller_id',"className":'text-center'},
            { 'data' : 'total'},
            { 'data' : 'paid'},
            { 'data' : 'due'},
            { 'data' : 'action','searchable':false,'orderable':false,"className":'text-center'}
        ]
    });
    $('#VendorDataTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            // console.log();
            row.child( format(JSON.parse(row.data().products)) ).show();
            tr.addClass('shown');
        }
    } );
});
function view(e){
    window.location.href = window.Laravel.base_url+"/sell/view/"+e;
}
</script>
@endsection
