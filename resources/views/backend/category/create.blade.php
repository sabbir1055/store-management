@extends('backend.layout.layout')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Add Category </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col-md-6">
                    <label for="name"> Category Name : </label>
                    <input type="text" class="form-control" name="title" >
                </div>
                <div class="col-md-6">
                    <label for="image"> Category Icon : </label>
                    <input type="file" class="form-control" name="image" required>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for=""> &nbsp </label>
                    <button type="submit" class="btn btn-success">ADD CATEGORY</button>
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">

    </script>
@endsection
