@extends('backend.layout.layout')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Category </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('category.update',$category->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col-md-6">
                    <label for="name"> Category Name : </label>
                    <input type="text" class="form-control" name="title" value = "{{ $category->title }}">
                </div>
                <div class="col-md-6">
                    <label for="image"> Category Icon : </label>
                    <input type="file" class="form-control" name="image">
                </div>
                <div class="col-md-12">
                        @if($category->image)
                            <img src="{{asset($category->image)}}" style="width:200px;height:200px; margin-left:65%;">
                        @endif
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for=""> &nbsp </label>
                    <button type="submit" class="btn btn-success">UPDATE CATEGORY</button>
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">

    </script>
@endsection
