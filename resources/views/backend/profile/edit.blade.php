@extends('backend.layout.layout')
@section('css')
<style media="screen">

</style>
@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3 class="panel-title">Edit Profile</h3>
        <p class="text-muted m-b-30"> Fill all required field.</p>
    </div>
    <div class="panel-body">
        <form name="add_permission" action="{{ route('profile.update') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="col-md-12">
                    <img src="@if(Auth::user()->data) {{asset((Auth::user()->data->image !='') ? Auth::user()->data->image : 'noimage.png')}} @endif" class="img img-responsive" style="width:100%;"/>
                </div>
                <label for="image">Choose new Image : </label>
                <input type="file" class="form-control" id="image" name="image" value="">
            </div>
            <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
                <label for="">Email : </label>
                <input type="text" class="form-control" value="{{ Auth::user()->email }}" disabled>
                <label for="">Role : </label>
                <input type="text" class="form-control" value="{{ Auth::user()->role->display_name }}" disabled>
                <label for="password">New Password : </label>
                <input type="password" id="password" name="password" class="form-control">
            </div>
            <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
                <div class="col-md-12 text-center">
                    <span>Basic Informations</span>
                </div>
                <label for="name">Name : </label>
                <input type="text" class="form-control" id="name" name="name" value="@if(Auth::user()->data) {{Auth::user()->data->name ?? ' '}} @else ' ' @endif">
                <label for="msisdn">Msisdn : </label>
                <input type="text" class="form-control" id="msisdn" name="msisdn" value="@if(Auth::user()->data) {{Auth::user()->data->msisdn ?? ' '}} @else ' ' @endif">
                <label for="b_date">Birthday : </label>
                <input type="date" class="form-control" id="b_date" name="b_date" value="@if(Auth::user()->data){{(string)substr(Auth::user()->data->b_date,0,10)??' '}}@else ' '@endif">
                <label for="address">Address : </label>
                <input type="text" class="form-control" id="address" name="address" value="@if(Auth::user()->data) {{Auth::user()->data->address ?? ' '}} @else ' ' @endif">
                <label for="b_group">Blood group : </label>
                <input type="text" class="form-control" id="b_group" name="b_group" value="@if(Auth::user()->data) {{Auth::user()->data->b_group ?? ' '}} @else ' ' @endif">
                <label for="gender">Gender : </label>
                <select class="form-control" id="gender" name="gender">
                    <option value="1" @if(Auth::user()->data) @if(Auth::user()->data->gender == 1) selected @endif @endif>Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label for="">&nbsp</label>
                <button type="submit" class="btn btn-success" name="button">UPDATE</button></a>
            </div>
        </form>
    </div>
</div>
@endsection
