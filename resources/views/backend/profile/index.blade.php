@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3 class="panel-title">Profile</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-6">
            <div class="col-md-12">
                <img src="@if(Auth::user()->data) {{asset((Auth::user()->data->image !='') ? Auth::user()->data->image : 'noimage.png')}} @else {{asset('noimage.png')}} @endif" class="img img-responsive" style="width:100%;"/>
            </div>
        </div>
        <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
            <label for="">Email : </label>
            <input type="text" class="form-control" value="{{ Auth::user()->email }}" disabled>
            <label for="">Role : </label>
            <input type="text" class="form-control" value="{{ Auth::user()->role->display_name }}" disabled>
        </div>
        <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
            <div class="col-md-12 text-center">
                <span>Basic Informations</span>
            </div>
            <label for="">Name : </label>
            <input type="text" class="form-control" value="@if(Auth::user()->data) {{Auth::user()->data->name ?? ' '}} @else ' ' @endif" disabled>
            <label for="">Msisdn : </label>
            <input type="text" class="form-control" value="@if(Auth::user()->data) {{Auth::user()->data->msisdn ?? ' '}} @else ' ' @endif" disabled>
            <label for="">Birthday : </label>
            <input type="text" class="form-control" value="@if(Auth::user()->data) {{substr(Auth::user()->data->b_date,0,10) ?? ' '}} @else ' ' @endif" disabled>
            <label for="">Address : </label>
            <input type="text" class="form-control" value="@if(Auth::user()->data) {{Auth::user()->data->address ?? ' '}} @else ' ' @endif" disabled>
            <label for="">Blood group : </label>
            <input type="text" class="form-control" value="@if(Auth::user()->data) {{Auth::user()->data->b_group ?? ' '}} @else ' ' @endif" disabled>
            <label for="">Gender : </label>
            <input type="text" class="form-control" value="@if(Auth::user()->data) @if(Auth::user()->data->gender == 1) Male @else Female @endif  @else ' ' @endif" disabled>
        </div>
        <div class="col-md-12 text-center" style="margin-top:5%;">
            <label for="">&nbsp</label>
            <a href="{{ route('profile.edit') }}"><button type="button" class="btn btn-primary" name="button">EDIT</button></a>
        </div>
    </div>
</div>
@endsection
