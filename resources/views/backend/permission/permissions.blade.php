@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
		@include('partials.status')
		<h3 class="panel-title">All Permissions</h3>
	</div>
	<div class="panel-body">
		<div class="pull-right" style="padding:10px;">
				<a href="{{ route('permissions.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Permission</button></a>
		</div>

		<table id="employee" class="table table-hover table-fixed" style="text-align:center;">

			<!--Table head-->
			<thead class="table--head">
				<tr>
					<td>#</td>
					<td>Name</td>
					<td>Display Name</td>
					<td>Action</td>
				</tr>
			</thead>
			<!--Table head-->

			<!--Table body-->
			<tbody>
				@if($permission)
				@foreach($permission as $key => $p)
				<tr>
					<td>{{ ++$key }}</td>
					<td>{{ $p->name }}</td>
					<td>{{ $p->display_name }}</td>
					<td> <a onclick="return confirm('Are you sure to delete?')" href="{{ route('permissions.delete',$p->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a> </td>
				</tr>
				@endforeach
				@endif
			</tbody>
			<!--Table body-->
		</table>
	</div>
</div>
@endsection
@section('script')

@endsection
