@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        <h3 class="panel-title">Overview</h3>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-download"></i></span>
                    <p>
                        <span class="number">{{ $stock }}</span>
                        <span class="title">Product in stock</span>
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                    <p>
                        <span class="number">{{ $sold }}</span>
                        <span class="title">Sales</span>
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-exclamation-circle"></i></span>
                    <p>
                        <span class="number">{{ $returned }}</span>
                        <span class="title">Returns</span>
                    </p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="metric">
                    <span class="icon"><i class="fa fa-exclamation-triangle"></i></span>
                    <p>
                        <span class="number">{{ $damaged }}</span>
                        <span class="title">Damages</span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <div id="daily_chart" style="width:100%;height:100%;"></div>
            </div>
            <div class="col-md-3">
                <div class="weekly-summary text-right">
                    <span class="number">{{ $sold_today }} Tk</span> <span class="percentage">@if($today_growth>0)<i class="fa fa-caret-up text-success"> @else <i class="fa fa-caret-down text-danger">@endif</i> {{ abs(substr($today_growth,0,6)) }}%</span>
                    <span class="info-label">Today Income</span>
                </div>
                <div class="weekly-summary text-right">
                    <span class="number">{{ $sold_month }} Tk</span> <span class="percentage">@if($monthly_growth>0)<i class="fa fa-caret-up text-success"> @else <i class="fa fa-caret-down text-danger">@endif</i> {{ abs(substr($monthly_growth,0,6)) }}%</span>
                    <span class="info-label">Monthly Income</span>
                </div>
                <div class="weekly-summary text-right">
                    <span class="number">{{ $sold_total }} Tk</span> <span class="percentage">
                        <span class="info-label">Total Income</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <!-- RECENT PURCHASES -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Recent Top Sales</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <!-- <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button> -->
                    </div>
                </div>
                <div class="panel-body no-padding">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Order No.</th>
                                <th>Name</th>
                                <th>Amount</th>
                                <th>Date &amp; Time</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($sales)
                            @foreach($sales as $key=>$sale)
                            <tr>
                                <td>{{$sale->id}}</td>
                                <td>{{$sale->employee->data->name ?? $sale->employee->email}}</td>
                                <td>{{$sale->total}}</td>
                                <td>{{substr($sale->created_at,0,10)}}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-md-12 text-right"><a href="{{ route('sell.index') }}" class="btn btn-primary">View All Purchases</a></div>
                    </div>
                </div>
            </div>
            <!-- END RECENT PURCHASES -->
        </div>
        <div class="col-md-6">
            <!-- MULTI CHARTS -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Projection vs. Realization</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <!-- <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button> -->
                    </div>
                </div>
                <div class="panel-body">
                    <div id="visits-trends-chart" class="ct-chart"></div>
                </div>
            </div>
            <!-- END MULTI CHARTS -->
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <!-- TASKS -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">My Tasks</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <!-- <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button> -->
                    </div>
                </div>
                <div class="panel-body">
                    <ul class="list-unstyled task-list">
                        <li>
                            <p>Updating Users Settings <span class="label-percent">23%</span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100" style="width:23%">
                                    <span class="sr-only">23% Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p>Load &amp; Stress Test <span class="label-percent">80%</span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                    <span class="sr-only">80% Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p>Data Duplication Check <span class="label-percent">100%</span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                    <span class="sr-only">Success</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p>Server Check <span class="label-percent">45%</span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                                    <span class="sr-only">45% Complete</span>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p>Mobile App Development <span class="label-percent">10%</span></p>
                            <div class="progress progress-xs">
                                <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    <span class="sr-only">10% Complete</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END TASKS -->
        </div>
        <div class="col-md-4">
            <!-- VISIT CHART -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Website Visits</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <!-- <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button> -->
                    </div>
                </div>
                <div class="panel-body">
                    <div id="visits-chart" class="ct-chart"></div>
                </div>
            </div>
            <!-- END VISIT CHART -->
        </div>
        <div class="col-md-4">
            <!-- REALTIME CHART -->
            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">System Load</h3>
                    <div class="right">
                        <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                        <!-- <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button> -->
                    </div>
                </div>
                <div class="panel-body">
                    <div id="system-load" class="easy-pie-chart" data-percent="70">
                        <span class="percent">70</span>
                    </div>
                    <h4>CPU Load</h4>
                    <ul class="list-unstyled list-justify">
                        <li>High: <span>95%</span></li>
                        <li>Average: <span>87%</span></li>
                        <li>Low: <span>20%</span></li>
                        <li>Threads: <span>996</span></li>
                        <li>Processes: <span>259</span></li>
                    </ul>
                </div>
            </div>
            <!-- END REALTIME CHART -->
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
var chart_data = JSON.stringify({!! json_encode($sales_chart) !!});
var chart_data = $.parseJSON(chart_data);
// console.log(chart_data);
google.charts.load('current', {'packages':['line']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

    var data = new google.visualization.DataTable();

    data.addColumn('string', 'Day');
    data.addColumn('number', 'Sold');
    data.addColumn('number', 'Profit');

    data.addRows(chart_data);

    var options = {
        chart: {
            title: 'Total sales and profit',
            subtitle: 'in taka'
        },
        width: 750,
        height: 350,
        axes: {
            x: {
                0: {side: 'bottom'}
            }
        }
    };

    var chart = new google.charts.Line(document.getElementById('daily_chart'));

    chart.draw(data, google.charts.Line.convertOptions(options));
    google.visualization.events.addListener(chart, 'select', selectHandler);

    function selectHandler() {
        var selection = chart.getSelection();
        var message = '';
        for (var i = 0; i < selection.length; i++) {
            var item = selection[i];
            if (item.row != null && item.column != null) {
                var str = data.getFormattedValue(item.row, item.column);
                var date = data.getValue(chart.getSelection()[0].row, 0);
                getBydate(date);
            }
        }

    }

}
function getBydate(date){
    $.ajax({
        url: '{{ route('dashboard.saleschart') }}',
        method: 'post',
        'data'			: {
            '_token'    : '{{ csrf_token() }}',
            'date'      : date
        },
        success: function (response) {
            var response = JSON.parse(response);
            google.charts.load("current", {packages:['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var datas = google.visualization.arrayToDataTable(response);

                var view = new google.visualization.DataView(datas);
                view.setColumns([0, 1,
                    { calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation" },
                    2]);

                    var options = {
                        title: "Sales on selected day",
                        width: 750,
                        height: 350,
                        bar: {groupWidth: "95%"},
                        legend: { position: "none" },
                    };
                    var chart = new google.visualization.ColumnChart(document.getElementById("daily_chart"));
                    chart.draw(view, options);

                    google.visualization.events.addListener(chart, 'select', selectHandler);

                    function selectHandler() {
                        var selection = chart.getSelection();
                        var message = '';
                        for (var i = 0; i < selection.length; i++) {
                            var item = selection[i];
                            if (item.row != null && item.column != null) {
                                var str = view.getFormattedValue(item.row, item.column);
                                var name = view.getValue(chart.getSelection()[0].row, 0);
                                getByName(name);
                            }
                        }
                    }
                }
            }
        });
    }

    function getByName(name){
        $.ajax({
            url: '{{ route('dashboard.saleschart') }}',
            method: 'post',
            'data'			: {
                '_token'    : '{{ csrf_token() }}',
                'name'      : name
            },
            success: function (response) {
                var response = JSON.parse(response);
                var data = new google.visualization.DataTable();

                data.addColumn('string', 'Day');
                data.addColumn('number', 'Sold');
                data.addColumn('number', 'Profit');

                data.addRows(response);

                var options = {
                    chart: {
                        title: 'Total sales and profit',
                        subtitle: 'in taka'
                    },
                    width: 750,
                    height: 350,
                    axes: {
                        x: {
                            0: {side: 'bottom'}
                        }
                    }
                };

                var chart = new google.charts.Line(document.getElementById('daily_chart'));

                chart.draw(data, google.charts.Line.convertOptions(options));
                google.visualization.events.addListener(chart, 'select', selectHandler);

                function selectHandler() {
                    var selection = chart.getSelection();
                    var message = '';
                    for (var i = 0; i < selection.length; i++) {
                        var item = selection[i];
                        if (item.row != null && item.column != null) {
                            var str = data.getFormattedValue(item.row, item.column);
                            var date = data.getValue(chart.getSelection()[0].row, 0);
                            getBydate(date);
                        }
                    }

                }
            }
        });
    }
    </script>
    @endsection
