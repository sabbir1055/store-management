@include('backend.layout.header')
@include('backend.layout.left-side')
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
</div>
@include('backend.layout.footer')
