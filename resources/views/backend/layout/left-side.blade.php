<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li>
                    <a href="{{ route('dashboard') }}"><i class="lnr lnr-home"></i>Dashboard</a>
                </li>
                @permission('can-modify-product')
                  <li>
                      <a href="#product" data-toggle="collapse" class="collapsed"><i class="lnr lnr-dice"></i> <span>Products</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                  <div id="product" class="collapse ">
                      <ul class="nav">
                          <li><a href="{{ route('product.index') }}"><i class="lnr lnr-list"></i> All Products </a></li>
                          <li><a href="{{ route('product.create') }}"><i class="lnr lnr-download"></i> Add Product </a></li>
                          <li><a href="{{ route('identifier.index') }}"><i class="lnr lnr-question-circle"></i> All Product Identifier </a></li>
                          <li><a href="{{ route('product.report') }}"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-vendor')
                  <li>
                      <a href="#vendor" data-toggle="collapse" class="collapsed"><i class="lnr lnr-cloud"></i> <span>Vendor</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="vendor" class="collapse ">
                          <ul class="nav">
                              <li><a href="{{ route('vendor.index') }}"><i class="lnr lnr-list"></i> All Vendors </a></li>
                              <li><a href="{{ route('vendor.create') }}"><i class="lnr lnr-download"></i> Add Vendor </a></li>
                              <li><a href="{{ route('vendor.product') }}"><i class="lnr lnr-download"></i> Add Vendor Product </a></li>
                              <li><a href="{{ route('vendor.report') }}"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                          </ul>
                      </div>
                  </li>
                  @endpermission
                  @permission('can-modify-import')
                  <li>
                      <a href="#import" data-toggle="collapse" class="collapsed"><i class="lnr lnr-cloud-upload"></i> <span>Import</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="import" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('import.index') }}"><i class="lnr lnr-list"></i> All Import Products </a></li>
                          <li><a href="{{ route('import.create') }}"><i class="lnr lnr-download"></i> Add Product </a></li>
                          <li><a href="{{ route('import.report') }}"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-sell')
                  <li>
                      <a href="#sell" data-toggle="collapse" class="collapsed"><i class="lnr lnr-cart"></i> <span>Sales</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="sell" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('sell.index') }}"><i class="lnr lnr-list"></i> All Seles </a></li>
                          <li><a href="{{ route('sell.create') }}"><i class="lnr lnr-download"></i> Make Sale </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission

                  @permission('can-modify-employee')
                  <li>
                      <a href="#user" data-toggle="collapse" class="collapsed"><i class="lnr lnr-users"></i> <span>Employee</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="user" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('users') }}"><i class="lnr lnr-list"></i> All Employees </a></li>
                          <li><a href="{{ route('employee.create') }}"><i class="lnr lnr-download"></i> Add Employee </a></li>
                          <li><a href="{{ route('employee.designation') }}"><i class="lnr lnr-license"></i> Add Designation</a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-resaler')
                  <li>
                      <a href="#resaler" data-toggle="collapse" class="collapsed"><i class="lnr lnr-briefcase"></i> <span>Reseller</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="resaler" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('resaler.index') }}"><i class="lnr lnr-list"></i> All Resellers </a></li>
                          <li><a href="{{ route('resaler.create') }}"><i class="lnr lnr-download"></i> Add Reseller </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-employee')
                  <li>
                      <a href="#designation" data-toggle="collapse" class="collapsed"><i class="lnr lnr-laptop"></i> <span>Designation</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="designation" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('designation.index') }}"><i class="lnr lnr-list"></i> All Designations </a></li>
                          <li><a href="{{ route('designation.create') }}"><i class="lnr lnr-download"></i> Add Designation </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-payroll')
                  <li>
                      <a href="#payroll" data-toggle="collapse" class="collapsed"><i class="lnr lnr-license"></i> <span>Payroll</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="payroll" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('payroll.index') }}"><i class="lnr lnr-list"></i> All Salerysheets </a></li>
                          <li><a href="{{ route('payroll.create') }}"><i class="lnr lnr-download"></i> Create Sallerysheet </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-membership')
                  <li>
                      <a href="#member" data-toggle="collapse" class="collapsed"><i class="lnr lnr-hand"></i> <span>Membership</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="member" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('membership.index') }}"><i class="lnr lnr-list"></i> All Membership</a></li>
                          <li><a href="{{ route('membership.create') }}"><i class="lnr lnr-download"></i> Add Membership </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-salesoffer')
                  <li>
                      <a href="#salesoffer" data-toggle="collapse" class="collapsed"><i class="lnr lnr-thumbs-up"></i> <span>Sales Offer</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="salesoffer" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('salesoffer.index') }}"><i class="lnr lnr-list"></i> All offers </a></li>
                          <li><a href="{{ route('salesoffer.create') }}"><i class="lnr lnr-download"></i> Add offer </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-commission')
                  <li>
                      <a href="#commision" data-toggle="collapse" class="collapsed"><i class="lnr lnr-sync"></i> <span>Commision</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="commision" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('comission.index') }}"><i class="lnr lnr-list"></i> All Commisions </a></li>
                          <li><a href="{{ route('comission.create') }}"><i class="lnr lnr-download"></i> Add Commisions </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-role')
                  <li>
                      <a href="#role" data-toggle="collapse" class="collapsed"><i class="lnr lnr-lock"></i> <span>Role & Permission</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="role" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('permissions') }}"><i class="lnr lnr-list"></i> All Permissions </a></li>
                          <!-- <li><a href="{{ route('permissions.create') }}"><i class="icon-plus"></i> Add Permission </a></li> -->
                          <li><a href="{{ route('roles') }}"><i class="lnr lnr-list"></i> All Roles </a></li>
                          <li><a href="{{ route('role.create') }}"><i class="lnr lnr-download"></i> Add Role </a></li>
                          <li><a href="{{ route('role.employee.edit') }}"><i class="lnr lnr-user"></i> Update Employee... </a></li>
                      </ul>
                      <div>
                  </li>
                  @endpermission

                  @permission('can-modify-accounts')
                  <li>
                      <a href="#account"><i class="lnr lnr-chart-bars"></i> Accounts </a>
                  </li>
                  <li>
                      <a href="#othercosts" data-toggle="collapse" class="collapsed"><i class="lnr lnr-layers"></i> <span>Other costs</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="othercosts" class="collapse ">
                          <ul class="nav">
                          <li><a href="#"><i class="lnr lnr-list"></i> All Costs </a></li>
                          <li><a href="#"><i class="lnr lnr-download"></i> Add Costs </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </div>
                  </li>
                  @endpermission
                  @permission('can-modify-category')
                  <li>
                      <a href="#category" data-toggle="collapse" class="collapsed"><i class="lnr lnr-indent-increase"></i> <span>Category & Subc...</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a></i></a>
                      <div id="category" class="collapse ">
                          <ul class="nav">
                          <li><a href="{{ route('category.index') }}"><i class="lnr lnr-list"></i> All Category </a></li>
                          <li><a href="{{ route('category.create') }}"><i class="lnr lnr-download"></i> Add Category </a></li>
                          <li><a href="{{ route('subcategory.index') }}"><i class="lnr lnr-list"></i> All Subcategory </a></li>
                          <li><a href="{{ route('subcategory.create') }}"><i class="lnr lnr-download"></i> Add Subcategory </a></li>
                          <li><a href="#"><i class="lnr lnr-pie-chart"></i> Report </a></li>
                      </ul>
                  </li>
                  @endpermission
                  <li>
                      <a href="{{ route('logout') }}"><i class="lnr lnr-exit"></i>Logout </a>
                  </li>
            </ul>
        </nav>
    </div>
</div>
<!-- END LEFT SIDEBAR -->
