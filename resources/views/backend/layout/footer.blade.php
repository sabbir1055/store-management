<div class="clearfix"></div>
<footer>
    <div class="container-fluid">
        <p class="copyright">&copy; 2017 <a href="https://www.themeineed.com" target="_blank">Theme I Need</a>. All Rights Reserved.</p>
    </div>
</footer>
</div>
<!-- END WRAPPER -->
<!-- Javascript -->
<script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
<script src="{{asset('assets/vendor/chartist/js/chartist.min.js')}}"></script>
<script src="{{asset('assets/scripts/klorofil-common.js')}}"></script>
<script src="{{asset('datatable/dataTable.min.js')}}" type="text/javascript"></script>
<script src="{{asset('bower_components/html5-editor/wysihtml5-0.3.0.js')}}" charset="utf-8"></script>
<script src="{{ asset('bower_components/html5-editor/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/scripts/googleChart.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var height = $('#left-side-span').height();
        $('#span10').attr('style','height:'+height+'px');

        $('#textarea').wysihtml5();

        var successMsg  = $('#successMsg').attr('data');
        var errorMsg    = $('#errorMsg').attr('data');
        if(successMsg){
            var dom = `<h4 class="text-center" style="font-family:cursive">`+successMsg+`</h4>
                        <img src="{{asset('success.png')}}" style="padding: 20px;width: 30%;display:block;margin:auto;">`;
            $('.modal-title').text('Success');
            $('.modal-body').html(dom);
            $('.modal-body').css('background','#d3fc99');
            $('#myModal').modal({
							show: true,
							keyboard: false,
							backdrop: 'static'
						});
        }else if(errorMsg) {
            var dom = `<p class="text-center">`+errorMsg+`</p>
                        <img src="{{asset('error.png')}}" style="padding: 20px;width: 30%;display:block;margin:auto;">`;
            $('.modal-title').text('Error');
            $('.modal-body').html(dom);
            $('.modal-body').css('background','#fcd799');
            $('#myModal').modal({
							show: true,
							keyboard: false,
							backdrop: 'static'
						});
        }
    });
</script>
@yield('script')
</body>

</html>
