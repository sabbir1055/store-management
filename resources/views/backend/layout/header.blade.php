<!doctype html>
<html lang="en">
<?php
	$theme = Auth::user()->theme;
 ?>
<head>
	<title>Store Management</title>
	<!-- <meta charset="utf-8"> -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('assets/vendor/linearicons/style.css')}}">
	<link rel="stylesheet" href="{{asset('assets/vendor/chartist/css/chartist-custom.css')}}">
    <link rel="stylesheet" href="{{asset('bower_components/html5-editor/bootstrap-wysihtml5.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('datatable/datatable.css')}}">
    <!-- MAIN CSS -->
	<link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="{{asset('assets/css/demo.css')}}">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/img/apple-icon.png')}}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{asset('icon.png')}}">
	<style media="screen">
		
		::-webkit-scrollbar {
		  	width: 0px;
		}

		.navbar-default{
			background: {{ $theme->top_nav }};
		}
		.brand{
			background: {{ $theme->brand }} !important;
		}
		.sidebar{
			background: {{ $theme->left_side }} !important;
		}
		.dropdown{
			background: {{ $theme->top_nav_drop_background }} !important;
		}
		.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
			background: {{ $theme->top_nav_drop_background_hover}} !important;
		}
		.dropdown-menu{
			background: {{ $theme->top_nav_drop_background_items}} !important;
		}
		.sidebar .nav > li > a:focus, .sidebar .nav > li > a.active{
			background: {{ $theme->left_side_dropdown}} !important;
			border-left-color : {{ $theme->left_border_left_color }} !important;
		}
		.sidebar .nav .nav {
			background: {{ $theme->left_dropdown_item_back}} !important;
		}
		body{
			font-family: {{ $theme->font_family}} !important;
			font-size: {{ $theme->font_size}} !important;
		}
		.main-content{
			background: {{ $theme->main_background }};
		}
		/* .main{
			color: {{ $theme->body_font_color }};
		} */
		.panel{
			background: {{ $theme->panel_background }} ;
		}
		.panel-heading{
			background	: {{ $theme->title_background }} !important;
			color		: {{ $theme->title_font_color }} !important ;
		}
		.panel .panel-body{
			background	: {{ $theme->body_background }} !important;
			color		: {{ $theme->body_font_color }} !important;
		}
		.sidebar .nav > li > a{
			color		: {{ $theme->left_side_font_color }} !important;
			font-family	: {{ $theme->left_side_font_family }} !important;
			font-size	: {{ $theme->left_side_font_size }} !important;
		}
		.role{
			font-weight: bold;
		}
		.role ul li{
			list-style: none;
		}
		label{
			font-size: 20px;
		}
		.modal-title{
			color: black;
		}
		.modal-body{
			color: black;
		}
	</style>
	<script>
	    window.Laravel = <?php echo json_encode([
	        'base_url'  => \URL::to('/'),
	    ]); ?>
   </script>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="{{ route('dashboard') }}"><img src="{{asset('logo.png')}}" alt="Klorofil Logo" class="img-responsive" style="height:62px; width:80px;"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="@if(Auth::user()->data) {{asset(Auth::user()->data->image ?? 'noimage.png')}} @else {{asset('noimage.png')}} @endif" class="img-circle" alt="Avatar" style="width: 72px;margin-right:23px;"> <span>{{ Auth::user()->data ? Auth::user()->data->name : Auth::user()->email}}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('profile.index') }}"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="{{ route('profile.edit') }}"><i class="lnr lnr-pencil"></i> <span>Edit Profile</span></a></li>
								<li><a href="{{ route('setting') }}"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
								<li><a href="{{ route('logout') }}"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
