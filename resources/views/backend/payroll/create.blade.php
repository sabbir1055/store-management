@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3>Create Sallery sheet</h3>
    </div>
    <div class="panel-body">
        <!-- <form action="{{ route('payroll.store') }}" method="post" enctype="multipart/form-data"> -->
        <div class="col-md-12">
            <label for="employee_id">Employee Name : </label>
            <select class="form-control" id="employee_id" onchange="getemployee()" name="employee_id">
                <option value="">Select an employee</option>
                @if($employee)
                @foreach($employee as $key=>$value)
                <option value="{{ $value->id }}">{{ $value->data->name ?? $value->email }}</option>
                @endforeach
                @endif
            </select>
        </div>
        <!-- </form> -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Basic Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label > Employee Name : </label>
                    <input type="text" id="e_name" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Email : </label>
                    <input type="text" id="e_email" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Designation : </label>
                    <input type="text" id="e_designation" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Role : </label>
                    <input type="text" id="e_role" class="form-control" name="" value="" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Sallery Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label for="basic"> Basic : </label>
                    <input type="number" id="basic" class="form-control" name="basic" value="0" disabled>
                </div>
                <div class="col-md-6">
                    <label for="medical"> Medical Allowance : </label>
                    <input type="number" id="medical" class="form-control" name="medical" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="home"> Home Allowance : </label>
                    <input type="number" id="home" class="form-control" name="home" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="commission"> Comission : </label>
                    <input type="number" id="comission" class="form-control" name="commission" value="0" disabled>
                </div>
                <!-- <div class="col-md-6">
                    <label for="over"> Over Time : </label>
                    <input type="number" id="over" class="form-control" name="over" value="0"required>
                </div> -->
                <div class="col-md-6">
                    <label for="adjust"> Adjust Total : </label>
                    <input type="number" id="adjust" class="form-control" name="adjust" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="others"> Others : </label>
                    <input type="number" id="others" class="form-control" name="others" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="total"> Total : </label>
                    <input type="number" id="total" class="form-control" name="total" value="0" readonly>
                </div>
                <div class="col-md-6">
                    <label for="total_pay"> Total Pay : </label>
                    <input type="number" id="total_pay" class="form-control" name="total_pay" value="0" readonly>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="button" class="btn btn-success" onclick="storeSheet()" value="CREATE">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
function getemployee(){
    var id = $("#employee_id").val();
    if(id){
        $.ajax({
            url: '{{ route('payroll.employee.data') }}',
            method: 'post',
            'data': {
                '_token'    : '{{ csrf_token() }}',
                'employee_id' : id
            },
            success: function (d) {
                var data = JSON.parse(d);
                $('#e_name').val(data[0].name);
                $('#e_designation').val(data[0].designation);
                $('#e_email').val(data[0].email);
                $('#e_role').val(data[0].role);
                $('#basic').val(data[0].basic);
                $('#medical').val(data[0].medical);
                $('#home').val(data[0].home);
                $('#commission').val(data[0].commission);

                count();
            }
        });
    }
}
function storeSheet(){
    var id = $("#employee_id").val();
    var basic = $('#basic').val();
    if(id && basic ){
        $.ajax({
            url: '{{ route('payroll.store') }}',
            method: 'post',
            'data': {
                '_token'    : '{{ csrf_token() }}',
                'user_id' : id,
                'basic_sallery': basic,
                'medical': $('#medical').val(),
                'home': $('#home').val(),
                'other': $('#others').val(),
                'comission': $('#comission').val(),
                'deduction': $('#adjust').val(),
                'total': $('#total').val(),
                'total_pay': $('#total_pay').val(),
            },
            success: function (d) {
                window.location.href = window.Laravel.base_url+"/payroll";
            }
        });
    }
}
$(document).on('keyup','#basic',function(){
    count();
});

$(document).on('keyup','#home',function(){
    count();
});

$(document).on('keyup','#meical',function(){
    count();
});

$(document).on('keyup','#commission',function(){
    count();
});

$(document).on('keyup','#adjust',function(){
    count();
});

$(document).on('keyup','#others',function(){
    count();
});

$(document).on('keyup','#over',function(){
    count();
});

function count(){
    var basic = $('#basic').val();
    var medical = $('#medical').val();
    var home = $('#home').val();
    var commission = $('#comission').val();
    var adjust = $('#adjust').val();
    var others = $('#others').val();

    var total = parseInt(basic)+parseInt(medical)+parseInt(home)+parseInt(commission)+parseInt(others);
    var total_pay = total-parseInt(adjust);
    $('#total').val(total);
    $('#total_pay').val(total_pay);
}
</script>
@endsection
