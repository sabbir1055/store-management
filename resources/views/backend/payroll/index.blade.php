@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Vendors </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('payroll.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Generate Sallery sheet</button></a>
        </div>

        <table id="VendorDataTable" class="table table-hover" style="text-align:center; width:100%;">
            <!-- <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="52%"> -->
            <!--Table head-->
            <thead class="table--head">
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Total</td>
                    <td>Adjust</td>
                    <td>Total Paid</td>
                    <td>Issue Date</td>
                    <td>Issue By</td>
                    <td>Action</td>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->

            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Total</td>
                    <td>Adjust</td>
                    <td>Total Paid</td>
                    <td>Issue Date</td>
                    <td>Issue By</td>
                    <td>Action</td>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#VendorDataTable').DataTable({
        'processing' 	: true,
        'serverSide' 	: true,
        'ajax'			:{
            'url'       : '{{ route('payroll.data') }}',
            'dataType' 	: 'json',
            'type' 		: 'POST',
            'data'		: {
                '_token'    : '{{ csrf_token() }}',
            }
        },
        'columns' 		: [
            { 'data' : '#','searchable':false,'orderable':false},
            { 'data' : 'name'},
            { 'data' : 'total'},
            { 'data' : 'deduction'},
            { 'data' : 'total_pay'},
            { 'data' : 'created_at'},
            { 'data' : 'created_by'},
            { 'data' : 'action','searchable':false,'orderable':false}
        ]
    });
});
function edit(e){
    window.location.href = window.Laravel.base_url+"/payroll/edit/"+e;
}
function view(e){
    window.location.href = window.Laravel.base_url+"/payroll/view/"+e;
}
</script>
@endsection
