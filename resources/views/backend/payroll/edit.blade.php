@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3>Create Sallery sheet</h3>
    </div>
    <div class="panel-body">
        <!-- <form action="{{ route('payroll.store') }}" method="post" enctype="multipart/form-data"> -->
        
        <!-- </form> -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Basic Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label > Employee Name : </label>
                    <input type="text" id="e_name" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Email : </label>
                    <input type="text" id="e_email" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Designation : </label>
                    <input type="text" id="e_designation" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Role : </label>
                    <input type="text" id="e_role" class="form-control" name="" value="" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Sallery Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label for="basic"> Basic : </label>
                    <input type="number" id="basic" class="form-control" name="basic" value="0" disabled>
                </div>
                <div class="col-md-6">
                    <label for="medical"> Medical Allowance : </label>
                    <input type="number" id="medical" class="form-control" name="medical" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="home"> Home Allowance : </label>
                    <input type="number" id="home" class="form-control" name="home" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="commission"> Commission : </label>
                    <input type="number" id="commission" class="form-control" name="commission" value="0" disabled>
                </div>
                <div class="col-md-6">
                    <label for="over"> Over Time : </label>
                    <input type="number" id="over" class="form-control" name="over" value="0"required>
                </div>
                <div class="col-md-6">
                    <label for="adjust"> Adjust Total : </label>
                    <input type="number" id="adjust" class="form-control" name="adjust" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="others"> Others : </label>
                    <input type="number" id="others" class="form-control" name="others" value="0" required>
                </div>
                <div class="col-md-6">
                    <label for="total"> Total : </label>
                    <input type="number" id="total" class="form-control" name="total" value="0" readonly>
                </div>
                <div class="col-md-6">
                    <label for="total_pay"> Total Pay : </label>
                    <input type="number" id="total_pay" class="form-control" name="total_pay" value="0" readonly>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="button" class="btn btn-success" onclick="storeSheet()" value="CREATE">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
