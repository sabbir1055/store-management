@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Designations </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('designation.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Designation</button></a>
        </div>

        <table class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <!-- <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="100"> -->
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th> Designation Title </th>
                    <th> Sallery </th>
                    <th> Increment </th>
                    <th> Medical </th>
                    <th> Home rent </th>
                    <th> Other </th>
                    <th> Leave </th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
                @if($designation)
                    @foreach($designation as $i => $value)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $value->title }}</td>
                            <td>{{ $value->basic_sallery }} Tk</td>
                            <td>{{ $value->increment }} %</td>
                            <td>{{ $value->medical }} Tk</td>
                            <td>{{ $value->home }} Tk</td>
                            <td>{{ $value->other }} Tk</td>
                            <td>{{ $value->leave }} Tk</td>
                            <td>
                                <a href="{{ route('designation.edit',$value->id) }}"> <button type="button" class="btn btn-info"> <i class="fa fa-edit"></i></button> </a>
                                <a href="{{ route('designation.delete',$value->id) }}"> <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button> </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th> Designation Title </th>
                    <th> Sallery </th>
                    <th> Increment </th>
                    <th> Medical </th>
                    <th> Home rent </th>
                    <th> Other </th>
                    <th> Leave </th>
                    <th class="text-center">Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
