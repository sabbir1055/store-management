@extends('backend.layout.layout')
@section('css')
@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Designation {{ $designation->title }}</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('designation.update',$designation->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="name"> Designation Name : </label>
                <input type="text" class="form-control" name="title" id="name" value="{{ $designation->title }}" required>
            </div>
            <div class="col-md-6">
                <label for="basic"> Basic Sallery : </label>
                <input type="number" class="form-control" name="basic" id="basic" value="{{ $designation->basic_sallery }}" required>
            </div>
            <div class="col-md-6">
                <label for="increment"> Increment : (in percent) </label>
                <input type="number" class="form-control"  name="increment" id="increment" value="{{ $designation->increment }}" required>
            </div>
            <div class="col-md-6">
                <label for="medical"> Medical : </label>
                <input type="number" class="form-control" name="medical" id="medical" value="{{ $designation->medical }}" required>
            </div>
            <div class="col-md-6">
                <label for="home"> Home rent : </label>
                <input type="number" class="form-control" name="home" id="home" value="{{ $designation->home }}" required>
            </div>
            <div class="col-md-6">
                <label for="other"> Others : </label>
                <input type="number" class="form-control" name="other" id="other" value="{{ $designation->other }}" required>
            </div>
            <div class="col-md-6">
                <label for="leave"> Leaves : (Days) </label>
                <input type="number" class="form-control" name="leave" id="leave" value="{{ $designation->leave }}" required>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">UPDATE DESIGNATION</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
