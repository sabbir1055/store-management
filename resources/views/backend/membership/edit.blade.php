@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Membership {{ $membership->title }}</h3>
    </div>
    <div class="panel-body">
        <form  action="{{ route('membership.update',$membership->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="name"> Membership Title : </label>
                <input type="text" name="title" id="name" class="form-control" value="{{ $membership->title }}" required>
            </div>
            <div class="col-md-6">
                <label for="discount"> Membership Discount : (in percent)</label>
                <input type="number" name="discount" class="form-control" id="discount" value="{{ $membership->discount }}" required>
            </div>

            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">UPDATE MEMBERSHIP</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
