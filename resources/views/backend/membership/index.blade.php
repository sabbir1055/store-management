@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Memberships </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('membership.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Membership</button></a>
        </div>

        <table class="table table-hover table-responsive" >
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th> Membership Title </th>
                    <th> Discount </th>
                    <th>Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
                @if($membership)
                    @foreach($membership as $i => $value)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $value->title }}</td>
                            <td>{{ $value->discount }} %</td>
                            <td>
                                <a href="{{ route('membership.edit',$value->id) }}"> <button type="button" class="btn btn-info"> <i class="fa fa-edit"></i> </button> </a>
                                <a href="{{ route('membership.delete',$value->id) }}"> <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button> </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th> Membership Title </th>
                    <th> Discount </th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
