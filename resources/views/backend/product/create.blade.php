@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Add Product</h3>
    </div>
    <div class="panel-body">
            <div class="alert alert-success" role="alert">
                <p> Select an Import ID that you want to add in your sell list </p>
            </div>
        <form class="form-horizontal row-fluid" action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                    <label for="I_id">Import ID : </label>
                    <select id="Import_id" class="form-control" name="I_id" onchange="getimportdata()" class="span12" required>
                        <option value="">Select an ID</option>
                        @if($import)
                            @foreach($import as $key=>$value)
                                <option value="{{ $value->id }}"> ID: {{ $value->id }} & Vendor : {{ $value->vendor->name }} </option>
                            @endforeach
                        @endif
                    </select>
                <div id="import_product_data" style="max-height:200px;overflow-y: scroll;display:none;">
                    <table class="table table-border table-dark">
                        <thead>
                            <tr>
                                <th> # </th>
                                <th> Product Title </th>
                                <th> Product Code </th>
                                <th> Unit Price </th>
                                <th> Product Quantity </th>
                            </tr>
                            <tbody id='import_product_list'>

                            </tbody>
                        </thead>
                    </table>
                </div>
                <div class="col-md-12" style="margin-top:5%;">
                    <div class="col-md-6">
                        <label for="import_product">Select Imported Product : </label>
                            <select class="form-control" id="import_product" onchange="getIdentification()" required>
                                <option value="">Select a product </option>
                            </select>
                    </div>
                    <div class="col-md-6">
                        <label for="import_identification">Imported Identification : </label>
                        <input type="text" class="form-control" name="import_identification" id="import_identification" value="" readonly>
                    </div>
                    <div class="col-md-6">
                        <label for="name">Product Title : </label>
                        <input type="text" id="name" name="title" placeholder="Type name here..." class="form-control" required>
                    </div>
                    <div class="col-md-6">
                        <label for="code">Product Code : </label>
                        <input type="text" id="code" name="code" placeholder="Type code here..." class="form-control" required>
                    </div>
                    <div class="col-md-6">
                            <label for="category">Product Category : </label>
                                <select class="form-control" id="category" name="category" onchange="getsubcategory()" required>
                                    <option value="">Select Category</option>
                                    @if($category)
                                    @foreach($category as $key=>$value)
                                    <option value="{{ $value->id }}">{{ $value->title }}</option>
                                    @endforeach
                                    @endif
                                </select>
                    </div>
                    <div class="col-md-6">
                        <div id="sub_category_data" style="display:none;"></div>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label" for="p_price">Product Purchase Price : </label>
                        <input type="text" id="p_price" name="p_price" class="form-control" readonly>
                    </div>
                    <div class="col-md-6">
                        <label class="control-label" for="price">Product Price : </label>
                        <input type="text" id="price" name="price" placeholder="Type price here..." class="form-control" required>
                    </div>
                    <div class="col-md-6">
                            <label for="stock">Product Stock : </label>
                            <input type="number" id="stock" name="stock" placeholder="Type stock here..." class="form-control" required>
                    </div>
                    <div class="col-md-6">
                        <label for="image">Product Image : </label>
                        <input class="form-control" type="file" id="image" name="image" required>
                    </div>
                    <div class="col-md-12">
                        <label for="about">Product About : </label>
                        <textarea name="about" class="form-control" rows="8" id="textarea"></textarea>
                    </div>
                    <div class="col-md-12 text-center" style="margin-top:5%;">
                            <label for="">&nbsp </label>
                            <button type="submit" class="btn btn-success" > ADD PRODUCT </button>
                    </div>
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    var imported_data = [];
    function getimportdata(){
        var import_id = $('#Import_id').val();
        $.ajax({
            url   : '{{ route('product.import') }}',
            method: 'post',
            'data': {
                '_token'    : '{{ csrf_token() }}',
                'import_id' : import_id
            },
            success: function (data) {
                var data = JSON.parse(data);
                imported_data = data;
                var dom  = '';
                var dom1 = '<option value="">Select a product</option>';
                if(data){
                    $.each(JSON.parse(data.products),function(i,value){
                        dom+=`<tr>
                                <td> `+(++i)+` </td>
                                <td>`+value.title+`</td>
                                <td>`+value.code+`</td>
                                <td>`+value.price+`</td>
                                <td>`+value.qty+`</td>
                              </tr>`;
                        dom1+=`<option value="`+value.code+`">`+value.title+`</option>`;
                    });
                    $('#import_product_list').html(dom);
                    $('#import_product').html(dom1);
                }else{
                    $('#import_product_data').html(' <p>This import ID has no products </p>');
                }

                $('#import_product_data').attr('style','display:block');
            }
        });
    }

    function getsubcategory(){
        var cat_id = $('select[name="category"]').val();
        $.ajax({
            url   : '{{ route('product.category') }}',
            method: 'post',
            'data': {
                '_token'    : '{{ csrf_token() }}',
                'category_id' : cat_id
            },
            success: function (data) {
                var dom = '';
                var data = JSON.parse(data);
                dom =`<label for="sub_category">Product Subcategory : </label>
                            <select id="sub_category" class="form-control" name="sub_category" required>
                                <option value="">Select SubCategory</option>`;
                $.each(data,function(i,value){
                    dom+=`<option value="`+value.id+`">`+value.title+`</option>`;
                });
                dom+=`</select>`;

                    $('#sub_category_data').html(dom);
                $('#sub_category_data').attr('style','display:block');


            }
        });
    }

    function getIdentification(){
        var code = $('#import_product').val();
        $.each(JSON.parse(imported_data.products),function(i,value){
            if(value.code == code){
                $('#import_identification').val(value.import_identification);
                $('#p_price').val(value.price);
            }
        });
    }

</script>
@endsection
