@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Edit Product</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('product.update',$product->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="I_id">Import ID : </label>
                <input type="text" class="form-control" name="import_id" value="{{ $product->import_id }}" disabled>
            </div>
            <div class="col-md-6">
                <label for="I_id">Vendor Name : </label>
                <input type="text" class="form-control" name="import_id" value="{{ $product->vendor->name }}" disabled>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="name">Product Title : </label>
                <input type="text" class="form-control" id="name" name="title" value="{{ $product->title }}" required>
            </div>
            <div class="col-md-6">
                <label for="code">Product Code : </label>
                <input type="text" class="form-control" id="code" name="code" value="{{ $product->product_code }}" required>
            </div>
            <div class="col-md-6">
                <label for="category">Product Category : </label>
                <select class="form-control" id="category" name="category" onchange="getsubcategory()" required>
                    <option value="">Select Category</option>
                    @if($category)
                    @foreach($category as $key=>$value)
                    <option value="{{ $value->id }}" @if($value->id == $product->category_id) selected @endif>{{ $value->title }}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="col-md-6" id="sub_category_data">
                <label for="subcategory">Product SubCategory : </label>
                <select name="sub_category" class="form-control">
                    @if($product->category->subcategory)
                    @foreach($product->category->subcategory as $key=>$value)
                    <option value="{{ $value->id }}" @if($value->id == $product->sub_category_id) selected @endif>{{ $value->title }}</option>
                    @endforeach
                    @endif
                </select>
            </div>
            <div class="col-md-6">
                <label for="p_price">Product Purchase Price : </label>
                <input type="text" class="form-control" id="p_price" name="p_price" value="{{ $product->price - $product->profit_unit}}" required>
            </div>
            <div class="col-md-6">
                <label for="price">Product Price : </label>
                <input type="text" class="form-control" id="price" name="price" value="{{ $product->price }}" required>
            </div>
            <div class="col-md-6">
                <label for="stock">Product Stock : </label>
                <input type="number" class="form-control" id="stock" name="stock" value="{{ $product->stock }}" disabled>
            </div>
            <div class="col-md-6">
                <label for="about">Product About : </label>
                <textarea name="about" class="form-control" rows="8" cols="80" id="textarea">{!! $product->about  !!}</textarea>
            </div>
            <div class="col-md-6">
                <label for="image">Product Image : </label>
                <input type="file" id="image" name="image"  class="form-control">
            </div>
            <div class="col-md-6" style="margin-top:2%;">
                <label >&nbsp</label>
                <img src="{{ asset($product->image) }}" alt="" style="width:200px;height:200px;float:left;">
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for="">&nbsp </label>
                <button type="submit" class="btn btn-success" > UPDATE PRODUCT </button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
function getsubcategory(){
    var cat_id = $('select[name="category"]').val();
    console.log(cat_id);
    if(cat_id){
        $.ajax({
            url   : '{{ route('product.category') }}',
            method: 'post',
            'data': {
                '_token'    : '{{ csrf_token() }}',
                'category_id' : cat_id
            },
            success: function (data) {
                var dom = '';
                var data = JSON.parse(data);
                dom =`<label for="sub_category">Product Subcategory : </label>
                <select class="form-control" id="sub_category" name="sub_category" required>
                <option value="">Select SubCategory</option>`;
                $.each(data,function(i,value){
                    dom+=`<option value="`+value.id+`">`+value.title+`</option>`;
                });
                dom+=`</select>
                </div>`;

                $('#sub_category_data').html(dom);
                $('#sub_category_data').attr('style','display:block');


            }
        });
    }else{
        $('#sub_category_data').html('');
    }
}
</script>
@endsection
