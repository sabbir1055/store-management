@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> {{ $product->title }}</h3>
    </div>
    <div class="panel-body">
            <div class="col-md-6">
                <div class="col-md-12">
                    <img src="{{ asset($product->image ?? 'noimage.png') }}" class="img img-responsive" style="border-radius:10px;border:1px solid black">
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT DETAIL : <span class="form-control" style="border:none;"> {!! $product->about !!} </span></h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12" style="margin-top:2%">
                    <h3> TITLE : <span class="form-control"> {{ $product->title }} </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT CODE : <span class="form-control"> {{ $product->product_code }} </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT PRICE : <span class="form-control"> {{ $product->price }} </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>VENDOR NAME : <span class="form-control" > {{ $product->vendor->name }} </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT CATEGORY : <span class="form-control"> {{ $product->category->title }} </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT SUBCATEGORY : <span class="form-control" > {{ $product->subcategory->title ?? "" }} </span></h3>
                </div>
            </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
