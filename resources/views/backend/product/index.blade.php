@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Products </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('product.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Product</button></a>
        </div>

        <table id="VendorDataTable" class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <col width="20">
            <col width="20">
            <col width="20">
            <col width="20">
            <col width="20">
            <col width="20">
            <col width="20">
            <col width="50">
            <!--Table head-->
            <thead class="table--head">
                <tr>
                    <td>#</td>
                    <td>Title</td>
                    <td>Produce Code</td>
                    <td>Stock</td>
                    <td>Category</td>
                    <td>Subcategory</td>
                    <td>Price</td>
                    <td>Import ID</td>
                    <td>Action</td>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->

            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <td>#</td>
                    <td>Title</td>
                    <td>Produce Code</td>
                    <td>Stock</td>
                    <td>Category</td>
                    <td>Subcategory</td>
                    <td>Price</td>
                    <td>Import ID</td>
                    <td>Action</td>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#VendorDataTable').DataTable({
        'processing' 	: true,
        'serverSide' 	: true,
        'ajax'			:{
            'url'       : '{{ route('product.data') }}',
            'dataType' 	: 'json',
            'type' 		: 'POST',
            'data'		: {
                '_token'    : '{{ csrf_token() }}',
            }
        },
        'columns' 		: [
            { 'data' : '#','searchable':false,'orderable':false},
            { 'data' : 'title'},
            { 'data' : 'code','orderable':false},
            { 'data' : 'stock'},
            { 'data' : 'category','orderable':false},
            { 'data' : 'subcategory','orderable':false},
            { 'data' : 'price'},
            { 'data' : 'import_id','orderable':false},
            { 'data' : 'action','searchable':false,'orderable':false}
        ]
    });
});
function status(e){
    $.ajax({
        url: '{{ route('product.status') }}',
        method: 'post',
        'data': {
            '_token'    : '{{ csrf_token() }}',
            'product_id' : e
        },
        success: function (data) {
            location.reload();
        }
    });
}
function edit(e){
    window.location.href = window.Laravel.base_url+"/product/edit/"+e;
}
function view(e){
    window.location.href = window.Laravel.base_url+"/product/view/"+e;
}
</script>
@endsection
