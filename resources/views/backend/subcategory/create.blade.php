@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Add Subcategory </h3>
    </div>
    <div class="panel-body">
        <form action="{{ route('subcategory.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col-md-6">
                    <label for="name"> Subcategory Name : </label>
                    <input type="text" class="form-control" name="title" >
                </div>
                <div class="col-md-6">
                    <label for="c_name"> Category Name : </label>
                        <select class="form-control" name="category_id" required>
                            <option value="">Select category</option>
                            @if($category)
                                @foreach( $category as $key => $value)
                                    <option value="{{ $value->id }}">{{ $value->title }}</option>
                                @endforeach
                            @endif
                        </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="image"> Category Icon : </label>
                    <div class="controls">
                        <input type="file" name="image" required>
                    </div>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for=""> &nbsp </label>
                    <button type="submit" class="btn btn-success">ADD SUBCATEGORY</button>
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">

    </script>
@endsection
