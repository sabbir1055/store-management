@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Subcategory </h3>
    </div>
    <div class="panel-body">
        <form action="{{ route('subcategory.update',$subcategory->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col-md-6">
                    <label for="name"> Subcategory Name : </label>
                    <input type="text" class="form-control" name="title" value="{{ $subcategory->title }}">
                </div>
                <div class="col-md-6">
                    <label for="c_name"> Category Name : </label>
                    <select class="form-control" name="category_id" required>
                        @if($category)
                            @foreach( $category as $key => $value)
                                <option value="{{ $value->id }}" @if($value->id == $subcategory->category_id) selected @endif>{{ $value->title }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="image"> Category Icon : </label>
                    <input type="file" class="form-control" name="image">
                </div>
                <div class="col-md-12">
                    @if($subcategory->image)
                        <img src="{{asset($subcategory->image)}}" style="width:200px;height:200px; margin-left:65%;">
                    @endif
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for=""> &nbsp </label>
                    <button type="submit" class="btn btn-success">UPDATE SUBCATEGORY</button>
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">

    </script>
@endsection
