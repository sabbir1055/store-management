@extends('backend.layout.layout')
@section('css')
    <style media="screen">
        .table_row_add_button{
            background-color: #99ff33; /* Green */
              border: none;
              color: white;
              text-align: center;
              text-decoration: none;
              display: inline-block;
        }
        .table_row_remove_button{
            background-color: #ff6600; /* Green */
              border: none;
              color: white;
              text-align: center;
              text-decoration: none;
              display: inline-block;
        }
    </style>
@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3>Add Vendor</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('vendor.product.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <label for="name">Select Vendor : </label>
                    <select  name="vendor_id" class="form-control vendor_name" onchange="getData()" required>
                        <option value="">Select a vendor</option>
                        @if($vendor)
                            @foreach($vendor as $key=>$value)
                                <option value="{{ $value->id }}"> {{ $value->name }} </option>
                            @endforeach
                        @endif
                    </select>
            <div class="col-md-12" id="product_list_table" style="margin-top:30px;display:none;" >
                <div class="controls">
                    <table class="table table-dark table-responsive">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>TITLE</th>
                            <th>CODE</th>
                            <th>UNIT PRICE </th>
                            <th>&nbsp &nbsp &nbsp <button type="button" title="add row" class="button button5 table_row_add_button" style="border-radius:50%;"> <i class="fa fa-plus"></i> </button></th>
                          </tr>
                        </thead>
                        <tbody id="product_list">

                        </tbody>
                    </table>
                </div>
            </div>
            <div id="form_submit_button" class="text-center" style="display:none;">
                <label for="">&nbsp </label>
                <button type="submit" class="btn btn-success" > UPDATE VENDOR PRODUCTS</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        $(document).on('click','.table_row_add_button',function(){
            var dom = `<tr>
                        <td><h5>#</h5></td>
                        <td>
                            <input type="text" name="product_title[]" class="form-control" required>
                        </td>
                        <td>
                            <input type="text" name="product_code[]" class="form-control" required>
                        </td>
                        <td>
                            <input type="number" name="product_price[]" class="form-control" size="20%;" required>
                        </td>
                        <td>
                            <button type="button" class="button button5 table_row_add_button" style="border-radius:50%;"> <i class="fa fa-plus"></i> </button> &nbsp <button type="button" class="button button5 table_row_remove_button" style="border-radius:50%;"> <i class="fa fa-minus"></i> </button>
                        </td>
                    </tr>`;
                $('#product_list').append(dom);
        });

        $(document).on('click','.table_row_remove_button',function(){
            $(this).parent().parent().remove();
        });

        function getData(){
            var vendor_id = $('.vendor_name').val();
            if(vendor_id){
                $.ajax({
                    url: '{{ route('vendor.product.data') }}',
                    method: 'post',
                    'data': {
                        '_token'    : '{{ csrf_token() }}',
                        'vendor_id' : vendor_id
                    },
                    success: function (data) {
                        var data = JSON.parse(data);
                        $('#product_list_table').attr('style','display:block');
                        $('#form_submit_button').attr('style','display:block');
                        if(data.length > 0){
                            var dom = "";
                            $.each( data, function( key, value ) {
                                dom+=`<tr>
                                        <td><h5>#</h5></td>
                                        <td>
                                            <input type="text" name="product_title[]" value="`+value.product_title+`" class="form-control" required>
                                        </td>
                                        <td>
                                            <input type="text" name="product_code[]" value="`+value.product_code+`" class="form-control" required>
                                        </td>
                                        <td>
                                            <input type="number" name="product_price[]" class="form-control" value="`+value.product_price+`" size="20%;" required>
                                        </td>
                                        <td>
                                            <button type="button" class="button button5 table_row_add_button" style="border-radius:50%;"> <i class="fa fa-plus"></i> </button> &nbsp <button type="button" class="button button5 table_row_remove_button" style="border-radius:50%;"> <i class="fa fa-minus"></i> </button>
                                        </td>
                                    </tr>`;
                            });
                            $('#product_list').html(dom);
                        }
                        else{
                            $('#product_list').html(`<tr>
                                <td><h3>#</h3></td>
                                <td>
                                    <input type="text" name="product_title[]" class="form-control" required>
                                </td>
                                <td>
                                    <input type="text" name="product_code[]" class="form-control" required>
                                </td>
                                <td>
                                    <input type="number" name="product_price[]" class="form-control" required> &nbsp <button type="button" class="button button5 table_row_add_button" style="border-radius:50%;"> <i class="icon-plus"></i> </button>
                                </td>
                                <td>
                                    <button type="button" class="button button5 table_row_add_button" style="border-radius:50%;"> <i class="icon-plus"></i> </button> &nbsp <button type="button" class="button button5 table_row_remove_button" style="border-radius:50%;"> <i class="icon-minus"></i> </button>
                                </td>
                            </tr>`);
                        }
                    }
                });
            }
        }
    </script>
@endsection
