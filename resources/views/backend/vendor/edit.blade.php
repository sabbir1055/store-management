@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Vendor</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('vendor.update',$vendor->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <div class="col-md-12">
                    @if($vendor->image)
                        <img src="{{ asset($vendor->image??'noimage.png') }}" class="img img-responsive" style="border-radius:10px;"alt="">
                    @endif
                </div>
                <div class="col-md-12">
                    <label for="image"> Choose new image : </label>
                    <input type="file" name="image" class="form-control" value="">
                </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <label class="control-label" for="name">Vendor Name : </label>
                <input type="text" id="name" class="form-control" name="name" placeholder="Type name here..." value="{{ $vendor->name }}" required>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="email">Vendor Email : </label>
                <input type="email" id="email" name="email" class="form-control" value="{{ $vendor->email }}" placeholder="Type email here..." disabled>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="image">Vendor Image : </label>
                <input type="file" id="image" class="form-control" name="image">
            </div>
            <div class="col-md-12">
                <label class="control-label" for="msisdn">Vendor Mobile : </label>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="country_code" value="+880" disabled>
                    </div>
                    <div class="col-md-8">
                        <input type="number" id="msisdn" class="form-control" name="msisdn" value="{{ substr($vendor->msisdn,-10) }}" required>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="address">Vendor Address : </label>
                <input type="text" id="address" class="form-control" value="{{ $vendor->address }}" name="address">
            </div>
            <div class="col-md-12">
                <label class="control-label" for="website">Vendor Website : </label>
                <input type="text" id="website" class="form-control" value="{{ $vendor->website }}" name="website" class="span8">
            </div>
        </div>
        <div class="col-md-12 text-center" style="margin-top:5%">
            <label for="">&nbsp </label>
            <button type="submit" class="btn btn-success" > UPDATE VENDOR </button>
        </div>
        </form>
    </div>
</div>
@endsection
