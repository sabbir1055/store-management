@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Vendors </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('vendor.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Vendor</button></a>
        </div>

        <table id="VendorDataTable" class="table table-hover" style="text-align:center; width:100%;">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="8%">
            <col width="52%">
            <!--Table head-->
            <thead class="table--head">
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Mobile</td>
                    <td>Address</td>
                    <td>status</td>
                    <td>Action</td>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->

            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <td>#</td>
                    <td>Name</td>
                    <td>Email</td>
                    <td>Mobile</td>
                    <td>Address</td>
                    <td>status</td>
                    <td>Action</td>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $('#VendorDataTable').DataTable({
        'processing' 	: true,
        'serverSide' 	: true,
        'ajax'			:{
            'url'       : '{{ route('vendor.data') }}',
            'dataType' 	: 'json',
            'type' 		: 'POST',
            'data'		: {
                '_token'    : '{{ csrf_token() }}',
            }
        },
        'columns' 		: [
            { 'data' : '#','searchable':false,'orderable':false},
            { 'data' : 'name'},
            { 'data' : 'email'},
            { 'data' : 'msisdn'},
            { 'data' : 'address','searchable':false,'orderable':false},
            { 'data' : 'status'},
            { 'data' : 'action','searchable':false,'orderable':false}
        ]
    });
});
function status(e){
    $.ajax({
        url: '{{ route('vendor.status') }}',
        method: 'post',
        'data': {
            '_token'    : '{{ csrf_token() }}',
            'vendor_id' : e
        },
        success: function (data) {
            location.reload();
        }
    });
}
function edit(e){
    window.location.href = window.Laravel.base_url+"/vendor/edit/"+e;
}
function view(e){
    window.location.href = window.Laravel.base_url+"/vendor/view/"+e;
}
</script>
@endsection
