@extends('backend.layout.layout')

@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Edit Theme</h3>
    </div>
    <div class="panel-body">
            <div class="alert alert-success" role="alert">
                <p> Edit Your web site color as you want </p>
            </div>
        <form class="form-horizontal row-fluid" action="{{ route('setting.update') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
                <div class="col-md-6">
                    <label for="top_nav"> Top Nav Color :  </label>
                    <input type="color" class="form-control" name="top_nav" id="top_nav" value="{{ $theme->top_nav }}">
                </div>
                <div class="col-md-6">
                    <label for="font_family"> Font Family :  </label>
                    <input type="text" name="font_family" class="form-control" id="font_family" value="{{ $theme->font_family }}">
                </div>
                <div class="col-md-6">
                    <label for="font_size"> Font Size :  </label>
                    <input type="text" name="font_size" class="form-control" id="font_size" value="{{ $theme->font_size }}">
                </div>
                <div class="col-md-6">
                    <label for="brand"> Top Nav Left Color :  </label>
                    <input type="color" name="brand" id="brand" class="form-control" value="{{ $theme->brand }}">
                </div>
                <div class="col-md-6">
                    <label for="left_side"> Left Sidebar Color :  </label>
                    <input type="color" name="left_side" class="form-control" id="left_side" value="{{ $theme->left_side }}">
                </div>
                <div class="col-md-6">
                    <label for="left_side_dropdown"> Left Sidebar Dropdown Color :  </label>
                    <input type="color" name="left_side_dropdown" class="form-control" id="left_side_dropdown" value="{{ $theme->left_side_dropdown }}">
                </div>
                <div class="col-md-6">
                    <label for="left_border_left_color"> Left Sidebar Border Color :  </label>
                    <input type="color" name="left_border_left_color" class="form-control" id="left_border_left_color" value="{{ $theme->left_border_left_color }}">
                </div>
                <div class="col-md-6">
                    <label for="left_dropdown_item_back"> Left Sidebar Dropdown Background :  </label>
                    <input type="color" name="left_dropdown_item_back" class="form-control" id="left_dropdown_item_back" value="{{ $theme->left_dropdown_item_back }}">
                </div>
                <div class="col-md-6">
                    <label for="top_nav_drop_background"> Top Nav Dropdown Color :  </label>
                    <input type="color" name="top_nav_drop_background" class="form-control" id="top_nav_drop_background" value="{{ $theme->top_nav_drop_background }}">
                </div>
                <div class="col-md-6">
                    <label for="top_nav_drop_background_hover"> Top Nav Dropdown Hover :  </label>
                    <input type="color" name="top_nav_drop_background_hover" class="form-control" id="top_nav_drop_background_hover" value="{{ $theme->top_nav_drop_background_hover }}">
                </div>
                <div class="col-md-6">
                    <label for="top_nav_drop_background_items"> Top Nav Dropdown Item Background :  </label>
                    <input type="color" id="top_nav_drop_background_items" class="form-control" name="top_nav_drop_background_items" value="{{ $theme->top_nav_drop_background_items }}">
                </div>
                <div class="col-md-6">
                    <label for="main_background"> Content Background :  </label>
                    <input type="color" id="main_background" class="form-control" name="main_background" value="{{ $theme->main_background }}">
                </div>
                <div class="col-md-6">
                    <label for="title_background"> Content Title Background :  </label>
                    <input type="color" id="title_background" class="form-control" name="title_background" value="{{ $theme->title_background }}">
                </div>
                <div class="col-md-6">
                    <label for="body_background"> Content Boody Background :  </label>
                    <input type="color" id="body_background" class="form-control" name="body_background" value="{{ $theme->body_background }}">
                </div>
                <div class="col-md-6">
                    <label for="panel_background"> Content Panel Background :  </label>
                    <input type="color" id="panel_background" class="form-control" name="panel_background" value="{{ $theme->panel_background }}">
                </div>
                <div class="col-md-6">
                    <label for="body_font_color"> Body Font Color :  </label>
                    <input type="color" id="body_font_color" class="form-control" name="body_font_color" value="{{ $theme->body_font_color }}">
                </div>
                <div class="col-md-6">
                    <label for="title_font_color"> Title Font Color :  </label>
                    <input type="color" id="title_font_color" class="form-control" name="title_font_color" value="{{ $theme->title_font_color }}">
                </div>
                <div class="col-md-6">
                    <label for="left_side_font_color"> Left Bar Font Color :  </label>
                    <input type="color" id="left_side_font_color" class="form-control" name="left_side_font_color" value="{{ $theme->left_side_font_color }}">
                </div>
                <div class="col-md-6">
                    <label for="left_side_font_family">  Left Bar Font Family :  </label>
                    <input type="text" id="left_side_font_family" class="form-control" name="left_side_font_family" value="{{ $theme->left_side_font_family }}">
                </div>
                <div class="col-md-6">
                    <label for="left_side_font_size">  Left Bar Font Family :  </label>
                    <input type="text" id="left_side_font_size" class="form-control" name="left_side_font_size" value="{{ $theme->left_side_font_size }}">
                </div>
                <div class="col-md-12 text-center" style="margin-top:2%;">
                    <label for="">&nbsp</label>
                    <button type="submit" name="button" class="btn btn-success"> UPDATE </button>
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')

@endsection
