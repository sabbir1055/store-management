@extends('backend.layout.layout')
@section('css')
<style media="screen">

</style>
@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Edit Product Status </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('identifier.update',$identifier->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="name"> Product Name : </label>
                <input type="text" class="form-control" name="amount" id="name" value="{{ @$identifier->product[0]->title }}" readonly>
            </div>
            <div class="col-md-6">
                <label for="discount"> Product Code : </label>
                <input type="text" class="form-control" name="comission" id="discount" value="{{ $identifier->product_code }}" readonly>
            </div>
            <div class="col-md-6">
                <label for="discount"> Product Status : </label>
                <select name="type" class="form-control" required>
                    <option value="">select product status </option>
                    <option value="1" @if($identifier->type == 1) selected @endif> Sold </option>
                    <option value="2" @if($identifier->type == 2) selected @endif> Resaler </option>
                    <option value="3" @if($identifier->type == 3) selected @endif> Returned </option>
                    <option value="4" @if($identifier->type == 4) selected @endif> Damaged </option>
                    <option value="5" @if($identifier->type == 5) selected @endif> Recovered </option>
                </select>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <div class="controls">
                    <button type="submit" class="btn btn-success">UPDATE STATUS</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
