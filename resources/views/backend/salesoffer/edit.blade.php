@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> Update Offer </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="{{ route('salesoffer.update',$sales->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="name"> Product : </label>
                <select class="form-control" name="product_id" readonly>
                    @if($product)
                        @foreach($product as $key => $value)
                            <option value="{{ $value->id }}" @if($value->id == $sales->product_id) selected @endif>{{ $value->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-md-6">
                <label for="discount"> Discount : (in percent)</label>
                <input type="number" class="form-control" name="discount" id="discount" value="{{ $sales->discount }}" required>
            </div>
            <div class="col-md-6">
                <label for="from"> Discount From : </label>
                <input type="text" class="form-control" name="from" id="from" value="{{ substr($sales->from,0,10) }}" required>
            </div>
            <div class="col-md-6" id="date_to">
                <label for="to"> Discount To : </label>
                <input type="text" class="form-control" name="to" id="to" value="{{ substr($sales->to,0,10) }}" required>
            </div>
            <div class="col-md-6">
                <label for="type"> Discount Type : </label>
                <select class="form-control" name="type" required>
                    <option value="">Select a discount type</option>
                    <option value="1" @if($sales->sales_type == 1) selected @endif> Regular discount </option>
                    <option value="2" @if($sales->sales_type == 2) selected @endif>Flash discount</option>
                </select>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">UPDATE OFFER</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        var date = new Date();
        date.setDate(date.getDate());
        $('#from').datepicker({
            todayHighlight:'TRUE',
            startDate: date,
            autoclose: true,
        });
        var today = '';
        $(document).on('change','#from',function(){
            $('#date_to').children().remove();
            $('#date_to').html(`<label for="to"> Discount To : </label>
                                                <input type="text"  name="to" id="to" required>`);
            var date_to = $(this).val();
            var today = new Date(date_to);
            var tomorrow = new Date(today);
            tomorrow.setDate(today.getDate()+1);
            console.log(tomorrow);
            $('#to').datepicker({
                startDate: tomorrow.toLocaleDateString(),
                startDateHighlight:'TRUE',
                autoclose: true,
            });
        });
        $(document).on('click','#to',function(){
            if($('#from').val() == ''){
                alert('Plese select discount start from');
            }else{
                var date_to = $(this).val();
                var today = new Date(date_to);
                var today = new Date(date_to);
                var tomorrow = new Date(today);
                $('#to').datepicker({
                    startDate: tomorrow.toLocaleDateString(),
                    startDateHighlight:'TRUE',
                    autoclose: true,
                });
            }
        });
    });
</script>
@endsection
