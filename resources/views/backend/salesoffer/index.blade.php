@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3> All Sales Offers </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('salesoffer.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Offer</button></a>
        </div>

        <table class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th> Product </th>
                    <th> Import ID </th>
                    <th> Discount </th>
                    <th> From </th>
                    <th> To </th>
                    <th> Type </th>
                    <th>Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
                @if($sales)
                    @foreach($sales as $i => $value)
                        <tr>
                            <td>{{ ++$i }}</td>
                            <td>{{ $value->product->title }} </td>
                            <td>{{ $value->product->import_id }} </td>
                            <td>{{ $value->discount }} %</td>
                            <td>{{ substr($value->from,0,10) }} </td>
                            <td>{{ substr($value->to,0,10) }} </td>
                            <td>@if($value->sales_type == 1) Discount Sale @elseif($value->sales_type ==-2) Flash Sale @endif</td>
                            <td>
                                <a href="{{ route('salesoffer.edit',$value->id) }}"> <button type="button" class="btn btn-info"> <i class="fa fa-edit"></i> </button> </a>
                                <a href="{{ route('salesoffer.delete',$value->id) }}"> <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button> </a>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th> Product </th>
                    <th> Import ID </th>
                    <th> Discount </th>
                    <th> From </th>
                    <th> To </th>
                    <th> Type </th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">

</script>
@endsection
