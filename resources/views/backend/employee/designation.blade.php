@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3 class="panel-title">Add Employee Designation</h3>
        <p class="text-muted m-b-30"> Fill all required field.</p>
    </div>
    <div class="panel-body">
        <form name="add_employee" action="{{ route('employee.designation.store') }}" method="post">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="role">Employee Name : </label>
                <select class="form-control" name="user_id" required>
                    <option value="">Select an employee</option>
                    @if($employee)
                        @foreach($employee as $key=>$value)
                            <option value="{{ $value->id }}">{{ $value->data->name ?? $value->email }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-md-6">
                <label for="role">Employee Designation : </label>
                <select class="form-control" name="designation" required>
                    <option value="">Select an designation</option>
                    @if($designation)
                        @foreach($designation as $key=>$value)
                            <option value="{{ $value->id }}">{{ $value->title }}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="submit" class="btn btn-success" value="CREATE">
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')

@endsection
