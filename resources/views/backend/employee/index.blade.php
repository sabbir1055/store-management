@extends('backend.layout.layout')
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <div class="panel-heading">
            <h3 class="panel-title">Employees</h3>
        </div>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="{{ route('employee.create') }}"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Employee</button></a>
        </div>
        <div class="panel-body">
            <div class="row">
                <!-- table -->

                <!--Table-->
                <table id="VendorDataTable" class="table table-hover table-fixed" style="text-align:center;padding:20px;border-collapse:inherit;width:100%;">

                    <!--Table head-->
                    <thead class="table--head">
                        <tr>
                            <td>#</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Msisdn</td>
                            <td>Role</td>
                            <td>Designation</td>
                            <td>Status</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->

                    <!--Table body-->


                    <!-- Table footer -->
                    <tfoot>
                        <tr>
                            <td>#</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Msisdn</td>
                            <td>Role</td>
                            <td>Designation</td>
                            <td>Status</td>
                            <td>Action</td>
                        </tr>
                    </tfoot>
                    <!-- Table footer -->
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    var table = $('#VendorDataTable').DataTable({
        'processing' 	: true,
        'serverSide' 	: true,
        'ajax'			:{
            'url'       : '{{ route('employee.data') }}',
            'dataType' 	: 'json',
            'type' 		: 'POST',
            'data'		: {
                '_token'    : '{{ csrf_token() }}',
            }
        },
        'columns' 		: [
            { 'data' : '#'},
            { 'data' : 'name'},
            { 'data' : 'email'},
            { 'data' : 'msisdn'},
            { 'data' : 'role'},
            { 'data' : 'designation'},
            { 'data' : 'status'},
            { 'data' : 'action','searchable':false,'orderable':false}
        ]
    });
});
function status(e){
    $.ajax({
        url: '{{ route('employee.status') }}',
        method: 'post',
        'data'			: {
            '_token'      : '{{ csrf_token() }}',
            'employee_id' : e
        },
        success: function (data) {
            location.reload();
        }
    });
}
</script>
@endsection
