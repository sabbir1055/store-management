@extends('backend.layout.layout')
@section('css')

@endsection
@section('content')
<div class="panel panel-headline">
    <div class="panel-heading">
        @include('partials.status')
        <h3 class="panel-title">Create Employee</h3>
        <p class="text-muted m-b-30"> Fill all required field.</p>
    </div>
    <div class="panel-body">
        <form name="add_employee" action="{{ route('employee.store') }}" method="post">
            {{ csrf_field() }}
            <div class="col-md-6">
                <label for="email">Employee Email : </label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter employee email here ........" value="" required>
            </div>
            <div class="col-md-6">
                <label for="role">Employee Role : </label>
                <select class="form-control" name="role" required>
                    <option value="">Select an role</option>
                    @if($role)
                        @foreach($role as $key=>$value)
                            <option value="{{ $value->id }}">{{ $value->display_name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="submit" class="btn btn-success" value="CREATE">
            </div>
        </form>
    </div>
</div>
@endsection
@section('script')

@endsection
