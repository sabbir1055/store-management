<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Add Offer </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('salesoffer.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <label for="name"> Product : </label>
                <select class="form-control" name="product_id" required>
                    <option value=""> Select a product </option>
                    <?php if($product): ?>
                        <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($value->id); ?>"><?php echo e($value->title); ?> & Import ID : <?php echo e($value->import_id); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="col-md-6">
                <label for="discount"> Discount : (in percent)</label>
                <input type="number" class="form-control" name="discount" id="discount" required>
            </div>
            <div class="col-md-6">
                <label for="from"> Discount From : </label>
                <input type="text" class="form-control" name="from" id="from" required>
            </div>
            <div class="col-md-6" id="date_to">
                <label for="to"> Discount To : </label>
                <input type="text" class="form-control" name="to" id="to" required>
            </div>
            <div class="col-md-6">
                <label for="type"> Discount Type : </label>
                <select class="form-control" name="type" required>
                    <option value="">Select a discount type</option>
                    <option value="1"> Regular discount </option>
                    <option value="2">Flash discount</option>
                </select>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">ADD OFFER</button>
            </div>
        </div>
    </div>
</form>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        var date = new Date();
        date.setDate(date.getDate());
        $('#from').datepicker({
            todayHighlight:'TRUE',
            startDate: date,
            autoclose: true,
        });
        var today = '';
        $(document).on('change','#from',function(){
            $('#date_to').children().remove();
            $('#date_to').html(`<label for="to"> Discount To : </label>
                                                <input type="text"  name="to" id="to" required>`);
            var date_to = $(this).val();
            var today = new Date(date_to);
            var tomorrow = new Date(today);
            tomorrow.setDate(today.getDate()+1);
            console.log(tomorrow);
            $('#to').datepicker({
                startDate: tomorrow.toLocaleDateString(),
                startDateHighlight:'TRUE',
                autoclose: true,
            });
        });
        $(document).on('click','#to',function(){
            if($('#from').val() == ''){
                alert('Plese select discount start from');
            }else{
                var date_to = $(this).val();
                var today = new Date(date_to);
                var today = new Date(date_to);
                var tomorrow = new Date(today);
                $('#to').datepicker({
                    startDate: tomorrow.toLocaleDateString(),
                    startDateHighlight:'TRUE',
                    autoclose: true,
                });
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>