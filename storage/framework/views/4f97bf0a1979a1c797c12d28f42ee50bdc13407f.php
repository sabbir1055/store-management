<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Edit Product</h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('product.update',$product->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <label for="I_id">Import ID : </label>
                <input type="text" class="form-control" name="import_id" value="<?php echo e($product->import_id); ?>" disabled>
            </div>
            <div class="col-md-6">
                <label for="I_id">Vendor Name : </label>
                <input type="text" class="form-control" name="import_id" value="<?php echo e($product->vendor->name); ?>" disabled>
            </div>
            <div class="col-md-6">
                <label class="control-label" for="name">Product Title : </label>
                <input type="text" class="form-control" id="name" name="title" value="<?php echo e($product->title); ?>" required>
            </div>
            <div class="col-md-6">
                <label for="code">Product Code : </label>
                <input type="text" class="form-control" id="code" name="code" value="<?php echo e($product->product_code); ?>" required>
            </div>
            <div class="col-md-6">
                <label for="category">Product Category : </label>
                <select class="form-control" id="category" name="category" onchange="getsubcategory()" required>
                    <option value="">Select Category</option>
                    <?php if($category): ?>
                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($value->id); ?>" <?php if($value->id == $product->category_id): ?> selected <?php endif; ?>><?php echo e($value->title); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="col-md-6" id="sub_category_data">
                <label for="subcategory">Product SubCategory : </label>
                <select name="sub_category" class="form-control">
                    <?php if($product->category->subcategory): ?>
                    <?php $__currentLoopData = $product->category->subcategory; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($value->id); ?>" <?php if($value->id == $product->sub_category_id): ?> selected <?php endif; ?>><?php echo e($value->title); ?></option>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>
            <div class="col-md-6">
                <label for="p_price">Product Purchase Price : </label>
                <input type="text" class="form-control" id="p_price" name="p_price" value="<?php echo e($product->price - $product->profit_unit); ?>" required>
            </div>
            <div class="col-md-6">
                <label for="price">Product Price : </label>
                <input type="text" class="form-control" id="price" name="price" value="<?php echo e($product->price); ?>" required>
            </div>
            <div class="col-md-6">
                <label for="stock">Product Stock : </label>
                <input type="number" class="form-control" id="stock" name="stock" value="<?php echo e($product->stock); ?>" disabled>
            </div>
            <div class="col-md-6">
                <label for="about">Product About : </label>
                <textarea name="about" class="form-control" rows="8" cols="80" id="textarea"><?php echo $product->about; ?></textarea>
            </div>
            <div class="col-md-6">
                <label for="image">Product Image : </label>
                <input type="file" id="image" name="image"  class="form-control">
            </div>
            <div class="col-md-6" style="margin-top:2%;">
                <label >&nbsp</label>
                <img src="<?php echo e(asset($product->image)); ?>" alt="" style="width:200px;height:200px;float:left;">
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for="">&nbsp </label>
                <button type="submit" class="btn btn-success" > UPDATE PRODUCT </button>
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
function getsubcategory(){
    var cat_id = $('select[name="category"]').val();
    console.log(cat_id);
    if(cat_id){
        $.ajax({
            url   : '<?php echo e(route('product.category')); ?>',
            method: 'post',
            'data': {
                '_token'    : '<?php echo e(csrf_token()); ?>',
                'category_id' : cat_id
            },
            success: function (data) {
                var dom = '';
                var data = JSON.parse(data);
                dom =`<label for="sub_category">Product Subcategory : </label>
                <select class="form-control" id="sub_category" name="sub_category" required>
                <option value="">Select SubCategory</option>`;
                $.each(data,function(i,value){
                    dom+=`<option value="`+value.id+`">`+value.title+`</option>`;
                });
                dom+=`</select>
                </div>`;

                $('#sub_category_data').html(dom);
                $('#sub_category_data').attr('style','display:block');


            }
        });
    }else{
        $('#sub_category_data').html('');
    }
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>