<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
		<?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<h3 class="panel-title">All Permissions</h3>
	</div>
	<div class="panel-body">
		<div class="pull-right" style="padding:10px;">
				<a href="<?php echo e(route('permissions.create')); ?>"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Permission</button></a>
		</div>

		<table id="employee" class="table table-hover table-fixed" style="text-align:center;">

			<!--Table head-->
			<thead class="table--head">
				<tr>
					<td>#</td>
					<td>Name</td>
					<td>Display Name</td>
					<td>Action</td>
				</tr>
			</thead>
			<!--Table head-->

			<!--Table body-->
			<tbody>
				<?php if($permission): ?>
				<?php $__currentLoopData = $permission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td><?php echo e(++$key); ?></td>
					<td><?php echo e($p->name); ?></td>
					<td><?php echo e($p->display_name); ?></td>
					<td> <a onclick="return confirm('Are you sure to delete?')" href="<?php echo e(route('permissions.delete',$p->id)); ?>" class="btn btn-danger">Delete</a> </td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
			</tbody>
			<!--Table body-->
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>