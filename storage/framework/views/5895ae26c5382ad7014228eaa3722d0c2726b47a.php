<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Add Subcategory </h3>
    </div>
    <div class="panel-body">
        <form action="<?php echo e(route('subcategory.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

                <div class="col-md-6">
                    <label for="name"> Subcategory Name : </label>
                    <input type="text" class="form-control" name="title" >
                </div>
                <div class="col-md-6">
                    <label for="c_name"> Category Name : </label>
                        <select class="form-control" name="category_id" required>
                            <option value="">Select category</option>
                            <?php if($category): ?>
                                <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($value->id); ?>"><?php echo e($value->title); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="image"> Category Icon : </label>
                    <div class="controls">
                        <input type="file" name="image" required>
                    </div>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for=""> &nbsp </label>
                    <button type="submit" class="btn btn-success">ADD SUBCATEGORY</button>
                </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>