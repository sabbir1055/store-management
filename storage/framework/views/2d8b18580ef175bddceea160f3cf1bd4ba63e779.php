<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Update Subcategory </h3>
    </div>
    <div class="panel-body">
        <form action="<?php echo e(route('subcategory.update',$subcategory->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

                <div class="col-md-6">
                    <label for="name"> Subcategory Name : </label>
                    <input type="text" class="form-control" name="title" value="<?php echo e($subcategory->title); ?>">
                </div>
                <div class="col-md-6">
                    <label for="c_name"> Category Name : </label>
                    <select class="form-control" name="category_id" required>
                        <?php if($category): ?>
                            <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($value->id); ?>" <?php if($value->id == $subcategory->category_id): ?> selected <?php endif; ?>><?php echo e($value->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="image"> Category Icon : </label>
                    <input type="file" class="form-control" name="image">
                </div>
                <div class="col-md-12">
                    <?php if($subcategory->image): ?>
                        <img src="<?php echo e(asset($subcategory->image)); ?>" style="width:200px;height:200px; margin-left:65%;">
                    <?php endif; ?>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for=""> &nbsp </label>
                    <button type="submit" class="btn btn-success">UPDATE SUBCATEGORY</button>
                </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>