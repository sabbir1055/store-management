<?php $__env->startSection('css'); ?>
<style media="screen">
	.role{

	}
	.role ul li{
		list-style: none;
		font-weight: bold;
	}
	label{
		font-size: 20px;
	}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
		<?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<h3 class="panel-title">Create Permission</h3>
		<p class="text-muted m-b-30"> Fill all required field.</p>
	</div>
	<div class="panel-body">
		<form name="add_permission" action="<?php echo e(route('permissions.store')); ?>" method="post">
			<?php echo e(csrf_field()); ?>

			<div class="col-md-12">
				<label for="name">Permission Name : </label>
				<input type="text" class="form-control" name="name" value="" class="span12" required>
			</div>
			<div class="col-md-12">
				<label for="display_name">Permission Display Name : </label>
				<input type="text" class="form-control" name="display_name" class="span12" value="" required>
			</div>
			<div class="col-md-12">
				<label for="detail"> Permission Detail </label>
				<textarea name="detail" class="textarea_editor form-control" style="height:40%;" required></textarea>
			</div>
			<div class="col-md-12 text-center" style="margin-top:5%;">
					<label for="">&nbsp</label>
					<input type="submit" class="btn btn-success" value="CREATE">
			</div>
		</form>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.textarea_editor').wysihtml5();
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>