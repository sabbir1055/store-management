<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Add Comission </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('comission.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <label for="name"> Comission Amount : (In Tk)</label>
                <input type="text" class="form-control" name="amount" id="name" required>
            </div>
            <div class="col-md-6">
                <label for="discount"> Comission Rate : (in percent)</label>
                <input type="number" class="form-control" name="comission" id="discount" required>
            </div>

            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">ADD COMISSION</button>
            </div>
        </div>
    </div>
</form>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>