<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Update Reseller # <?php echo e($resaler->name); ?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('resaler.update',$resaler->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

                <div class="col-md-6">
                    <label for="name">Reseller Name : </label>
                    <input type="text" class="form-control" name="name" id="name" value="<?php echo e($resaler->name); ?>" required>
                </div>
                <div class="col-md-6">
                    <label for="email">Reseller Email : </label>
                    <input type="email" class="form-control" name="email" id="email" value="<?php echo e($resaler->email); ?>" required>
                </div>
                <div class="col-md-6">
                    <label for="contact">Reseller Contact No : </label>
                    <input type="text" class="form-control" name="contact" id="contact" value="<?php echo e($resaler->contact); ?>" required>
                </div>
                <div class="col-md-6">
                    <label for="district">Reseller District : </label>
                    <input type="text" class="form-control" name="district" id="district" value="<?php echo e($resaler->district); ?>" required>
                </div>
                <div class="col-md-6">
                    <label for="village">Reseller Village : </label>
                    <input type="text" class="form-control" name="village" id="village" value="<?php echo e($resaler->village); ?>" required>
                </div>
                <div class="col-md-6">
                    <label for="area">Reseller Area : </label>
                    <input type="text" class="form-control" name="area" id="area" value="<?php echo e($resaler->area); ?>" required>
                </div>
                <div class="col-md-6">
                    <label for="image">Reseller Image : </label>
                    <input type="file" class="form-control" name="image" id="district">
                </div>
                <?php if($resaler->image): ?>
                <div class="col-md-6" style="margin-top:2%">
                    <label for=""> &nbsp </label>
                    <img src="<?php echo e(asset($resaler->image)); ?>" alt="">
                </div>
                <?php endif; ?>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="submit" class="btn btn-success" value="UPDATE RESELLER">
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>