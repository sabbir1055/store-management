<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Update Category </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('category.update',$category->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

                <div class="col-md-6">
                    <label for="name"> Category Name : </label>
                    <input type="text" class="form-control" name="title" value = "<?php echo e($category->title); ?>">
                </div>
                <div class="col-md-6">
                    <label for="image"> Category Icon : </label>
                    <input type="file" class="form-control" name="image">
                </div>
                <div class="col-md-12">
                        <?php if($category->image): ?>
                            <img src="<?php echo e(asset($category->image)); ?>" style="width:200px;height:200px; margin-left:65%;">
                        <?php endif; ?>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for=""> &nbsp </label>
                    <button type="submit" class="btn btn-success">UPDATE CATEGORY</button>
                </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>