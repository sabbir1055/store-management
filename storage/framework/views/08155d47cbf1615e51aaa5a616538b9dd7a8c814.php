<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Import Product </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('import.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-12">
                <div class="col-md-6">
                    <label for="name"> Vendor Name : </label>
                        <select  name="vendor_id" class="form-control vendor_name" onchange="getData()" required>
                            <option value="">Select a vendor</option>
                            <?php if($vendor): ?>
                                <?php $__currentLoopData = $vendor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($value->id); ?>"> <?php echo e($value->name); ?> </option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="col-md-6" id="address_data"></div>
            </div>
                <div class="col-md-12" id="import_product_list" style="margin-top:30px;display:none;">
                        <table class="table table-dark table-responsive">
                            <thead>
                              <tr>
                                <th>#</th>
                                <th>CODE</th>
                                <th>TITLE</th>
                                <th>UNIT PRICE</th>
                                <th>QTY</th>
                                <th>TOTAL</th>
                                <th><button type="button" class="button button5 table_row_add_button" style="border-radius:50%;"> <i class="fa fa-plus"></i> </button></th>
                              </tr>
                            </thead>
                            <tbody id="product_list">

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td></td>
                                    <td>
                                        Total payment : <input type="text" name="total_payment" id="total_payment" class="form-control" value="0" readonly>
                                    </td>
                                    <td>
                                        Total paid : <input type="text" name="total_paid" id="total_paid" class="form-control" value="0">
                                    </td>
                                    <td>
                                        Total due : <input type="text" name="total_due" id="total_due" class="form-control" value="0" readonly>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                </div>
                <div id="form_submit_btn" class="text-center" style="display:none;">
                        <button type="submit" class="btn btn-success"> ADD IMPORT </button>
                </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
    <script type="text/javascript">
    var data;
        function getData(){
            var vendor_id = $('.vendor_name').val();
            if(vendor_id){
                $.ajax({
                    url: '<?php echo e(route('vendor.detail')); ?>',
                    method: 'post',
                    'data': {
                        '_token'    : '<?php echo e(csrf_token()); ?>',
                        'vendor_id' : vendor_id
                    },
                    success: function (d) {
                        data = JSON.parse(d);
                        var dom = ''
                        var dom2 = ''
                        dom = `<div ="col-md-6" >
                                        <label for="name">Vendor Contact : </label>
                                        <input type="text" id="name" value="`+data.vendor.msisdn+`" class="form-control" disabled>
                                        <label for="name">Vendor Address : </label>
                                        <input type="text" id="name" value="`+data.vendor.address+`" class="form-control" disabled>
                                        <label for="name">Vendor Email : </label>
                                        <input type="text" id="name" value="`+data.vendor.email+`" class="form-control" disabled>
                                        <label for="name">Vendor Website : </label>
                                        <input type="text" id="name" value="`+data.vendor.website+`" class="form-control" disabled>
                                    </div>`;
                        dom2 = `<tr>
                                    <td><h5>#</h5></td>
                                    <td>
                                    <select name="product_code[]" class="form-control" id="product_code" >
                                        <option value="">Select product</option>`;
                                    $.each( data.product, function( key, value ) {
                                        dom2+=`<option value="`+value.product_code+`">`+value.product_code+`</option>`;
                                    });
                        dom2+=     `</select>
                                    </td>
                                    <td>
                                        <input type="text" name="product_title[]" id="product_title" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input type="number" name="product_price[]" id="product_price" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input type="text" name="product_qty[]" id="product_qty" class="form-control">
                                    </td>
                                    <td>
                                        <input type="text" id="product_total" class="form-control" disabled>
                                    </td>
                                </tr>`;
                        $('#address_data').html(dom);
                        $('#product_list').html(dom2);
                        $('#import_product_list').attr('style','display:block');
                        $('#form_submit_btn').attr('style','display:block');
                    }
                });
            }
        }
        $(document).on('change','#product_code',function(){
            var dom = $(this);
                var p_id = $(this).val();
                $.each( data.product, function( key, value ) {
                    if(value.product_code == p_id){
                        dom.parent().parent().find('#product_price').val(value.product_price);
                        dom.parent().parent().find('#product_title').val(value.product_title);
                        dom.parent().parent().find('#product_qty').val(1);
                        dom.parent().parent().find('#product_total').val(value.product_price*1);
                        var product_prices = $("input[id='product_price']")
                          .map(function(){return $(this).val();}).get();

                          var product_qties = $("input[id='product_qty']")
                            .map(function(){return $(this).val();}).get();
                            var total = 0;
                        $.each(product_prices,function(i,value){
                            total += (value*product_qties[i]);
                        });
                        $('#total_payment').val(total);
                        $('#total_due').val(total);
                    }
                });
        });
        $(document).on('keyup','#product_qty',function(){
            var price = $(this).parent().parent().find('#product_price').val();
            var qty = $(this).parent().parent().find('#product_qty').val();
            $(this).parent().parent().find('#product_total').val(price*qty);
            var product_prices = $("input[id='product_price']")
              .map(function(){return $(this).val();}).get();

              var product_qties = $("input[id='product_qty']")
                .map(function(){return $(this).val();}).get();
                var total = 0;
            $.each(product_prices,function(i,value){
                total += (value*product_qties[i]);
            });
            var paid  = $('#total_paid').val();
            $('#total_payment').val(total);
            $('#total_due').val(total-paid);
        });
        $(document).on('click','.table_row_add_button',function(){
            var dom2 ="";
            dom2 = `<tr>
                        <td><h5>#</h5></td>
                        <td>
                        <select name="product_code[]" class="form-control" id="product_code" >
                            <option value="">Select product</option>`;
                        $.each( data.product, function( key, value ) {
                            dom2+=`<option value="`+value.product_code+`">`+value.product_code+`</option>`;
                        });
            dom2+=     `</select>
                        </td>
                        <td>
                            <input type="text" name="product_title[]" id="product_title" class="form-control" readonly>
                        </td>
                        <td>
                            <input type="number" name="product_price[]" id="product_price" class="form-control" readonly>
                        </td>
                        <td>
                            <input type="text" name="product_qty[]" id="product_qty" class="form-control">
                        </td>
                        <td>
                            <input type="text" id="product_total" class="form-control" disabled>
                        </td>
                        <td><button type="button" class="button button5 table_row_remove_button" style="border-radius:50%;"> <i class="fa fa-minus"></i> </button></td>
                    </tr>`;
                $('#product_list').append(dom2);
        });
        $(document).on('click','.table_row_remove_button',function(){
            $(this).parent().parent().remove();
        });
        $(document).on('keyup','#total_paid',function(){
            var total  = $('#total_payment').val();
            var paid  = $('#total_paid').val();
            $('#total_due').val(total-paid);
        });
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>