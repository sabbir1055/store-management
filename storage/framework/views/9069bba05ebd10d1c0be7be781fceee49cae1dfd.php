<!-- error/success messages -->
<!-- <?php if(count($errors)): ?>
      <div class="alert alert-danger">
            <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
      </div>
<?php endif; ?> -->
<!-- <?php if( Session::has('successMsg')): ?>
      <p class="btn btn-block btn-success disabled successMsg m-b-30"><?php echo e(Session::get('successMsg')); ?></p>
<?php endif; ?>
<?php if( Session::has('errorMsg') ): ?>
      <p class="btn btn-block btn-danger disabled errorMsg m-b-30"><?php echo e(Session::get('errorMsg')); ?></p>
<?php endif; ?>  -->
<?php
    $successMsg = Session::get('successMsg');
    $errorMsg   = Session::get('errorMsg');
?>
<span id="successMsg" data="<?php echo e($successMsg); ?>"></span>
<span id="errorMsg" data="<?php echo e($errorMsg); ?>"></span>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-centre"></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal" style="display:block;margin:auto;">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END -->
