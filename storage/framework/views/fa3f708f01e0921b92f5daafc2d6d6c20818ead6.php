<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> All Products Identity </h3>
    </div>
<div class="panel-body">
        <table id="VendorDataTable" class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th>Product Title</th>
                    <th>Product Code</th>
                    <th>Product Identity</th>
                    <th>Product Batch ID</th>
                    <th>Resaler ID</th>
                    <th>Product status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->

            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th>Product Title</th>
                    <th>Product Code</th>
                    <th>Product Identity</th>
                    <th>Product Batch ID</th>
                    <th>Resaler ID</th>
                    <th>Product status</th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
$(document).ready(function(){
    var table = $('#VendorDataTable').DataTable({
            'processing' 	: true,
            'serverSide' 	: true,
            'ajax'			:{
                'url'       : '<?php echo e(route('identifier.data')); ?>',
                'dataType' 	: 'json',
                'type' 		: 'POST',
                'data'		: {
                    '_token'    : '<?php echo e(csrf_token()); ?>',
                }
            },
            'columns' 		: [
                { 'data' : '#'},
                { 'data' : 'title','orderable':false},
                { 'data' : 'code','orderable':false},
                { 'data' : 'identity','orderable':false},
                { 'data' : 'import_id'},
                { 'data' : 'resaler_id'},
                { 'data' : 'type'},
                { 'data' : 'action','searchable':false,'orderable':false}
            ]
        });
    });

    function edit(e){
        window.location.href = window.Laravel.base_url+"/identifier/edit/"+e;
    }
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>