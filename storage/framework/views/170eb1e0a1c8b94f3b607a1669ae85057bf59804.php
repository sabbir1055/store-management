<!doctype html>
<html lang="en">
<?php
	$theme = Auth::user()->theme;
 ?>
<head>
	<title>Store Management</title>
	<!-- <meta charset="utf-8"> -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="<?php echo e(asset('assets/vendor/bootstrap/css/bootstrap.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('assets/vendor/font-awesome/css/font-awesome.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('assets/vendor/linearicons/style.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('assets/vendor/chartist/css/chartist-custom.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('bower_components/html5-editor/bootstrap-wysihtml5.css')); ?>">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo e(asset('datatable/datatable.css')); ?>">
    <!-- MAIN CSS -->
	<link rel="stylesheet" href="<?php echo e(asset('assets/css/main.css')); ?>">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="<?php echo e(asset('assets/css/demo.css')); ?>">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo e(asset('assets/img/apple-icon.png')); ?>">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo e(asset('icon.png')); ?>">
	<style media="screen">
		
		::-webkit-scrollbar {
		  	width: 0px;
		}

		.navbar-default{
			background: <?php echo e($theme->top_nav); ?>;
		}
		.brand{
			background: <?php echo e($theme->brand); ?> !important;
		}
		.sidebar{
			background: <?php echo e($theme->left_side); ?> !important;
		}
		.dropdown{
			background: <?php echo e($theme->top_nav_drop_background); ?> !important;
		}
		.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
			background: <?php echo e($theme->top_nav_drop_background_hover); ?> !important;
		}
		.dropdown-menu{
			background: <?php echo e($theme->top_nav_drop_background_items); ?> !important;
		}
		.sidebar .nav > li > a:focus, .sidebar .nav > li > a.active{
			background: <?php echo e($theme->left_side_dropdown); ?> !important;
			border-left-color : <?php echo e($theme->left_border_left_color); ?> !important;
		}
		.sidebar .nav .nav {
			background: <?php echo e($theme->left_dropdown_item_back); ?> !important;
		}
		body{
			font-family: <?php echo e($theme->font_family); ?> !important;
			font-size: <?php echo e($theme->font_size); ?> !important;
		}
		.main-content{
			background: <?php echo e($theme->main_background); ?>;
		}
		/* .main{
			color: <?php echo e($theme->body_font_color); ?>;
		} */
		.panel{
			background: <?php echo e($theme->panel_background); ?> ;
		}
		.panel-heading{
			background	: <?php echo e($theme->title_background); ?> !important;
			color		: <?php echo e($theme->title_font_color); ?> !important ;
		}
		.panel .panel-body{
			background	: <?php echo e($theme->body_background); ?> !important;
			color		: <?php echo e($theme->body_font_color); ?> !important;
		}
		.sidebar .nav > li > a{
			color		: <?php echo e($theme->left_side_font_color); ?> !important;
			font-family	: <?php echo e($theme->left_side_font_family); ?> !important;
			font-size	: <?php echo e($theme->left_side_font_size); ?> !important;
		}
		.role{
			font-weight: bold;
		}
		.role ul li{
			list-style: none;
		}
		label{
			font-size: 20px;
		}
		.modal-title{
			color: black;
		}
		.modal-body{
			color: black;
		}
	</style>
	<script>
	    window.Laravel = <?php echo json_encode([
	        'base_url'  => \URL::to('/'),
	    ]); ?>
   </script>
</head>

<body>
	<!-- WRAPPER -->
	<div id="wrapper">
		<!-- NAVBAR -->
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="brand">
				<a href="<?php echo e(route('dashboard')); ?>"><img src="<?php echo e(asset('logo.png')); ?>" alt="Klorofil Logo" class="img-responsive" style="height:62px; width:80px;"></a>
			</div>
			<div class="container-fluid">
				<div class="navbar-btn">
					<button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
				</div>
				<div id="navbar-menu">
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php if(Auth::user()->data): ?> <?php echo e(asset(Auth::user()->data->image ?? 'noimage.png')); ?> <?php else: ?> <?php echo e(asset('noimage.png')); ?> <?php endif; ?>" class="img-circle" alt="Avatar" style="width: 72px;margin-right:23px;"> <span><?php echo e(Auth::user()->data ? Auth::user()->data->name : Auth::user()->email); ?></span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo e(route('profile.index')); ?>"><i class="lnr lnr-user"></i> <span>My Profile</span></a></li>
								<li><a href="<?php echo e(route('profile.edit')); ?>"><i class="lnr lnr-pencil"></i> <span>Edit Profile</span></a></li>
								<li><a href="<?php echo e(route('setting')); ?>"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>
								<li><a href="<?php echo e(route('logout')); ?>"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- END NAVBAR -->
