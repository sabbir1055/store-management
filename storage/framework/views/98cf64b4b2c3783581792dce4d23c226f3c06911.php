<?php $__env->startSection('css'); ?>
<style media="screen">

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3 class="panel-title">Edit Profile</h3>
        <p class="text-muted m-b-30"> Fill all required field.</p>
    </div>
    <div class="panel-body">
        <form name="add_permission" action="<?php echo e(route('profile.update')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <div class="col-md-12">
                    <img src="<?php if(Auth::user()->data): ?> <?php echo e(asset((Auth::user()->data->image !='') ? Auth::user()->data->image : 'noimage.png')); ?> <?php endif; ?>" class="img img-responsive" style="width:100%;"/>
                </div>
                <label for="image">Choose new Image : </label>
                <input type="file" class="form-control" id="image" name="image" value="">
            </div>
            <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
                <label for="">Email : </label>
                <input type="text" class="form-control" value="<?php echo e(Auth::user()->email); ?>" disabled>
                <label for="">Role : </label>
                <input type="text" class="form-control" value="<?php echo e(Auth::user()->role->display_name); ?>" disabled>
                <label for="password">New Password : </label>
                <input type="password" id="password" name="password" class="form-control">
            </div>
            <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
                <div class="col-md-12 text-center">
                    <span>Basic Informations</span>
                </div>
                <label for="name">Name : </label>
                <input type="text" class="form-control" id="name" name="name" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->name ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>">
                <label for="msisdn">Msisdn : </label>
                <input type="text" class="form-control" id="msisdn" name="msisdn" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->msisdn ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>">
                <label for="b_date">Birthday : </label>
                <input type="date" class="form-control" id="b_date" name="b_date" value="<?php if(Auth::user()->data): ?><?php echo e((string)substr(Auth::user()->data->b_date,0,10)??' '); ?><?php else: ?> ' '<?php endif; ?>">
                <label for="address">Address : </label>
                <input type="text" class="form-control" id="address" name="address" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->address ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>">
                <label for="b_group">Blood group : </label>
                <input type="text" class="form-control" id="b_group" name="b_group" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->b_group ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>">
                <label for="gender">Gender : </label>
                <select class="form-control" id="gender" name="gender">
                    <option value="1" <?php if(Auth::user()->data): ?> <?php if(Auth::user()->data->gender == 1): ?> selected <?php endif; ?> <?php endif; ?>>Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label for="">&nbsp</label>
                <button type="submit" class="btn btn-success" name="button">UPDATE</button></a>
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>