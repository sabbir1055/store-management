<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Update Comission </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('comission.update',$comission->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="com-md-6">
                <label for="name"> Comission Amount : (In Tk)</label>
                <input type="text" class="form-control" name="amount" id="name" value="<?php echo e($comission->amount); ?>" required>
            </div>
            <div class="com-md-6">
                <label for="discount"> Comission Rate : (in percent)</label>
                <input type="number" class="form-control" name="comission" id="discount" value="<?php echo e($comission->commision); ?>" required>
            </div>

            <div class="com-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">UPDATE COMISSION</button>
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>