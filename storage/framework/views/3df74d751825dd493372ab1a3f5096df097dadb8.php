<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3 class="panel-title">Create Employee</h3>
        <p class="text-muted m-b-30"> Fill all required field.</p>
    </div>
    <div class="panel-body">
        <form name="add_employee" action="<?php echo e(route('employee.store')); ?>" method="post">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <label for="email">Employee Email : </label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Enter employee email here ........" value="" required>
            </div>
            <div class="col-md-6">
                <label for="role">Employee Role : </label>
                <select class="form-control" name="role" required>
                    <option value="">Select an role</option>
                    <?php if($role): ?>
                        <?php $__currentLoopData = $role; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($value->id); ?>"><?php echo e($value->display_name); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </select>
            </div>

            <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="submit" class="btn btn-success" value="CREATE">
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>