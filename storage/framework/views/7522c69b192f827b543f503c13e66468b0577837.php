<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> All Imports </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="<?php echo e(route('import.create')); ?>"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Import</button></a>
        </div>

        <table id="VendorDataTable" class="table table-hover table-fixed table-responsive" style="width:100%;">
            <!-- <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="100"> -->
            <!--Table head-->
            <thead class="table--head">
                <tr>
                    <th> Show Products </th>
                    <th>Import Id</th>
                    <th>Total</th>
                    <th>Paid</th>
                    <th>Due</th>
                    <th>Vendor</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->

            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th>Show Products</th>
                    <th>Import Id</th>
                    <th>Total</th>
                    <th>Paid</th>
                    <th>Due</th>
                    <th>Vendor</th>
                    <th class="text-center">Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
function format ( d ) {
    // `d` is the original data object for the row
    var dom = '';
    $.each( d, function( key, value ){
        dom+= ` <table cellpadding="5" cellspacing="0" border="0">
                    <span class="pull-left"> `+(++key)+`/  </span>
                    <tr>
                        <td>Product Title : </td>
                        <td>`+value.title+`</td>
                    </tr>
                    <tr>
                        <td>Product Code : </td>
                        <td>`+value.code+`</td>
                    </tr>
                    <tr>
                        <td>Product Price : </td>
                        <td>`+value.price+`</td>
                    </tr>
                    <tr>
                        <td>Product Quantity : </td>
                        <td> `+value.qty+` </td>
                    </tr>
                </table>`;
    });
    return dom;
    }
$(document).ready(function(){
    var table = $('#VendorDataTable').DataTable({
        'processing' 	: true,
        'serverSide' 	: true,
        'ajax'			:{
            'url'       : '<?php echo e(route('import.data')); ?>',
            'dataType' 	: 'json',
            'type' 		: 'POST',
            'data'		: {
                '_token'    : '<?php echo e(csrf_token()); ?>',
            }
        },
        'columns' 		: [
            {
                "className":'details-control',
                "orderable":false,
                "searchable":false,
                "defaultContent":'<i class="fa fa-eye"></i>',
            },
            { 'data' : 'id'},
            { 'data' : 'total'},
            { 'data' : 'paid'},
            { 'data' : 'due'},
            { 'data' : 'vendor_id'},
            { 'data' : 'action','searchable':false,'orderable':false}
        ]
    });
    $('#VendorDataTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            // console.log();
            row.child( format(JSON.parse(row.data().products)) ).show();
            tr.addClass('shown');
        }
    } );
});
function status(e){
    $.ajax({
        url: '<?php echo e(route('import.status')); ?>',
        method: 'post',
        'data': {
            '_token'    : '<?php echo e(csrf_token()); ?>',
            'import_id' : e
        },
        success: function (data) {
            location.reload();
        }
    });
}
function edit(e){
    window.location.href = window.Laravel.base_url+"/import/edit/"+e;
}
function view(e){
    window.location.href = window.Laravel.base_url+"/import/view/"+e;
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>