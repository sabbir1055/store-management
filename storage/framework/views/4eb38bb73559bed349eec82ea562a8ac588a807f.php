<?php $__env->startSection('content'); ?>
<div class="module">
    <div class="module-head">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Update Vendor</h3>
    </div>
    <div class="module-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('vendor.update',$vendor->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <?php if($vendor->image): ?>
            <div class="control-group" style="padding:10px;margin-left:25%;">
                <img src="<?php echo e(asset($vendor->image)); ?>" class="img img-responsive img-circle"alt="">
            </div>
            <?php endif; ?>
            <div class="control-group">
                <label class="control-label" for="name">Vendor Name : </label>
                <div class="controls">
                    <input type="text" id="name" name="name" placeholder="Type name here..." class="span8" value="<?php echo e($vendor->name); ?>" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="email">Vendor Email : </label>
                <div class="controls">
                    <input type="email" id="email" name="email" value="<?php echo e($vendor->email); ?>" placeholder="Type email here..." class="span8" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="image">Vendor Image : </label>
                <div class="controls">
                    <input type="file" id="image" name="image" class="span8">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="msisdn">Vendor Mobile : </label>
                <div class="controls">
                    <input type="text" name="country_code" value="+880" style="width:5%;" disabled>
                    <input type="number" id="msisdn" name="msisdn" value="<?php echo e(substr($vendor->msisdn,-10)); ?>" class="span8" style="width:58%;" required>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="address">Vendor Address : </label>
                <div class="controls">
                    <input type="text" id="address" value="<?php echo e($vendor->address); ?>" name="address" class="span8">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="website">Vendor Website : </label>
                <div class="controls">
                    <input type="text" id="website" value="<?php echo e($vendor->website); ?>" name="website" class="span8">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="">&nbsp </label>
                <div class="controls">
                    <button type="submit" class="btn btn-success" > UPDATE VENDOR </button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>