<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3 class="panel-title">Profile</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-6">
            <div class="col-md-12">
                <img src="<?php if(Auth::user()->data): ?> <?php echo e(asset((Auth::user()->data->image !='') ? Auth::user()->data->image : 'noimage.png')); ?> <?php else: ?> <?php echo e(asset('noimage.png')); ?> <?php endif; ?>" class="img img-responsive" style="width:100%;"/>
            </div>
        </div>
        <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
            <label for="">Email : </label>
            <input type="text" class="form-control" value="<?php echo e(Auth::user()->email); ?>" disabled>
            <label for="">Role : </label>
            <input type="text" class="form-control" value="<?php echo e(Auth::user()->role->display_name); ?>" disabled>
        </div>
        <div class="col-md-6" style="margin:0 auto;padding:10px;font-size:26px;font-weight:bold;">
            <div class="col-md-12 text-center">
                <span>Basic Informations</span>
            </div>
            <label for="">Name : </label>
            <input type="text" class="form-control" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->name ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>" disabled>
            <label for="">Msisdn : </label>
            <input type="text" class="form-control" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->msisdn ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>" disabled>
            <label for="">Birthday : </label>
            <input type="text" class="form-control" value="<?php if(Auth::user()->data): ?> <?php echo e(substr(Auth::user()->data->b_date,0,10) ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>" disabled>
            <label for="">Address : </label>
            <input type="text" class="form-control" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->address ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>" disabled>
            <label for="">Blood group : </label>
            <input type="text" class="form-control" value="<?php if(Auth::user()->data): ?> <?php echo e(Auth::user()->data->b_group ?? ' '); ?> <?php else: ?> ' ' <?php endif; ?>" disabled>
            <label for="">Gender : </label>
            <input type="text" class="form-control" value="<?php if(Auth::user()->data): ?> <?php if(Auth::user()->data->gender == 1): ?> Male <?php else: ?> Female <?php endif; ?>  <?php else: ?> ' ' <?php endif; ?>" disabled>
        </div>
        <div class="col-md-12 text-center" style="margin-top:5%;">
            <label for="">&nbsp</label>
            <a href="<?php echo e(route('profile.edit')); ?>"><button type="button" class="btn btn-primary" name="button">EDIT</button></a>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>