<?php $__env->startSection('css'); ?>
<style media="screen">

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Edit Product Status </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('identifier.update',$identifier->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <label for="name"> Product Name : </label>
                <input type="text" class="form-control" name="amount" id="name" value="<?php echo e(@$identifier->product[0]->title); ?>" readonly>
            </div>
            <div class="col-md-6">
                <label for="discount"> Product Code : </label>
                <input type="text" class="form-control" name="comission" id="discount" value="<?php echo e($identifier->product_code); ?>" readonly>
            </div>
            <div class="col-md-6">
                <label for="discount"> Product Status : </label>
                <select name="type" class="form-control" required>
                    <option value="">select product status </option>
                    <option value="1" <?php if($identifier->type == 1): ?> selected <?php endif; ?>> Sold </option>
                    <option value="2" <?php if($identifier->type == 2): ?> selected <?php endif; ?>> Resaler </option>
                    <option value="3" <?php if($identifier->type == 3): ?> selected <?php endif; ?>> Returned </option>
                    <option value="4" <?php if($identifier->type == 4): ?> selected <?php endif; ?>> Damaged </option>
                    <option value="5" <?php if($identifier->type == 5): ?> selected <?php endif; ?>> Recovered </option>
                </select>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <div class="controls">
                    <button type="submit" class="btn btn-success">UPDATE STATUS</button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>