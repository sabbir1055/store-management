<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Add Designation </h3>
    </div>
    <div class="panel-body">
        <form action="<?php echo e(route('membership.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <label for="name"> Membership Title : </label>
                <input type="text" class="form-control" name="title" id="name" required>
            </div>
            <div class="col-md-6">
                <label for="discount"> Membership Discount : (in percent)</label>
                <input type="number" class="form-control" name="discount" id="discount" required>
            </div>

            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label for=""> &nbsp </label>
                <button type="submit" class="btn btn-success">ADD MEMBERSHIP</button>
            </div>
        </div>
    </div>
</form>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>