<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3>View Sallery sheet</h3>
    </div>
    <div class="panel-body">
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Basic Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label > Employee Name : </label>
                    <input type="text" id="e_name" class="form-control" name="" value="<?php echo e($payroll->employee->data->name); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Email : </label>
                    <input type="text" id="e_email" class="form-control" name="" value="<?php echo e($payroll->employee->email); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Designation : </label>
                    <input type="text" id="e_designation" class="form-control" name="" value="<?php echo e($payroll->employee->designationdata[0]->title); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Role : </label>
                    <input type="text" id="e_role" class="form-control" name="" value="<?php echo e($payroll->employee->role->display_name); ?>" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Sallery Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label for="basic"> Basic : </label>
                    <input type="number" id="basic" class="form-control" name="basic" value="<?php echo e($payroll->basic_sallery); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label for="medical"> Medical Allowance : </label>
                    <input type="number" id="medical" class="form-control" name="medical" value="<?php echo e($payroll->medical); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label for="home"> Home Allowance : </label>
                    <input type="number" id="home" class="form-control" name="home" value="<?php echo e($payroll->home); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label for="commission"> Commission : </label>
                    <input type="number" id="commission" class="form-control" name="commission" value="<?php echo e($payroll->commission); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label for="adjust"> Adjust Total : </label>
                    <input type="number" id="adjust" class="form-control" name="adjust" value="<?php echo e($payroll->deduction); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label for="others"> Others : </label>
                    <input type="number" id="others" class="form-control" name="others" value="<?php echo e($payroll->other); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label for="total"> Total : </label>
                    <input type="number" id="total" class="form-control" name="total" value="<?php echo e($payroll->total); ?>" disabled>
                </div>
                <div class="col-md-6">
                    <label for="total_pay"> Total Pay : </label>
                    <input type="number" id="total_pay" class="form-control" name="total_pay" value="<?php echo e($payroll->total_pay); ?>" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>