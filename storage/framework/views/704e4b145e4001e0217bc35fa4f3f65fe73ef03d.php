<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> All Sales Offers </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="<?php echo e(route('salesoffer.create')); ?>"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Offer</button></a>
        </div>

        <table class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th> Product </th>
                    <th> Import ID </th>
                    <th> Discount </th>
                    <th> From </th>
                    <th> To </th>
                    <th> Type </th>
                    <th>Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
                <?php if($sales): ?>
                    <?php $__currentLoopData = $sales; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e(++$i); ?></td>
                            <td><?php echo e($value->product->title); ?> </td>
                            <td><?php echo e($value->product->import_id); ?> </td>
                            <td><?php echo e($value->discount); ?> %</td>
                            <td><?php echo e(substr($value->from,0,10)); ?> </td>
                            <td><?php echo e(substr($value->to,0,10)); ?> </td>
                            <td><?php if($value->sales_type == 1): ?> Discount Sale <?php elseif($value->sales_type ==-2): ?> Flash Sale <?php endif; ?></td>
                            <td>
                                <a href="<?php echo e(route('salesoffer.edit',$value->id)); ?>"> <button type="button" class="btn btn-info"> <i class="fa fa-edit"></i> </button> </a>
                                <a href="<?php echo e(route('salesoffer.delete',$value->id)); ?>"> <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button> </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </tbody>
            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th> Product </th>
                    <th> Import ID </th>
                    <th> Discount </th>
                    <th> From </th>
                    <th> To </th>
                    <th> Type </th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>