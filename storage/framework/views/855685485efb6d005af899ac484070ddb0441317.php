<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
		<?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<h3 class="panel-title"> Edit Employee Role </h3>
		<p class="text-muted m-b-30"> Fill all required field.</p>
	</div>
	<div class="panel-body">
		<form name="add_permission" action="<?php echo e(route('role.employee.update')); ?>" method="post">
			<?php echo e(csrf_field()); ?>


			<div class="col-md-6 role">
				<label for="permission">Role : </label>
				<select class="form-control" name="role" name="role">
					<option value="">Select a role</option>
					<?php if($roles): ?>
						<?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($r->id); ?>"><?php echo e($r->display_name); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</select>
			</div>

			<div class="col-md-6 role">
				<label for="permission">Employee : </label>
				<select class="form-control" name="employee" name="role">
					<option value="">Select an employee</option>
					<?php if($user): ?>
						<?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<option value="<?php echo e($r->id); ?>"><?php echo e($r->data ? $r->data->name : $r->email); ?></option>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</select>
			</div>
			<div class="col-md-12 text-center" style="margin-top:5%;">
					<label for="">&nbsp</label>
					<input type="submit" class="btn btn-success" value="UPDATE">
			</div>
		</form>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.textarea_editor').wysihtml5();
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>