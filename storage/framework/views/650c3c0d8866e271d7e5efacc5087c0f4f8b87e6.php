<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
		<?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<h3 class="panel-title"> Edit Role </h3>
		<p class="text-muted m-b-30"> Fill all required field.</p>
	</div>
	<div class="panel-body">
		<form name="add_permission" action="<?php echo e(route('role.update',$role->id)); ?>" method="post">
			<?php echo e(csrf_field()); ?>

			<div class="col-md-6">
				<label for="name">Role Name : </label>
				<input type="text" class="form-control" name="name" id="name" value="<?php echo e($role->name); ?>" required>
			</div>
			<div class="col-md-6">
				<label for="display_name">Role Display Name : </label>
				<input type="text" class="form-control" name="display_name" id="display_name"  value="<?php echo e($role->display_name); ?>" required>
			</div>
			<div class="col-md-6 role">
				<label for="permission">Role Permission : </label>
				<ul>
					<?php if($permission): ?>
						<?php $__currentLoopData = $permission; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $r): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<li> <input type="checkbox" name="permissions[]" value="<?php echo e($r->id); ?>" <?php $__currentLoopData = $role->permissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $p): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																									<?php if($p->id == $r->id): ?>
																										checked
																									<?php endif; ?>
																									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>> <?php echo e($r->display_name); ?> </li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					<?php endif; ?>
				</ul>
			</div>
			<div class="col-md-6">
				<label for="detail">Role Details : </label>
				<textarea name="detail" class="form-control textarea_editor" style="width:90%;height:10%"><?php echo $role->description; ?></textarea>
			</div>
			<div class="col-md-12 text-center" style="margin-top:5%;">
					<label for="">&nbsp</label>
					<input type="submit" class="btn btn-success" value="UPDATE">
			</div>
		</form>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
	$(document).ready(function(){
		$('.textarea_editor').wysihtml5();
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>