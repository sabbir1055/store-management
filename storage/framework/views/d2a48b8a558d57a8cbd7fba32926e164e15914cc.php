<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3>Add Vendor</h3>
    </div>
    <div class="panel-body">
        <form action="<?php echo e(route('vendor.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-12">
                <label class="form-control" for="name">Vendor Name : </label>
                <input type="text" id="name" name="name" placeholder="Type name here..." class="form-control" required>
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <label class="form-control" for="email">Vendor Email : </label>
                <input type="email" id="email" name="email" placeholder="Type email here..." class="form-control" required>
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <label class="form-control" for="image">Vendor Image : </label>
                <input type="file" id="image" name="image" class="form-control">
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <label class="form-control" for="msisdn">Vendor Mobile : </label>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <input type="text" name="country_code" value="+880" class="form-control" disabled>
                    </div>
                    <div class="col-md-8">
                        <input type="number" id="msisdn" name="msisdn" placeholder="Enter number here ..." class=" form-control" required>
                    </div>
                </div>
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <label class="form-control" for="address">Vendor Address : </label>
                <input type="text" id="address" name="address" class="form-control">
            </div>
            <div class="col-md-12" style="margin-top:10px;">
                <label class="form-control" for="website">Vendor Website : </label>
                <input type="text" id="website" name="website" class="form-control">
            </div>
            <div class="col-md-12 text-center" style="margin-top:10px;">
                <label for="">&nbsp </label>
                <button type="submit" class="btn btn-success" > ADD VENDOR </button>
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>