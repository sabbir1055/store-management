<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> <?php echo e($product->title); ?></h3>
    </div>
    <div class="panel-body">
            <div class="col-md-6">
                <div class="col-md-12">
                    <img src="<?php echo e(asset($product->image ?? 'noimage.png')); ?>" class="img img-responsive" style="border-radius:10px;border:1px solid black">
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT DETAIL : <span class="form-control" style="border:none;"> <?php echo $product->about; ?> </span></h3>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-12" style="margin-top:2%">
                    <h3> TITLE : <span class="form-control"> <?php echo e($product->title); ?> </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT CODE : <span class="form-control"> <?php echo e($product->product_code); ?> </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT PRICE : <span class="form-control"> <?php echo e($product->price); ?> </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>VENDOR NAME : <span class="form-control" > <?php echo e($product->vendor->name); ?> </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT CATEGORY : <span class="form-control"> <?php echo e($product->category->title); ?> </span></h3>
                </div>
                <div class="col-md-12">
                    <h3>PRODUCT SUBCATEGORY : <span class="form-control" > <?php echo e($product->subcategory->title ?? ""); ?> </span></h3>
                </div>
            </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>