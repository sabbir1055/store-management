<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Add Resaler </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('resaler.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

                <div class="col-md-6">
                    <label for="name">Resaler Name : </label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="email">Resaler Email : </label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="contact">Resaler Contact No : </label>
                    <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="district">Resaler District : </label>
                    <input type="text" class="form-control" name="district" id="district" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="village">Resaler Village : </label>
                    <input type="text" class="form-control" name="village" id="village" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="area">Resaler Area : </label>
                    <input type="text" class="form-control" name="area" id="area" placeholder="Enter resaler name here ........" value="" required>
                </div>
                <div class="col-md-6">
                    <label for="image">Resaler Image : </label>
                    <input type="file" class="form-control" name="image" id="district" placeholder="Enter resaler name here ........" value="" required>
                </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="submit" class="btn btn-success" value="ADD RESALER">
            </div>
        </form>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>