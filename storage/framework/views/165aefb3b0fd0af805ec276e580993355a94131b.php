<?php $__env->startSection('css'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Add Designation </h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('designation.store')); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <label for="name"> Designation Name : </label>
                <input type="text" class="form-control" name="title" id="name" required>
            </div>
            <div class="col-md-6">
                <label for="basic"> Basic Sallery : </label>
                <input type="number" class="form-control" name="basic" id="basic" required>
            </div>
            <div class="col-md-6">
                <label for="increment"> Increment : (in percent) </label>
                <input type="number" class="form-control" name="increment" id="increment" required>
            </div>
            <div class="col-md-6">
                <label for="medical"> Medical : </label>
                <input type="number" class="form-control" name="medical" id="medical" required>
            </div>
            <div class="col-md-6">
                <label for="home"> Home rent : </label>
                <input type="number" class="form-control" name="home" id="home" required>
            </div>
            <div class="col-md-6">
                <label for="other"> Others : </label>
                <input type="number" class="form-control" name="other" id="other" required>
            </div>
            <div class="col-md-6">
                <label for="leave"> Leaves : (Days) </label>
                <input type="number" class="form-control" name="leave" id="leave" required>
            </div>
            <div class="col-md-12 text-center" style="margin-top:5%;">
                <label class="control-label" for=""> &nbsp </label>
                <div class="controls">
                    <button type="submit" class="btn btn-success">ADD DESIGNATION</button>
                </div>
            </div>
        </div>
    </div>
</form>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>