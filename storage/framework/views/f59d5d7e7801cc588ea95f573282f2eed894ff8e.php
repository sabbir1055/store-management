<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> All Subcategories </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="<?php echo e(route('subcategory.create')); ?>"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Subcategory</button></a>
        </div>

        <table class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th> Subcategory Title </th>
                    <th> Category Title </th>
                    <th> Image </th>
                    <th>Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
                <?php if($category): ?>
                    <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e(++$i); ?></td>
                            <td><?php echo e($value->title); ?></td>
                            <td><?php echo e($value->category->title); ?></td>
                            <td> <img src="<?php echo e(asset($value->image)); ?>" style="width:60px;height:60px;"></td>
                            <td>
                                <a href="<?php echo e(route('subcategory.edit',$value->id)); ?>"> <button type="button" class="btn btn-info"> <i class="icon-edit"></i> Edit </button> </a>
                                <a href="<?php echo e(route('subcategory.delete',$value->id)); ?>"> <button type="button" class="btn btn-danger"> <i class="icon-trash"></i> Delete </button> </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </tbody>
            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th> Subcategory Title </th>
                    <th> Category Title </th>
                    <th> Image </th>
                    <th>Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>