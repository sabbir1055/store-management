<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> Vendor #<?php echo e($vendor->name); ?></h3>
    </div>
    <div class="panel-body">
        <form class="form-horizontal row-fluid" action="<?php echo e(route('vendor.update',$vendor->id)); ?>" method="post" enctype="multipart/form-data">
            <?php echo e(csrf_field()); ?>

            <div class="col-md-6">
                <div class="col-md-12">
                    <?php if($vendor->image): ?>
                        <img src="<?php echo e(asset($vendor->image??'noimage.png')); ?>" class="img img-responsive" style="border-radius:10px;"alt="">
                    <?php endif; ?>
                </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <label class="control-label" for="name">Vendor Name : </label>
                <input type="text" id="name" class="form-control" name="name" placeholder="Type name here..." value="<?php echo e($vendor->name); ?>" disabled>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="email">Vendor Email : </label>
                <input type="email" id="email" name="email" class="form-control" value="<?php echo e($vendor->email); ?>" placeholder="Type email here..." disabled>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="image">Vendor Image : </label>
                <input type="file" id="image" class="form-control" name="image">
            </div>
            <div class="col-md-12">
                <label class="control-label" for="msisdn">Vendor Mobile : </label>
                <div class="col-md-12">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="country_code" value="+880" disabled>
                    </div>
                    <div class="col-md-8">
                        <input type="number" id="msisdn" class="form-control" name="msisdn" value="<?php echo e(substr($vendor->msisdn,-10)); ?>" disabled>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="address">Vendor Address : </label>
                <input type="text" id="address" class="form-control" value="<?php echo e($vendor->address); ?>" name="address" disabled>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="website">Vendor Website : </label>
                <input type="text" id="website" class="form-control" value="<?php echo e($vendor->website); ?>" name="website" disabled>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- TASKS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Vendor product transections</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                    <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                </div>
            </div>
            <div class="panel-body">
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>