<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3>Create Sallery sheet</h3>
    </div>
    <div class="panel-body">
        <!-- <form action="<?php echo e(route('payroll.store')); ?>" method="post" enctype="multipart/form-data"> -->
        <div class="col-md-12">
            <label for="employee_id">Employee Name : </label>
            <select class="form-control" id="employee_id" onchange="getemployee()" name="employee_id">
                <option value="">Select an employee</option>
                <?php if($employee): ?>
                <?php $__currentLoopData = $employee; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($value->id); ?>"><?php echo e($value->data->name ?? $value->email); ?></option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </select>
        </div>
        <!-- </form> -->
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Basic Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label > Employee Name : </label>
                    <input type="text" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Email : </label>
                    <input type="text" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Designation : </label>
                    <input type="text" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Employee Role : </label>
                    <input type="text" class="form-control" name="" value="" disabled>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- MULTI CHARTS -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Employee Sallery Info </h3>
                <div class="right">
                    <button type="button" class="btn-toggle-collapse"><i class="lnr lnr-chevron-up"></i></button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-6">
                    <label > Basic : </label>
                    <input type="text" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Medical Allowance : </label>
                    <input type="text" class="form-control" name="" value="" required>
                </div>
                <div class="col-md-6">
                    <label > Home Allowance : </label>
                    <input type="text" class="form-control" name="" value="" required>
                </div>
                <div class="col-md-6">
                    <label > Commission : </label>
                    <input type="text" class="form-control" name="" value="" disabled>
                </div>
                <div class="col-md-6">
                    <label > Over Time : </label>
                    <input type="text" class="form-control" name="" value=""required>
                </div>
                <div class="col-md-6">
                    <label > Adjust Total : </label>
                    <input type="text" class="form-control" name="" value="" required>
                </div>
                <div class="col-md-12 text-center" style="margin-top:5%;">
                    <label for="">&nbsp</label>
                    <input type="button" class="btn btn-success" onclick="storeSheet()" value="CREATE">
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
function getemployee(){
    var id = $("#employee_id").val();
    if(id){
        $.ajax({
            url: '<?php echo e(route('payroll.employee.data')); ?>',
            method: 'post',
            'data': {
                '_token'    : '<?php echo e(csrf_token()); ?>',
                'employee_id' : id
            },
            success: function (d) {
                alert(d);
            }
        });
    }
}
function storeSheet(){
    var id = $("#employee_id").val();
    if(id){
        $.ajax({
            url: '<?php echo e(route('payroll.employee.data')); ?>',
            method: 'post',
            'data': {
                '_token'    : '<?php echo e(csrf_token()); ?>',
                'employee_id' : id
            },
            success: function (d) {
                alert(d);
            }
        });
    }
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>