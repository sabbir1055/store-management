<?php $__env->startSection('content'); ?>
<div class="panel panel-headline">
    <div class="panel-heading">
        <?php echo $__env->make('partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <h3> All Designations </h3>
    </div>
    <div class="panel-body">
        <div class="pull-right" style="padding:10px;">
            <a href="<?php echo e(route('designation.create')); ?>"><button type="button" class="btn btn-success" name="button"> <i class="fa fa-plus"></i>&nbsp &nbsp Add Designation</button></a>
        </div>

        <table class="table table-hover table-fixed table-responsive" style="text-align:center; width:100%;">
            <!-- <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="50">
            <col width="100"> -->
            <thead class="table--head">
                <tr>
                    <th> SL </th>
                    <th> Designation Title </th>
                    <th> Sallery </th>
                    <th> Increment </th>
                    <th> Medical </th>
                    <th> Home rent </th>
                    <th> Other </th>
                    <th> Leave </th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <!--Table head-->

            <!--Table body-->
            <tbody>
                <?php if($designation): ?>
                    <?php $__currentLoopData = $designation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e(++$i); ?></td>
                            <td><?php echo e($value->title); ?></td>
                            <td><?php echo e($value->basic_sallery); ?> Tk</td>
                            <td><?php echo e($value->increment); ?> %</td>
                            <td><?php echo e($value->medical); ?> Tk</td>
                            <td><?php echo e($value->home); ?> Tk</td>
                            <td><?php echo e($value->other); ?> Tk</td>
                            <td><?php echo e($value->leave); ?> Tk</td>
                            <td>
                                <a href="<?php echo e(route('designation.edit',$value->id)); ?>"> <button type="button" class="btn btn-info"> <i class="fa fa-edit"></i></button> </a>
                                <a href="<?php echo e(route('designation.delete',$value->id)); ?>"> <button type="button" class="btn btn-danger"> <i class="fa fa-trash"></i> </button> </a>
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </tbody>
            <!--Table body-->

            <!-- table footer -->
            <tfoot>
                <tr>
                    <th> SL </th>
                    <th> Designation Title </th>
                    <th> Sallery </th>
                    <th> Increment </th>
                    <th> Medical </th>
                    <th> Home rent </th>
                    <th> Other </th>
                    <th> Leave </th>
                    <th class="text-center">Action</th>
                </tr>
            </tfoot>
            <!-- table footer -->
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.layout.layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>