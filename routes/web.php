<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){
        return redirect()->route('login.form');
})->middleware('guest');
Route::get('/login','Auth\LoginController@index')->name('login.form');
Route::post('/login','Auth\LoginController@login')->name('login')->middleware('guest');
Route::get('/logout','Auth\LoginController@logout')->name('logout')->middleware('auth');
// dashboard routes
Route::group(['prefix'=>'dashboard','middleware'=>['auth']],function(){
    Route::get('/','DashboardController@index')->name('dashboard');
    Route::post('/date-slaes-data','DashboardController@salesDataDate')->name('dashboard.saleschart');
});
// Vendor routes
Route::group(['prefix'=>'vendor','middleware'=>['permission:can-modify-vendor','auth']],function(){
    Route::get('/','VendorController@index')->name('vendor.index');
    Route::get('/create','VendorController@create')->name('vendor.create');
    Route::post('/store','VendorController@store')->name('vendor.store');
    Route::post('/update/{id}','VendorController@update')->name('vendor.update');
    Route::post('/data','VendorController@data')->name('vendor.data');
    Route::get('/edit/{id}','VendorController@edit')->name('vendor.edit');
    Route::get('/view/{id}','VendorController@view')->name('vendor.view');
    Route::post('/status','VendorController@status')->name('vendor.status');
    Route::post('/details','VendorController@detail')->name('vendor.detail');
    Route::get('/product','VendorController@product')->name('vendor.product');
    Route::post('/product','VendorController@storeProduct')->name('vendor.product.store');
    Route::post('/product/data','VendorController@getProuctData')->name('vendor.product.data');
    Route::get('/report','VendorController@report')->name('vendor.report');
});
//  Product routes
Route::group(['prefix'=>'product','middleware'=>['permission:can-modify-product','auth']],function(){
    Route::get('/','ProductController@index')->name('product.index');
    Route::get('/create','ProductController@create')->name('product.create');
    Route::post('/store','ProductController@store')->name('product.store');
    Route::post('/update/{id}','ProductController@update')->name('product.update');
    Route::post('/data','ProductController@data')->name('product.data');
    Route::get('/edit/{id}','ProductController@edit')->name('product.edit');
    Route::get('/view/{id}','ProductController@view')->name('product.view');
    Route::post('/status','ProductController@status')->name('product.status');
    Route::post('/import','ProductController@import')->name('product.import');
    Route::get('/report','ProductController@report')->name('product.report');
    Route::post('/category','ProductController@category')->name('product.category');
});
//  Import routes
Route::group(['prefix'=>'import','middleware'=>['permission:can-modify-import','auth']],function(){
    Route::get('/','ImportController@index')->name('import.index');
    Route::get('/create','ImportController@create')->name('import.create');
    Route::post('/store','ImportController@store')->name('import.store');
    Route::post('/update','ImportController@update')->name('import.update');
    Route::post('/data','ImportController@data')->name('import.data');
    Route::get('/edit/{id}','ImportController@edit')->name('import.edit');
    Route::get('/view/{id}','ImportController@view')->name('import.view');
    Route::post('/status','ImportController@status')->name('import.status');
    Route::post('/import','ImportController@import')->name('import.import');
    Route::get('/report','ImportController@report')->name('import.report');
});
//  Employee routes
Route::group(['prefix'=>'/employee','middleware'=>['permission:can-modify-employee','auth']],function(){
    Route::get('/','EmployeeController@index')->name('users');
    Route::post('/','EmployeeController@data')->name('employee.data');
    Route::get('/create','EmployeeController@create')->name('employee.create');
    Route::get('/designation','EmployeeController@designation')->name('employee.designation');
    Route::post('/designation','EmployeeController@designationStore')->name('employee.designation.store');
    Route::post('/create','EmployeeController@store')->name('employee.store');
    Route::post('/status','EmployeeController@status')->name('employee.status');
});
//  Category routes
Route::group(['prefix'=>'category','middleware'=>['permission:can-modify-category','auth']],function(){
    Route::get('/','CategoryController@index')->name('category.index');
    Route::get('/create','CategoryController@create')->name('category.create');
    Route::post('/store','CategoryController@store')->name('category.store');
    Route::get('/edit/{id}','CategoryController@edit')->name('category.edit');
    Route::post('/update/{id}','CategoryController@update')->name('category.update');
    Route::get('/delete/{id}','CategoryController@delete')->name('category.delete');
});
//  Sub category routes
Route::group(['prefix'=>'subcategory','middleware'=>['permission:can-modify-category','auth']],function(){
    Route::get('/','SubCategoryController@index')->name('subcategory.index');
    Route::get('/create','SubCategoryController@create')->name('subcategory.create');
    Route::post('/store','SubCategoryController@store')->name('subcategory.store');
    Route::post('/update/{id}','SubCategoryController@update')->name('subcategory.update');
    Route::get('/edit/{id}','SubCategoryController@edit')->name('subcategory.edit');
    Route::get('/delete/{id}','SubCategoryController@delete')->name('subcategory.delete');
});
//  Permission routes
Route::group(['prefix'=>'/permissions','middleware'=>['permission:can-modify-role','auth']],function(){
  Route::get('/','PermissionController@index')->name('permissions');
  Route::get('/create','PermissionController@create')->name('permissions.create');
  Route::post('/create','PermissionController@store')->name('permissions.store');
  Route::get('/delete/{id}','PermissionController@delete')->name('permissions.delete');
});
// Role routes
Route::group(['prefix'=>'/role','middleware'=>['permission:can-modify-role','auth']],function(){
  Route::get('/','RoleController@index')->name('roles');
  Route::get('/create','RoleController@create')->name('role.create');
  Route::post('/create','RoleController@store')->name('role.store');
  Route::get('/edit/{id}','RoleController@edit')->name('role.edit');
  Route::post('/update/{id}','RoleController@update')->name('role.update');
  Route::get('/delete/{id}','RoleController@delete')->name('role.delete');
  Route::get('/employee/edit','RoleController@editEmployee')->name('role.employee.edit');
  Route::post('/employee/update','RoleController@updateEmployee')->name('role.employee.update');
});
//  Designation routes
Route::group(['prefix'=>'/designation','middleware'=>['permission:can-modify-employee','auth']],function(){
    Route::get('/','DesignationController@index')->name('designation.index');
    Route::get('/create','DesignationController@create')->name('designation.create');
    Route::post('/store','DesignationController@store')->name('designation.store');
    Route::get('/edit/{id}','DesignationController@edit')->name('designation.edit');
    Route::post('/update/{id}','DesignationController@update')->name('designation.update');
    Route::get('/delete/{id}','DesignationController@delete')->name('designation.delete');
});
//  Membership routes
Route::group(['prefix'=>'/membership','middleware'=>['permission:can-modify-membership','auth']],function(){
    Route::get('/','MembershipController@index')->name('membership.index');
    Route::get('/create','MembershipController@create')->name('membership.create');
    Route::post('/store','MembershipController@store')->name('membership.store');
    Route::get('/edit/{id}','MembershipController@edit')->name('membership.edit');
    Route::post('/update/{id}','MembershipController@update')->name('membership.update');
    Route::get('/delete/{id}','MembershipController@delete')->name('membership.delete');
});
//  comission routes
Route::group(['prefix'=>'/comission','middleware'=>['permission:can-modify-commission','auth']],function(){
    Route::get('/','ComissionController@index')->name('comission.index');
    Route::get('/create','ComissionController@create')->name('comission.create');
    Route::post('/store','ComissionController@store')->name('comission.store');
    Route::get('/edit/{id}','ComissionController@edit')->name('comission.edit');
    Route::post('/update/{id}','ComissionController@update')->name('comission.update');
    Route::get('/delete/{id}','ComissionController@delete')->name('comission.delete');
});
// Salesoffer routes
Route::group(['prefix'=>'/salesoffer','middleware'=>['permission:can-modify-salesoffer','auth']],function(){
    Route::get('/','SalesOfferController@index')->name('salesoffer.index');
    Route::get('/create','SalesOfferController@create')->name('salesoffer.create');
    Route::post('/store','SalesOfferController@store')->name('salesoffer.store');
    Route::get('/edit/{id}','SalesOfferController@edit')->name('salesoffer.edit');
    Route::post('/update/{id}','SalesOfferController@update')->name('salesoffer.update');
    Route::get('/delete/{id}','SalesOfferController@delete')->name('salesoffer.delete');
});
//  Identifier routes
Route::group(['prefix'=>'/identifier','middleware'=>['permission:can-modify-product','auth']],function(){
    Route::get('/','IdentifierController@index')->name('identifier.index');
    Route::post('/data','IdentifierController@data')->name('identifier.data');
    Route::get('/edit/{id}','IdentifierController@edit')->name('identifier.edit');
    Route::post('/update/{id}','IdentifierController@update')->name('identifier.update');
});
// Resaler routes
Route::group(['prefix'=>'resaler','middleware'=>['permission:can-modify-resaler','auth']],function(){
    Route::get('/','ResalerController@index')->name('resaler.index');
    Route::get('/create','ResalerController@create')->name('resaler.create');
    Route::post('/store','ResalerController@store')->name('resaler.store');
    Route::post('/update/{id}','ResalerController@update')->name('resaler.update');
    Route::post('/data','ResalerController@data')->name('resaler.data');
    Route::get('/edit/{id}','ResalerController@edit')->name('resaler.edit');
    Route::get('/view/{id}','ResalerController@view')->name('resaler.view');
    Route::post('/status','ResalerController@status')->name('resaler.status');
    Route::post('/resaler','ResalerController@resaler')->name('resaler.resaler');
    Route::get('/report','ResalerController@report')->name('resaler.report');
});
//  sell routes
Route::group(['prefix'=>'sell','middleware'=>['permission:can-modify-sell','auth']],function(){
    Route::get('/','salesController@index')->name('sell.index');
    Route::get('/create','salesController@create')->name('sell.create');
    Route::post('/store','salesController@store')->name('sell.store');
    Route::post('/update','salesController@update')->name('sell.update');
    Route::post('/data','salesController@data')->name('sell.data');
    Route::get('/edit/{id}','salesController@edit')->name('sell.edit');
    Route::get('/view/{id}','salesController@view')->name('sell.view');
    Route::post('/products','salesController@product')->name('sell.product');
    Route::get('/report','salesController@report')->name('sell.report');
});
// Theme routes
Route::group(['prefix'=>'setting','middleware'=>['auth']],function(){
    Route::get('/','ThemeController@edit')->name('setting');
    Route::post('/','ThemeController@update')->name('setting.update');
});
// profile routes
Route::group(['prefix'=>'/profile','middleware'=>['auth']],function(){
    Route::get('/','ProfileController@index')->name('profile.index');
    Route::get('/edit','ProfileController@edit')->name('profile.edit');
    Route::post('/update','ProfileController@update')->name('profile.update');
});
// Sallery-Sheet routes
Route::group(['prefix'=>'/payroll','middleware'=>['permission:can-modify-payroll','auth']],function(){
    Route::get('/','PayrollController@index')->name('payroll.index');
    Route::post('/','PayrollController@data')->name('payroll.data');
    Route::get('/create','PayrollController@create')->name('payroll.create');
    Route::post('/create','PayrollController@store')->name('payroll.store');
    Route::get('/edit/{id}','PayrollController@edit')->name('payroll.edit');
    Route::post('/edit/{id}','PayrollController@update')->name('payroll.update');
    Route::get('/view/{id}','PayrollController@view')->name('payroll.view');
    Route::post('/employee/data','PayrollController@getEmployeeData')->name('payroll.employee.data');
    Route::post('/delete','PayrollController@delete')->name('payroll.delete');
});
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('auth');
